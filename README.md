# SwiBa

<img src="./03_marketing/SwiBaLogo.png" height=100>

## App-Beschreibung
SwiBa ist ein Spiel, das Deine Geschwindigkeit und Reaktionsfähigkeit auf die Probe stellt. Sortieren... Langweilig... NEIN! Mit SwiBa macht sortieren auch mal Spaß! Versuche die richtigen Farben den richtigen Sektionen zuzuordnen, um Deinen Highscore immer aufs neue zu knacken! Natürlich gibt es auch Power-Ups, die Dir die Highscorejagt vereinfachen. Und das wichtigste, es fallen keine zusätzlichen Kosten für die Power-Ups an, denn die Philosophie von mir: "Pay to win ist nicht können, sondern cheaten!"
Made with Love in Bavaria 

<img src="./03_marketing/1.jpg" height=300>
<img src="./03_marketing/2.jpg" height=300>
<img src="./03_marketing/3.jpg" height=300>
<img src="./03_marketing/4.jpg" height=300>
<img src="./03_marketing/5.jpg" height=300>


## Features:
- Light and Dark Mode (Automatisch/Manuell)
- Audio stummschalten
- App zurücksetzbar
- Tutorial am Anfang des Spiels
- Endless Game (Es gibt kein Ziel)
- Keine versteckten kosten

## Technologie
- Die App ist komplett in Swift entwickelt
  - https://www.swift.org
- Ich habe SpriteKit als Framework verwendet
  - https://developer.apple.com/spritekit/
- Ohne externe Libraries entwickelt

## Zum Betatest:
- Es gibt einen Betatest der auf 50 Personen beschränkt ist
- Leider ist dieses Spiel nur für iOS erhältlich weswegen es auch nur einen iOS Betatest gibt
- Der Betatest läuft über Apples Testflight Plattform bei der Du Dich mit deiner Apple-ID anmelden musst. https://testflight.apple.com
- Hier der Link zum Betatest: https://testflight.apple.com/join/t7nljCdc
<br><img src="./03_marketing/qr-code.png" height=200>


## Copyrights:
© TapApp Software

Das Repo ist ausschließlich dafür gedacht, die App zu Präsentieren! Ich veröffentliche hier den Code, da ich der Meinung bin, dass jeder Einsicht in den Quellcode haben soll, um zu sehen wie das Spiel im Hintergrund funktioniert. Aber ich Untersage ausdrücklich eine Veröffentlichung der App unter eigenem Namen!
