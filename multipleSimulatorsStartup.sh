#!/bin/sh

#  multipleSimulatorsStartup.sh
#  SwiBa
#
#  Created by Franz Murner on 16.01.22.
#  

open -a Simulator.app

xcrun simctl shutdown all

uninstallBefore=false
filename=SimulatorsList.txt

path=$(find ~/Library/Developer/Xcode/DerivedData/SwiBa-*/Build/Products/Debug-iphonesimulator -name "SwiBa.app" | head -n 1)
echo "${path}"


grep -v '^#' $filename | while read -r line
do
xcrun simctl boot "$line" # Boot the simulator with identifier hold by $line var
done



if [[ $uninstallBefore = true ]]
then
grep -v '^#' $filename | while read -r line
do
xcrun simctl uninstall "$line" TapApp.SwiBa # Uninstall the .app file located at location hold by $path var at booted simulator with identifier hold by $line var
done
fi


grep -v '^#' $filename | while read -r line
do
xcrun simctl install "$line" "$path" # Install the .app file located at location hold by $path var at booted simulator with identifier hold by $line var
xcrun simctl launch "$line" TapApp.SwiBa # Launch .app using its bundle at simulator with identifier hold by $line var
done
