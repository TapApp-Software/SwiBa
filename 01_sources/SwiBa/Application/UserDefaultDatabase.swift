//
//  UserDefaultDatabase.swift
//  SwiBa
//
//  Created by Franz Murner on 05.12.21.
//

import Foundation
import os

class UserDefaultDatabase {
    static let shared = UserDefaultDatabase()

    private let jsonEncoder = JSONEncoder()
    private let jsonDecoder = JSONDecoder()
    private let userDefault = UserDefaults.standard
    private let gameSaveKey = "SWIBA_GAMEDATA"

    private init() {
        EventBus.shared.subscribe(event: .systemNotificationEvent) { event in
            guard let event = event as? SystemNotificationEvent,
                  event.eventType == .sceneDidDisconnect ||
                  event.eventType == .sceneDidEnterBackground
            else { return }

            self.userDefault.synchronize()
        }
    }

    public func saveData(dataModel: GameDataModel) {
        do {
            let data = try jsonEncoder.encode(dataModel)
            userDefault.set(data, forKey: gameSaveKey)

        } catch {
            Logger().error("ERROR: JSON Encoding error")
            exit(1)
        }
    }

    public func loadData() -> GameDataModel {
        do {
            let data = userDefault.data(forKey: gameSaveKey)

            guard let data = data else { return GameDataModel(highscore: 0, creditAmount: 0) }

            return try jsonDecoder.decode(GameDataModel.self, from: data)

        } catch {
            Logger().error("ERROR: JSON Decoding error")
            userDefault.removeObject(forKey: gameSaveKey)
            return GameDataModel(highscore: 0, creditAmount: 0)
        }
    }

    public func updateHighscore(newValue: Int) {
        var model = UserDefaultDatabase.shared.loadData()
        model.highscore = newValue
        saveData(dataModel: model)
    }

    public func updateCredit(newValue: Int) {
        var model = UserDefaultDatabase.shared.loadData()
        model.creditAmount = newValue
        saveData(dataModel: model)
    }

    public func updateStorage(newValue: [PowerUps: Int]) {
        var model = UserDefaultDatabase.shared.loadData()
        model.powerUpStorage = newValue
        saveData(dataModel: model)
    }

    public func updateDarkModeStatus(newValue: DarkModeStatus) {
        var model = UserDefaultDatabase.shared.loadData()
        model.darkModeStatus = newValue
        saveData(dataModel: model)
    }

    public func updateIsSoundOn(newValue: Bool) {
        var model = UserDefaultDatabase.shared.loadData()
        model.isSoundOn = newValue
        saveData(dataModel: model)
    }

    public func updatePlayedGames(newValue: Int) {
        var model = UserDefaultDatabase.shared.loadData()
        model.playedGames = newValue
        saveData(dataModel: model)
    }

    public func updateTourtorialShowed(newValue: Bool) {
        var model = UserDefaultDatabase.shared.loadData()
        model.tourtorialShowed = newValue
        saveData(dataModel: model)
    }

    public func resetAppData() {
        userDefault.removeObject(forKey: gameSaveKey)
    }
}
