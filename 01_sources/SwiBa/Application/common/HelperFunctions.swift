//
//  HelperFunctions.swift
//  SwiBa
//
//  Created by Franz Murner on 14.09.21.
//

import SpriteKit

class HelperFunctions {
    static func textureRatioResize(textureSize: CGSize, newSize: CGSize) -> CGSize {
        let ratio = textureSize.width / textureSize.height
        var result = CGSize.zero

        if ratio < 1 {
            result = CGSize(width: newSize.width * ratio, height: newSize.height)
        } else if ratio > 1 {
            result = CGSize(width: newSize.width, height: newSize.height / ratio)
        } else {
            result = newSize
        }

        return result
    }

    static func hasDeviceNotch() -> Bool {
        guard let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate,
              let window = sceneDelegate.window
        else { return false }

        return window.safeAreaInsets.bottom > 20
    }

    static func getSaveAreaInsets() -> UIEdgeInsets {
        guard let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate,
              let window = sceneDelegate.window
        else { return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0) }

        return window.safeAreaInsets
    }
}
