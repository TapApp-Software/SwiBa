//
//  IEventBus.swift
//  SwiBa
//
//  Created by Franz Murner on 02.08.21.
//

import Foundation

protocol IEventBus {
    func subscribe(subscriberId: UUID, event: EventType, listener: @escaping (IEvent) -> Void)
    func subscribe(event: EventType, listener: @escaping (IEvent) -> Void)
    func unsubscribe(subscriberId: UUID, event: EventType)
    func unsubscribeAll(subscriberId: UUID)
    func publish(event: IEvent)
}
