//
//  SubscriberCallback.swift
//  SwiBa
//
//  Created by Franz Murner on 18.11.21.
//

import Foundation

class SubscriberCallback {
    var subscriberID: UUID
    var event: (IEvent) -> Void

    internal init(subscriberID: UUID, event: @escaping (IEvent) -> Void) {
        self.subscriberID = subscriberID
        self.event = event
    }
}
