//
//  CountinueButtonPressEvent.swift
//  SwiBa
//
//  Created by Franz Murner on 02.01.22.
//

import Foundation

class CountinueButtonPressEvent: IEvent {
    func getEventType() -> EventType {
        .countinueButtonPressEvent
    }
}
