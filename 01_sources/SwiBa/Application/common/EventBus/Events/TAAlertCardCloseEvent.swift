//
//  CloseDeletionAlert.swift
//  SwiBa
//
//  Created by Franz Murner on 05.12.21.
//

import Foundation

class TAAlertCardCloseEvent: IEvent {
    func getEventType() -> EventType {
        return .taAlertCardCloseEvent
    }
}
