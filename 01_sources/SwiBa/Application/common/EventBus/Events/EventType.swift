//
//  EventNames.swift
//  SwiBa
//
//  Created by Franz Murner on 18.08.21.
//

import Foundation

enum EventType {
    /// Use if the trait collection send a system appearance change notification
    case UIInterfaceStyleUpdateNotification
    /// Use for the updates in a specific class
    case UIInterfaceStyleUpdate
    /// Use to notify when the dark mode button is pressed
    case UIInterfaceStyleOverride

    case touchLockOnEvent
    case touchLockOffEvent
    case hapticEvent
    case wrongCollisionEvent
    case rightCollisionEvent
    case goEvent
    case endEvent
    case collisionEvent
    case buttonPressedEvent
    case buttonUpdateEvent
    case labelButtonUpdateEvent
    case buttonLockEvent
    case powerUpEvent
    case autolabelChangeEvent
    case confettiEvent
    /// Use to change the stroke color of the score board
    case scoreBoardStrokeEvent
    case comboLevelEvent
    case manualDockEvents
    case taBottomCardCloseEvent

    // MARK: PowerUps

    case powerUpAddButtonPressEvent
    case taButtonChangeEvent
    case taAlertCardCloseEvent
    case deletionConfirmButtonPressEvent

    case systemNotificationEvent

    case taTimerStartEvent
    case taTimerStopEvent

    case countinueButtonPressEvent
}
