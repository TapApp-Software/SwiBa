//
//  HapticEvent.swift
//  SwiBa
//
//  Created by Franz Murner on 09.08.21.
//

import Foundation
import UIKit

class HapticEvent: IEvent {
    public var hapticLevel: UIImpactFeedbackGenerator.FeedbackStyle

    init(hapticLevel: UIImpactFeedbackGenerator.FeedbackStyle) {
        self.hapticLevel = hapticLevel
    }

    func getEventType() -> EventType {
        return .hapticEvent
    }
}
