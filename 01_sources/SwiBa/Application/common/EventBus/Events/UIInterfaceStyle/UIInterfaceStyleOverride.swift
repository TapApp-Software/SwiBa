//
//  UIInterfaceStyleOverride.swift
//  SwiBa
//
//  Created by Franz Murner on 26.09.21.
//

import Foundation
import UIKit

class UIInterfaceStyleOverride: IEvent {
    var appearance: UIUserInterfaceStyle

    init(appearance: UIUserInterfaceStyle) {
        self.appearance = appearance
    }

    func getEventType() -> EventType {
        .UIInterfaceStyleOverride
    }
}
