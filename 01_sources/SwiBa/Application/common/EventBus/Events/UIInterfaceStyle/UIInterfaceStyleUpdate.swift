//
//  TestEvent.swift
//  SwiBa
//
//  Created by Franz Murner on 02.08.21.
//

import Foundation

class UIInterfaceStyleUpdate: IEvent {
    var isDark: Bool

    init(isDark: Bool = true) {
        self.isDark = isDark
    }

    func getEventType() -> EventType {
        return .UIInterfaceStyleUpdate
    }
}
