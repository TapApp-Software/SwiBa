//
//  UIInterfaceStyleUpdateNotification.swift
//  SwiBa
//
//  Created by Franz Murner on 26.09.21.
//

import Foundation
import UIKit

class UIInterfaceStyleUpdateNotification: IEvent {
    var currentMode: UIUserInterfaceStyle

    init(currentMode: UIUserInterfaceStyle) {
        self.currentMode = currentMode
    }

    func getEventType() -> EventType {
        .UIInterfaceStyleUpdateNotification
    }
}
