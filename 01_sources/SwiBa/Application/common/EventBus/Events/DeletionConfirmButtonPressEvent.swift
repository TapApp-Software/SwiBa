//
//  DeletionConfirmButtonPressEvent.swift
//  SwiBa
//
//  Created by Franz Murner on 05.12.21.
//

import Foundation

class DeletionConfirmButtonPressEvent: IEvent {
    func getEventType() -> EventType {
        return .deletionConfirmButtonPressEvent
    }
}
