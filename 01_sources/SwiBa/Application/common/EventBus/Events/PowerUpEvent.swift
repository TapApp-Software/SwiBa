//
//  PowerUpEvent.swift
//  SwiBa
//
//  Created by Franz Murner on 28.09.21.
//

import SpriteKit

class PowerUpEvent: IEvent {
    var buttonType: DockButtonType
    var isActive: Bool
    var duration: CGFloat
    var completion: () -> Void

    init(buttonType: DockButtonType, isActive: Bool, duration: CGFloat, completion: @escaping () -> Void = {}) {
        self.buttonType = buttonType
        self.isActive = isActive
        self.duration = duration
        self.completion = completion
    }

    init(buttonType: DockButtonType, isActive: Bool) {
        self.buttonType = buttonType
        self.isActive = isActive
        self.duration = 0
        self.completion = {}
    }

    func getEventType() -> EventType {
        return .powerUpEvent
    }
}
