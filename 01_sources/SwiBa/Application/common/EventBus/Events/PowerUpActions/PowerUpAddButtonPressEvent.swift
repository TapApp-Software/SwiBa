//
//  PowerUpAddButtonEvent.swift
//  SwiBa
//
//  Created by Franz Murner on 02.12.21.
//

import Foundation

class PowerUpAddButtonPressEvent: IEvent {
    var buttonType: DockButtonType

    internal init(buttonType: DockButtonType) {
        self.buttonType = buttonType
    }

    func getEventType() -> EventType {
        return .powerUpAddButtonPressEvent
    }
}
