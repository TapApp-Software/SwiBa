//
//  AutolabelChangeEvent.swift
//  SwiBa
//
//  Created by Franz Murner on 03.10.21.
//

import Foundation

class AutolabelChangeEvent: IEvent {
    public var newLabelText: String
    public var labelType: LabelTypes

    init(newLabelText: String, labelType: LabelTypes) {
        self.newLabelText = newLabelText
        self.labelType = labelType
    }

    func getEventType() -> EventType {
        return .autolabelChangeEvent
    }
}
