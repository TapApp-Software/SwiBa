//
//  ManualDockEvents.swift
//  SwiBa
//
//  Created by Franz Murner on 01.11.21.
//

import Foundation

class ManualDockEvents: IEvent {
    var isMainMenuDock: Bool

    init(isMainMenuDock: Bool) {
        self.isMainMenuDock = isMainMenuDock
    }

    func getEventType() -> EventType {
        return .manualDockEvents
    }
}
