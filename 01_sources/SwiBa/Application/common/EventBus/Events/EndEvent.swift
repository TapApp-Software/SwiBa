//
//  EndEvent.swift
//  SwiBa
//
//  Created by Franz Murner on 17.08.21.
//

import Foundation

class EndEvent: IEvent {
    func getEventType() -> EventType {
        return .endEvent
    }
}
