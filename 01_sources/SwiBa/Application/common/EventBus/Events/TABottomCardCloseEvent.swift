//
//  PowerUpStoreCloseEvent.swift
//  SwiBa
//
//  Created by Franz Murner on 23.11.21.
//

import Foundation

class TABottomCardCloseEvent: IEvent {
    func getEventType() -> EventType {
        return .taBottomCardCloseEvent
    }
}
