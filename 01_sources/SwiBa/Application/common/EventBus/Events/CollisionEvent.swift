//
//  CollisionEvent.swift
//  SwiBa
//
//  Created by Franz Murner on 18.08.21.
//

import Foundation

class CollisionEvent: IEvent {
    var ball: Ball
    var segment: Segment

    init(ball: Ball, segment: Segment) {
        self.ball = ball
        self.segment = segment
    }

    func getEventType() -> EventType {
        return .collisionEvent
    }
}
