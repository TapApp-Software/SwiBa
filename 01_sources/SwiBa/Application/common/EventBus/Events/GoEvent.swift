//
//  GoEvent.swift
//  SwiBa
//
//  Created by Franz Murner on 17.08.21.
//

import Foundation

class GoEvent: IEvent {
    func getEventType() -> EventType {
        return .goEvent
    }
}
