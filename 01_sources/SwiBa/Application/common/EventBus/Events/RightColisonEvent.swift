//
//  RightColisonEvent.swift
//  SwiBa
//
//  Created by Franz Murner on 17.08.21.
//

import Foundation

class RightCollisionEvent: IEvent {
    func getEventType() -> EventType {
        return .rightCollisionEvent
    }
}
