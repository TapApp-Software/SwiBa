//
//  TATimerStartEvent.swift
//  SwiBa
//
//  Created by Franz Murner on 31.12.21.
//

import Foundation

class TATimerStopEvent: IEvent {
    var timerType: TimerType

    init(timerType: TimerType) {
        self.timerType = timerType
    }

    func getEventType() -> EventType {
        return .taTimerStopEvent
    }
}
