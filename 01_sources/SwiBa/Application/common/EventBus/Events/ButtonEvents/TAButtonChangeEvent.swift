//
//  TAButtonChangeEvent.swift
//  SwiBa
//
//  Created by Franz Murner on 04.12.21.
//

import Foundation

class TAButtonChangeEvent: IEvent {
    public var isEnabled: Bool
    public var buttonType: ButtonType

    internal init(isEnabled: Bool, buttonType: ButtonType) {
        self.isEnabled = isEnabled
        self.buttonType = buttonType
    }

    func getEventType() -> EventType {
        return .taButtonChangeEvent
    }
}
