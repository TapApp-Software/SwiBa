//
//  ButtonLockEvent.swift
//  SwiBa
//
//  Created by Franz Murner on 28.09.21.
//

import SpriteKit

class ButtonLockEvent: IEvent {
    var buttonType: DockButtonType
    var buttonLocked: Bool

    init(buttonType: DockButtonType, buttonLocked: Bool) {
        self.buttonType = buttonType
        self.buttonLocked = buttonLocked
    }

    func getEventType() -> EventType {
        .buttonLockEvent
    }
}
