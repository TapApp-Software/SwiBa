//
//  LabelButtonUpdateEvent.swift
//  SwiBa
//
//  Created by Franz Murner on 28.09.21.
//

import Foundation

class LabelButtonUpdateEvent: IEvent {
    var buttonType: DockButtonType
    var labelValue: String

    init(buttonType: DockButtonType, labelValue: String) {
        self.buttonType = buttonType
        self.labelValue = labelValue
    }

    func getEventType() -> EventType {
        return .labelButtonUpdateEvent
    }
}
