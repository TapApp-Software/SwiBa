//
//  ButtonPressedEvent.swift
//  SwiBa
//
//  Created by Franz Murner on 16.09.21.
//

import Foundation

class ButtonPressedEvent: IEvent {
    var buttonType: DockButtonType

    init(buttonType: DockButtonType) {
        self.buttonType = buttonType
    }

    func getEventType() -> EventType {
        .buttonPressedEvent
    }
}
