//
//  ButtonUpdateEvent.swift
//  SwiBa
//
//  Created by Franz Murner on 25.09.21.
//

import SpriteKit

class ButtonUpdateEvent: IEvent {
    var buttonType: DockButtonType
    var buttonState: ButtonState

    init(buttonType: DockButtonType, buttonState: ButtonState) {
        self.buttonType = buttonType
        self.buttonState = buttonState
    }

    func getEventType() -> EventType {
        return .buttonUpdateEvent
    }
}
