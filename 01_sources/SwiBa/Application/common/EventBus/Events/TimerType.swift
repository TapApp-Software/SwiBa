//
//  TimerType.swift
//  SwiBa
//
//  Created by Franz Murner on 31.12.21.
//

import Foundation

enum TimerType {
    case ballEmitterTimer
    case circleRotationTimer
}
