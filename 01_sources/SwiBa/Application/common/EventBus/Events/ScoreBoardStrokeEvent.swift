//
//  ScoreBoardStrokeEvent.swift
//  SwiBa
//
//  Created by Franz Murner on 01.11.21.
//

import Foundation

class ScoreBoardStrokeEvent: IEvent {
    var currentComboColor: GameColor?
    var animationDuration: Double

    init(currentComboColor: GameColor?, animationDuration: Double) {
        self.currentComboColor = currentComboColor
        self.animationDuration = animationDuration
    }

    func getEventType() -> EventType {
        .scoreBoardStrokeEvent
    }
}
