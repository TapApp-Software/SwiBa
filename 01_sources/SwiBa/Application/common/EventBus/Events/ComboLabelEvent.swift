//
//  ComboLabelEvent.swift
//  SwiBa
//
//  Created by Franz Murner on 01.11.21.
//

import Foundation

class ComboLevelEvent: IEvent {
    var text: String

    init(text: String) {
        self.text = text
    }

    func getEventType() -> EventType {
        .comboLevelEvent
    }
}
