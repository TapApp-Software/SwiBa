//
//  ConfettiEvent.swift
//  SwiBa
//
//  Created by Franz Murner on 31.10.21.
//

import Foundation

class ConfettiEvent: IEvent {
    func getEventType() -> EventType {
        return .confettiEvent
    }
}
