//
//  TouchLockOnEvent.swift
//  SwiBa
//
//  Created by Franz Murner on 18.08.21.
//

import Foundation

class TouchLockOffEvent: IEvent {
    func getEventType() -> EventType {
        return .touchLockOffEvent
    }
}
