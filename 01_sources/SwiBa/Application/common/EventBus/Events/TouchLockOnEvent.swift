//
//  TouchLockOnEvent.swift
//  SwiBa
//
//  Created by Franz Murner on 18.08.21.
//

import Foundation

class TouchLockOnEvent: IEvent {
    func getEventType() -> EventType {
        return .touchLockOnEvent
    }
}
