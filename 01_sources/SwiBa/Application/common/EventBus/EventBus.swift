//
//  EventBus.swift
//  SwiBa
//
//  Created by Franz Murner on 02.08.21.
//

import Foundation

class EventBus: IEventBus {
    static let shared: IEventBus = EventBus()

    var listeners = [EventType: [SubscriberCallback]]()

    private init() {}

    func subscribe(event: EventType, listener: @escaping (IEvent) -> Void) {
        subscribe(subscriberId: UUID(), event: event, listener: listener)
    }

    func subscribe(subscriberId: UUID, event: EventType, listener: @escaping (IEvent) -> Void) {
        var listernerList = [SubscriberCallback]()
        if let listenerIndexForEvent = listeners.index(forKey: event) {
            listernerList = listeners[listenerIndexForEvent].value
            listeners.remove(at: listenerIndexForEvent)
        }

        listernerList.append(SubscriberCallback(subscriberID: subscriberId, event: listener))

        listeners[event] = listernerList
    }

    func unsubscribeAll(subscriberId: UUID) {
        for listener in listeners {
            unsubscribe(subscriberId: subscriberId, event: listener.key)
        }
    }

    func unsubscribe(subscriberId: UUID, event: EventType) {
        guard let listenerIndexForEvent = listeners.index(forKey: event) else { return }

        var count = 0

        for listener in listeners[listenerIndexForEvent].value {
            if listener.subscriberID == subscriberId {
                var subscriberListeners = listeners.remove(at: listenerIndexForEvent)
                subscriberListeners.value.remove(at: count)
                listeners[subscriberListeners.key] = subscriberListeners.value
            }

            count += 1
        }
    }

    func publish(event: IEvent) {
        guard let listenerIndexForEvent = listeners.index(forKey: event.getEventType()) else { return }

        for listener in listeners[listenerIndexForEvent].value {
            listener.event(event)
        }
    }
}
