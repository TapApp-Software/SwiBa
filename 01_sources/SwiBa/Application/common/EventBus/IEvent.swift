//
//  IEvent.swift
//  SwiBa
//
//  Created by Franz Murner on 02.08.21.
//

import Foundation

protocol IEvent {
    func getEventType() -> EventType
}
