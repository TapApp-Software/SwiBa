//
//  ButtonType.swift
//  SwiBa
//
//  Created by Franz Murner on 04.12.21.
//

import Foundation

enum ButtonType {
    case powerUpStoreMagentAddButton
    case powerUpStoreOverlapAddButton
    case powerUpStoreFreezeAddButton
    case powerUpStoreStopCircleAddButton

    case gameCountinueButton
}
