//
//  CSVImporter.swift
//  SwiBa
//
//  Created by Franz Murner on 21.12.21.
//

import Foundation
import Metal
import os

class CSVImporter {
    public static let shared = CSVImporter()

    public var gameDifficultyData: [[Float?]] = []

    private init() {
        if let data = Bundle.main.path(forResource: "hardnessControl", ofType: "csv") {
            do {
                try self.gameDifficultyData = csv(data: String(contentsOfFile: data))
                Logger().info("Game-Difficulty control data loaded!")
            } catch {
                Logger()
                    .error("ERROR: Error while getting contents of file -> Endless difficulty handler will be used!")
                gameDifficultyData = []
            }
        } else {
            Logger().error("ERROR: Game difficulty file not found! -> Endless difficulty handler will be used!")
            gameDifficultyData = []
        }
    }

    private func csv(data: String) -> [[Float?]] {
        var result: [[Float?]] = []
        let rows = data.components(separatedBy: "\n")
        for row in rows {
            let columns = row.components(separatedBy: ";")
            var preresult: [Float?] = []

            guard columns.count > 3 else { break }

            for column in columns {
                preresult.append(Float(column))
            }

            result.append(preresult)
        }
        return result
    }
}
