//
//  GameColors.swift
//  SwiBa
//
//  Created by Franz Murner on 09.08.21.
//

import SpriteKit

// https://imagecolorpicker.com/color-code/757575

class GameColor {
    private var colorLight: UIColor
    private var colorDark: UIColor
    private var colorID: UUID

    private init(light: String, dark: String) {
        self.colorID = UUID()
        self.colorLight = UIColor(hexString: light)
        self.colorDark = UIColor(hexString: dark)
    }

    private init(light: UIColor, dark: UIColor) {
        self.colorID = UUID()
        self.colorLight = light
        self.colorDark = dark
    }

    private init(both: UIColor) {
        self.colorID = UUID()
        self.colorLight = both
        self.colorDark = both
    }

    private init(both: String) {
        self.colorID = UUID()
        self.colorLight = UIColor(hexString: both)
        self.colorDark = UIColor(hexString: both)
    }

    private init(gameColor: GameColor) {
        self.colorID = UUID()
        self.colorLight = gameColor.colorLight
        self.colorDark = gameColor.colorDark
    }

    static var clear = GameColor(both: UIColor.clear)
    static var white = GameColor(both: "#FFFFFF")
    static var black = GameColor(both: "#000000")

    static var systemBlue = GameColor(light: "#007aff", dark: "#0a84ff")
    static var systemRed = GameColor(light: "#ff3b30", dark: "#ff453a")
    static var systemGreen = GameColor(light: "#34c759", dark: "#32d74b")
    static var systemYellow = GameColor(light: "#ffcc00", dark: "#ffcc00")
    static var systemMint = GameColor(both: "#3eb489")
    static var systemPink = GameColor(both: "#F50057")
    static var systemPurple = GameColor(light: "#af52de", dark: "#bf5af2")
    static var systemCyan = GameColor(light: "#5ac8fa", dark: "#64d2ff")

    static var mainLabel = GameColor(light: black.colorLight, dark: white.colorLight)

    static var primaryBackground = GameColor(light: white.colorLight, dark: black.colorLight)
    static var secondaryBackgound = GameColor(light: "#f1f2f7", dark: "#1c1c1e")
    static var tertiaryBackground = GameColor(light: "#ffffff", dark: "#2c2c2e")
    static var quaternaryBackground = GameColor(light: "#e5e5ea", dark: "#636366")

    static var separatorColor = GameColor(both: .gray.withAlphaComponent(0.5))

    static var buttonColor = GameColor(gameColor: tertiaryBackground)
    static var buttonColorPressed = GameColor(light: buttonColor.colorLight.applyShade(shade: 0.8),
                                              dark: buttonColor.colorDark.applyTint(tint: 0.4))
    static var buttonColorDisabled = GameColor(light: buttonColor.colorLight.applyShade(shade: 0.7),
                                               dark: buttonColor.colorDark.applyTint(tint: 0.5))

    static var touchBlendNodeColor = GameColor(both: .black.withAlphaComponent(0.3))

    static var grey = GameColor(light: "#999999", dark: "#757575") // 0%S / 0%T
    static var greyPressed = GameColor(light: grey.colorLight.applyShade(shade: 0.8),
                                       dark: grey.colorDark.applyTint(tint: 0.4))
    static var greyDisabled = GameColor(light: grey.colorLight.applyShade(shade: 0.7),
                                        dark: grey.colorDark.applyTint(tint: 0.5))

    static var confirmRed = GameColor(gameColor: systemRed) // 0%S / 0%T
    static var confirmRedPressed = GameColor(light: confirmRed.colorLight.applyShade(shade: 0.8),
                                             dark: confirmRed.colorDark.applyTint(tint: 0.4))
    static var confirmRedDisabled = GameColor(light: confirmRed.colorLight.applyShade(shade: 0.7),
                                              dark: confirmRed.colorDark.applyTint(tint: 0.5))

    static var confirmGreen = GameColor(light: "#389838", dark: "#228C22")
    static var confirmGreenPressed = GameColor(light: confirmGreen.colorLight.applyShade(shade: 0.8),
                                               dark: confirmGreen.colorDark.applyTint(tint: 0.4))
    static var confirmGreenDisabled = GameColor(light: confirmGreen.colorLight.applyShade(shade: 0.7),
                                                dark: confirmGreen.colorDark.applyTint(tint: 0.5))

    static var buttonBlue = GameColor(light: GameColor.systemBlue.colorLight, dark: GameColor.systemBlue.colorDark)
    static var buttonBluePressed = GameColor(light: buttonBlue.colorLight.applyShade(shade: 0.8),
                                             dark: buttonBlue.colorDark.applyTint(tint: 0.4))
    static var buttonBlueDisabled = GameColor(light: buttonBlue.colorLight.applyShade(shade: 0.7),
                                              dark: buttonBlue.colorDark.applyTint(tint: 0.5))

    static var goMenuOverlayFillColor = GameColor(light: primaryBackground.colorLight.withAlphaComponent(0.35),
                                                  dark: primaryBackground.colorDark.withAlphaComponent(0.35))
    static var goMenuOverlayStrokeColor = GameColor(light: primaryBackground.colorLight.withAlphaComponent(0.5),
                                                    dark: primaryBackground.colorDark.withAlphaComponent(0.5))

    static var progressBarColor = GameColor(light: tertiaryBackground.colorLight.withAlphaComponent(0.9),
                                            dark: tertiaryBackground.colorDark.withAlphaComponent(0.7))
    static var cardNodeShadowColor = GameColor(light: black.getColor().withAlphaComponent(0.05),
                                               dark: black.getColor().withAlphaComponent(0.1))

    static var inactiveIndicatorColor = GameColor(light: separatorColor.colorLight, dark: separatorColor.colorDark)
    static var activeIndicatorColor = GameColor(light: .black, dark: .white)

    public static func getFirstColorSet() -> [GameColor] {
        return [.systemRed,
                .systemMint,
                .systemBlue,
                .systemYellow]
    }

    public static func getSecondColorSet() -> [GameColor] {
        return [.systemPurple,
                .systemCyan,
                .systemGreen,
                .systemPink]
    }

    public static func getAllColorsForGame() -> [GameColor] {
        return [.systemRed,
                .systemMint,
                .systemBlue,
                .systemYellow,
                .systemPurple,
                .systemCyan,
                .systemGreen,
                .systemPink]
    }

    private func isDark() -> Bool {
        let darkModeStatus = UserDefaultDatabase.shared.loadData().darkModeStatus

        switch darkModeStatus {
        case .system:
            return UITraitCollection.current.userInterfaceStyle == .dark
        case .lockedDark:
            return true
        case .lockedLight:
            return false
        }
    }

    public func getColor() -> UIColor {
        return isDark() ? colorDark : colorLight
    }

    public func getID() -> UUID {
        return colorID
    }
}
