//
//  GameImages.swift
//  SwiBa
//
//  Created by Franz Murner on 30.11.21.
//

import Foundation

enum GameImages: String {
    case magnet = "Magnet"
    case overlap = "Overlap-Alowed"
    case stopRotation = "Stop-Rotation"
    case freze = "Freeze-Emitter"
    case resetIcon = "ResetIcon"
    case cart = "Cart"
    case soundOnIcon = "SoundOn"
    case soundOffIcon = "SoundOff"
}
