//
//  SKTexture+Extension.swift
//  SwiBa
//
//  Created by Franz Murner on 21.11.21.
//
import os
import SpriteKit

extension SKTexture {
    static func getTextureFromSymbole(symboleName: String, color: GameColor?, font: UIFont) -> SKTexture? {
        let configuration = UIImage.SymbolConfiguration(font: font)

        if
            let color = color,
            let textureImage = UIImage(systemName: symboleName, withConfiguration: configuration)?
            .withTintColor(color.getColor()),
            let pngData = textureImage.pngData(),
            let image = UIImage(data: pngData) {
            return SKTexture(image: image)

        } else if
            let textureImage = UIImage(systemName: symboleName, withConfiguration: configuration)?
            .withRenderingMode(.alwaysOriginal),
            let pngData = textureImage.pngData(),
            let image = UIImage(data: pngData) {
            return SKTexture(image: image)
        }

        Logger().warning("[SKTexture] WARN: No SF-Symbole with name \"\(symboleName)\" found! ")

        return nil
    }

    static func getCustomTextureFromSymbole(named: String, color: GameColor?, font: UIFont) -> SKTexture? {
        let configuration = UIImage.SymbolConfiguration(font: font)

        if
            let color = color,
            let textureImage = UIImage(named: named, in: nil, with: configuration)?
            .withTintColor(color.getColor()),
            let pngData = textureImage.pngData(),
            let image = UIImage(data: pngData) {
            return SKTexture(image: image)

        } else if
            let textureImage = UIImage(named: named, in: nil, with: configuration)?
            .withRenderingMode(.alwaysOriginal),
            let pngData = textureImage.pngData(),
            let image = UIImage(data: pngData) {
            return SKTexture(image: image)
        }

        Logger().warning("[SKTexture] WARN: No custom SF-Symbole with name \"\(named)\" found! ")

        return nil
    }
}
