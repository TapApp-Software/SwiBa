//
//  CGSize.swift
//  SwiBa
//
//  Created by Franz Murner on 09.01.22.
//

import SpriteKit

extension CGSize {
    func apply(_ size: CGSize) -> CGSize {
        let px = width + size.width
        let py = height + size.height

        return CGSize(width: px, height: py)
    }

    func applyBoth(_ size: CGFloat) -> CGSize {
        let px = width + size
        let py = height + size

        return CGSize(width: px, height: py)
    }

    mutating func apply(_ size: CGSize) {
        width += size.width
        height += size.height
    }

    mutating func applyBoth(_ size: CGFloat) {
        width += size
        height += size
    }
}
