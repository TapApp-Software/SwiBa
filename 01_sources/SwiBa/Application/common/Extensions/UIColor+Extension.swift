//
//  UIColor+Extension.swift
//  SwiBa
//
//  Created by Franz Murner on 26.09.21.
//

import SpriteKit

public extension UIColor {
    convenience init(hexString: String, alpha: Int = 1) {
        guard hexString.starts(with: "#"),
              hexString.count == 7
        else {
            self.init(red: 0, green: 0, blue: 0, alpha: 0)
            return
        }

        let red = hexString.subString(from: 1, to: 3)
        let green = hexString.subString(from: 3, to: 5)
        let blue = hexString.subString(from: 5, to: 7)

        self.init(hexR: red, hexG: green, hexB: blue, alpha: alpha)
    }

    func applyShade(shade: CGFloat) -> UIColor {
        let rgbComponents = self.rgbComponents()

        guard let rgbComponents = rgbComponents else {
            return self
        }

        let red = rgbComponents.Red * shade
        let green = rgbComponents.Green * shade
        let blue = rgbComponents.Blue * shade

        return UIColor(red: red, green: green, blue: blue, alpha: rgbComponents.Alpha)
    }

    func applyTint(tint: CGFloat) -> UIColor {
        guard let rgbComponents = self.rgbComponents() else {
            return self
        }

        let red = rgbComponents.Red + ((1.0 - rgbComponents.Red) * tint)
        let green = rgbComponents.Green + ((1.0 - rgbComponents.Green) * tint)
        let blue = rgbComponents.Blue + ((1.0 - rgbComponents.Blue) * tint)

        return UIColor(red: red, green: green, blue: blue, alpha: rgbComponents.Alpha)
    }

    convenience init(hexR: String, hexG: String, hexB: String, alpha: Int = 1) {
        let red = UInt(hexR, radix: 16) ?? 0
        let green = UInt(hexG, radix: 16) ?? 0
        let blue = UInt(hexB, radix: 16) ?? 0

        self.init(red: CGFloat(red) / 255, green: CGFloat(green) / 255, blue: CGFloat(blue) / 255,
                  alpha: CGFloat(alpha))
    }

    // swiftlint:disable large_tuple
    internal func rgbComponents() -> (Red: CGFloat, Green: CGFloat, Blue: CGFloat, Alpha: CGFloat)? {
        var fRed: CGFloat = 0
        var fGreen: CGFloat = 0
        var fBlue: CGFloat = 0
        var fAlpha: CGFloat = 0
        if self.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha) {
            return (fRed, fGreen, fBlue, fAlpha)
        } else {
            // Could not extract RGBA components:
            return nil
        }
    }

    internal func rgb() -> Int? {
        var fRed: CGFloat = 0
        var fGreen: CGFloat = 0
        var fBlue: CGFloat = 0
        var fAlpha: CGFloat = 0
        if self.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha) {
            let iRed = Int(fRed * 255.0)
            let iGreen = Int(fGreen * 255.0)
            let iBlue = Int(fBlue * 255.0)
            let iAlpha = Int(fAlpha * 255.0)

            //  (Bits 24-31 are alpha, 16-23 are red, 8-15 are green, 0-7 are blue).
            let rgb = (iAlpha << 24) + (iRed << 16) + (iGreen << 8) + iBlue
            return rgb
        } else {
            // Could not extract RGBA components:
            return nil
        }
    }
}
