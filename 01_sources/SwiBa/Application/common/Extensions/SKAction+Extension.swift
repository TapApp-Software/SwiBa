//
//  SKAction+Extension.swift
//  SwiBa
//
//  Created by Franz Murner on 14.12.21.
//

import Foundation
import SpriteKit

public extension SKAction {
    private static func changePath(radius: CGFloat) -> CGPath {
        return CGPath(ellipseIn: CGRect(x: -radius, y: -radius, width: radius * 2, height: radius * 2), transform: nil)
    }

    class func scaleAction(oldRadius: CGFloat,
                           newRadius: CGFloat,
                           animationDuration: TimeInterval,
                           timingFunction: @escaping (CGFloat) -> CGFloat = TATimingFunction
                               .elasticEaseOutSoft) -> SKAction {
        let delta: CGFloat = newRadius - oldRadius

        let scaleAction = SKAction.customAction(withDuration: animationDuration) { node, timer in
            guard let node = node as? SKShapeNode else { return }

            node.path = SKAction.changePath(radius: oldRadius + delta * timingFunction(timer / animationDuration))
        }

        return scaleAction
    }

    class func reduceLineWidth(oldLineWidth: CGFloat,
                               newLineWidth: CGFloat,
                               animationDuration: TimeInterval,
                               timingFunction: @escaping (CGFloat) -> CGFloat = TATimingFunction
                                   .elasticEaseOutSoft) -> SKAction {
        let delta: CGFloat = newLineWidth - oldLineWidth

        let lineWidthReduction = SKAction.customAction(withDuration: animationDuration) { node, timer in
            guard let node = node as? SKShapeNode else { return }

            node.lineWidth = oldLineWidth + delta * timingFunction(timer / animationDuration)
        }

        return lineWidthReduction
    }

    class func fadeFillAlpha(start: CGFloat, end: CGFloat, animationDuration: TimeInterval,
                             timingFunction: @escaping (CGFloat) -> CGFloat = TATimingFunction
                                 .elasticEaseOutSoft) -> SKAction {
        let delta: CGFloat = end - start

        var fillColor: UIColor?

        let lineWidthReduction = SKAction.customAction(withDuration: animationDuration) { node, timer in
            guard let node = node as? SKShapeNode else { return }

            if fillColor == nil {
                fillColor = node.fillColor
            }

            guard let fillColor = fillColor else {
                return
            }

            node.fillColor = fillColor.withAlphaComponent(start + delta * timingFunction(timer / animationDuration))
        }

        return lineWidthReduction
    }
}
