//
//  CGPoint+Extension.swift
//  SwiBa
//
//  Created by Franz Murner on 08.01.22.
//

import SpriteKit

extension CGPoint {
    func apply(_ point: CGPoint) -> CGPoint {
        let px = x + point.x
        let py = y + point.y

        return CGPoint(x: px, y: py)
    }
}
