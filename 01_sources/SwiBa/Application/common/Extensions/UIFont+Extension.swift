//
//  UIFont+Extension.swift
//  SwiBa
//
//  Created by Franz Murner on 16.01.22.
//

import Foundation
import UIKit

extension UIFont {
    class func tapFontRusticalFont(ofSize size: CGFloat) -> UIFont {
        return UIFont(name: "TapFontRustical", size: size) ?? UIFont.systemFont(ofSize: size)
    }
}
