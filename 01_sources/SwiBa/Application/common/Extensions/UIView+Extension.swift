//
//  UIView+Extension.swift
//  SwiBa
//
//  Created by Franz Murner on 02.01.22.
//

import SpriteKit
import UIKit

extension UIView {
    func getScreenshot() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(self.frame.size, false, 1.0)
        self.drawHierarchy(in: self.frame, afterScreenUpdates: true)
        let screenshot: UIImage? = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return screenshot
    }
}
