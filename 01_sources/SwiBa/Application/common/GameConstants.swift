//
//  GameConstants.swift
//  SwiBa
//
//  Created by Franz Murner on 30.09.21.
//

import SpriteKit

class GameConstants {
    static var circleRadius: CGFloat?
    static var colorCircleLineWidth: CGFloat = 25
    static var ballRadius: CGFloat = 50
    static var sizePercetage: CGFloat = 0.88
    static var ballLineWidth: CGFloat = 25
    static var dockButtonRadius: CGFloat = 75
    static var font = "TapFontRustical"

    static var prices = [DockButtonType.magnet: 100,
                         DockButtonType.overlap: 70,
                         DockButtonType.stopCircle: 20,
                         DockButtonType.freeze: 40]

    static var priceSymbole = "💎"

    static var durationFreeze: CGFloat = 15
    static var durationStopRotation: CGFloat = 20

    static func getSizeWithPercetage(size: CGFloat) -> CGFloat {
        return size * sizePercetage
    }
}
