//
//  GameControlSystem+DifficultyControl.swift
//  SwiBa
//
//  Created by Franz Murner on 30.10.21.
//

import SpriteKit

extension GameControlSystem {
    func addDifficultyLevels() {
        let gameDifficultyData = CSVImporter.shared.gameDifficultyData

        guard gameDifficultyData.count > 0 else { return }

        for index in 1 ..< gameDifficultyData.count {
            var addCircle = {}
            var changeBallEmmit = {}
            var changeRotation = {}

            if gameDifficultyData[index][1] != nil {
                addCircle = { [self] in
                    addCircleSegment(withColor: getRandomColor() ?? .clear)
                }
            }

            if let ballEmit = gameDifficultyData[index][2] {
                changeBallEmmit = { [self] in
                    ballEmitterTimer?.timeInterval = TimeInterval(ballEmit)
                }
            }

            if let rotation = gameDifficultyData[index][3] {
                changeRotation = { [self] in
                    setColorCircleRotation(toRPM: CGFloat(rotation))
                }
            }

            gameDifficultyHandler[index] = {
                addCircle()
                changeBallEmmit()
                changeRotation()
            }
        }
    }

    func setRandomizedDifficultyLevler() {
        gameEndlessDifficultyHandler = [Int: () -> Void]()

        gameEndlessDifficultyHandler[Int.random(in: 0 ... 5)] = { [self] in
            ballEmitterTimer?.timeInterval = TimeInterval.random(in: 0.7 ... 1.0)
        }

        gameEndlessDifficultyHandler[Int.random(in: 6 ... 10)] = { [self] in
            ballEmitterTimer?.timeInterval = TimeInterval.random(in: 0.6 ... 0.8)
        }

        gameEndlessDifficultyHandler[Int.random(in: 11 ... 15)] = { [self] in
            ballEmitterTimer?.timeInterval = TimeInterval.random(in: 0.5 ... 0.7)
        }

        gameEndlessDifficultyHandler[Int.random(in: 16 ... 20)] = { [self] in
            ballEmitterTimer?.timeInterval = TimeInterval.random(in: 0.4 ... 0.6)
        }

        gameEndlessDifficultyHandler[Int.random(in: 21 ... 25)] = { [self] in
            ballEmitterTimer?.timeInterval = TimeInterval.random(in: 0.4 ... 0.5)
        }

        gameEndlessDifficultyHandler[Int.random(in: 26 ... 30)] = { [self] in
            ballEmitterTimer?.timeInterval = 0.4
        }

        gameEndlessDifficultyHandler[Int.random(in: 31 ... 35)] = { [self] in
            ballEmitterTimer?.timeInterval = TimeInterval.random(in: 0.4 ... 0.5)
        }

        gameEndlessDifficultyHandler[Int.random(in: 36 ... 40)] = { [self] in
            ballEmitterTimer?.timeInterval = TimeInterval.random(in: 0.4 ... 0.6)
        }

        gameEndlessDifficultyHandler[Int.random(in: 41 ... 45)] = { [self] in
            ballEmitterTimer?.timeInterval = TimeInterval.random(in: 0.5 ... 0.7)
        }

        gameEndlessDifficultyHandler[Int.random(in: 46 ... 50)] = { [self] in
            ballEmitterTimer?.timeInterval = TimeInterval.random(in: 0.6 ... 0.8)
        }

        gameEndlessDifficultyHandler[Int.random(in: 51 ... 55)] = { [self] in
            ballEmitterTimer?.timeInterval = TimeInterval.random(in: 0.7 ... 1.0)
        }

        gameEndlessDifficultyHandler[Int.random(in: 56 ... 59)] = { [self] in
            ballEmitterTimer?.timeInterval = 1
        }
    }
}
