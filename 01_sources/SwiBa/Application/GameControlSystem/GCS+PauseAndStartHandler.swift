//
//  GCS+PauseAndStartHandler.swift
//  SwiBa
//
//  Created by Franz Murner on 31.12.21.
//

import Foundation
import SpriteKit

extension GameControlSystem {
    func handleUnpauseThroghPauseMenu() {
        if !isRotationLocked {
            EventBus.shared.publish(event: TATimerStartEvent(timerType: .circleRotationTimer))
        }

        if !isBallEmitterLocked {
            EventBus.shared.publish(event: TATimerStartEvent(timerType: .ballEmitterTimer))
        }

        gameMainNode.speed = 1
        gameMainNode.isPaused = false
        gamePauseNode?.removeFromParent()
        gamePauseNode = nil
    }

    func handleSystemNotificationPause() {
        EventBus.shared.publish(event: TATimerStopEvent(timerType: .ballEmitterTimer))
        EventBus.shared.publish(event: TATimerStopEvent(timerType: .circleRotationTimer))
        gameMainNode.speed = 0
        gameMainNode.isPaused = true
    }

    func handleSystemNotificationUnpause() {
        if gameActive == true {
            guard gamePauseNode == nil else { return }

            gamePauseNode = GamePausedNode(sceneSize: gameScene.size, gameMainNode: gameMainNode)

            guard let gamePauseNode = gamePauseNode else { return }

            gameScene.addChild(gamePauseNode)

        } else {
            gameMainNode.speed = 1
            gameMainNode.isPaused = false
        }
    }
}
