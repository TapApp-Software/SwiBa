//
//  GameControlSystem.swift
//  SwiBa
//
//  Created by Franz Murner on 16.08.21.
//

import SpriteKit

class GameControlSystem {
    // MARK: - Computed Properties

    // MARK: Normal

    internal var currentScore = 0 {
        didSet {
            EventBus.shared
                .publish(event: AutolabelChangeEvent(newLabelText: String(currentScore), labelType: .scoreLabel))
        }
    }

    internal var difficultyLevler = 0 {
        didSet {
            handleDificultyHandlerChange()
        }
    }

    // MARK: - Properties

    internal var combo = 1

    // MARK: - State Properties

    internal var isConfettiFired = false
    internal var gameActive = false

    internal var remainingPossibleColors = GameColor.getFirstColorSet()
    internal var currentColors = [GameColor]()

    internal var predefinedHandlers = [DockButtonType: () -> Void]()
    internal var gameDifficultyHandler = [Int: () -> Void]()
    internal var gameEndlessDifficultyHandler = [Int: () -> Void]()

    // MARK: BallEmitter

    internal var ballEmitterTimer: TATimer?
    internal var isBallEmitterLocked = false

    // MARK: PowerUps

    internal var isMagnetEnabled = false
    internal var isRotationLocked = false

    // MARK: Others

    internal var currentColorCircleRPM: CGFloat = 0

    // MARK: - Nodes

    internal var previousBallColorID: UUID?
    internal var colorCircle: ColorCircle
    internal var ballField: BallField
    internal var gameMainNode: SKNode
    internal var gameScene: SKScene

    internal var gamePauseNode: SKNode?

    internal let powerUpMappingTablePuToDockBtn = [PowerUps.magnet: DockButtonType.magnet,
                                                   PowerUps.overlap: DockButtonType.overlap,
                                                   PowerUps.freeze: DockButtonType.freeze,
                                                   PowerUps.stopCircleRotation: DockButtonType.stopCircle]

    internal let powerUpMappingTablePuToStockLabel = [PowerUps.magnet: LabelTypes.stockLabelMagnet,
                                                      PowerUps.overlap: LabelTypes.stockLabelOverlap,
                                                      PowerUps.freeze: LabelTypes.stockLabelFreze,
                                                      PowerUps.stopCircleRotation: LabelTypes.stockLabelStopRotation]

    internal let powerUpMappingTableDockBtnToPu = [DockButtonType.magnet: PowerUps.magnet,
                                                   DockButtonType.overlap: PowerUps.overlap,
                                                   DockButtonType.freeze: PowerUps.freeze,
                                                   DockButtonType.stopCircle: PowerUps.stopCircleRotation]

    // MARK: Initializer

    init(gameScene: GameScene) {
        self.colorCircle = gameScene.colorCircle
        self.ballField = gameScene.ballField
        self.gameMainNode = gameScene.gameMainNode
        self.gameScene = gameScene

        subscribeEventBusEvents()

        setupPredefinedHandlers()
        addDifficultyLevels()
        setRandomizedDifficultyLevler()

        initializeScene()

        _ = HapticController()
    }

    func initializeScene() {
        currentScore = 0
        EventBus.shared.publish(event: ManualDockEvents(isMainMenuDock: true))
        EventBus.shared
            .publish(event: AutolabelChangeEvent(newLabelText: String(highScore), labelType: .highScoreLabel))
        EventBus.shared
            .publish(event: AutolabelChangeEvent(newLabelText: "\(GameConstants.priceSymbole) \(creditAmount)",
                                                 labelType: .moneyLabel))
        refreshPowerUpLabels()
        refreshApperance(newValue: darkModeStatusModel)
        EventBus.shared
            .publish(event: ButtonUpdateEvent(buttonType: .speakerSwitch, buttonState: isSoundOn ? .on : .off))
        EventBus.shared.publish(event: AutolabelChangeEvent(newLabelText: "GO".localized(), labelType: .goMenuLabel))
        EventBus.shared.publish(event: AutolabelChangeEvent(newLabelText: "Highscore", labelType: .highScoreTextLabel))

        checkShowTourtorial()
    }

    func checkShowTourtorial() {
        guard !tourtorialShowed else { return }
        let tourtorialStack = TutorialNode(sceneSize: gameScene.size, cardModelList: CardModelService.getData())
        tourtorialStack.zPosition = 100
        tourtorialStack.tutorialClosedHandler = {
            self.tourtorialShowed = true
        }
        gameScene.addChild(tourtorialStack)
    }

    // MARK: - Methods

    func handleDificultyHandlerChange() {
        print("Difficulty Levler: ", difficultyLevler)
        guard difficultyLevler <= gameDifficultyHandler.count else {
            handleEndlesDifficultyLevling()
            return
        }
        guard let method = gameDifficultyHandler[difficultyLevler] else { return }
        method()
    }

    func handleEndlesDifficultyLevling() {
        let state = difficultyLevler % 60

        if state == 0 {
            setRandomizedDifficultyLevler()
        }

        guard let method = gameEndlessDifficultyHandler[state] else { return }
        method()
    }

    func refreshPowerUpLabels() {
        for (key, value) in powerUpStorage {
            guard let button = powerUpMappingTablePuToDockBtn[key],
                  let labelType = powerUpMappingTablePuToStockLabel[key]
            else { return }

            if value < 1 {
                EventBus.shared.publish(event: ButtonLockEvent(buttonType: button, buttonLocked: true))
            } else {
                EventBus.shared.publish(event: ButtonLockEvent(buttonType: button, buttonLocked: false))
            }
            EventBus.shared.publish(event: LabelButtonUpdateEvent(buttonType: button, labelValue: "\(value)"))

            EventBus.shared.publish(event: AutolabelChangeEvent(newLabelText: "Stock: \(value)", labelType: labelType))
        }
    }

    internal func addCircleSegment(withColor color: GameColor) {
        guard currentColors.count <= 8 else { return }

        if remainingPossibleColors.count == 0 {
            remainingPossibleColors = GameColor.getSecondColorSet()
        }

        colorCircle.addSegment(animationSpeed: 2, color: color, completion: {})
        currentColors.append(color)
    }

    private func checkIsHighScoreReached() {
        if currentScore > highScore {
            highScore = currentScore
            if !isConfettiFired {
                isConfettiFired = true
                EventBus.shared.publish(event: ConfettiEvent())
                HighScoreConfettiComponent.fire(target: ballField)
            }
        }
    }

    func getRandomColor() -> GameColor? {
        guard !remainingPossibleColors.isEmpty else { return nil }
        let color = remainingPossibleColors.remove(at: Int.random(in: 0 ..< remainingPossibleColors.count))
        return color
    }

    func setColorCircleRotation(toRPM rpm: CGFloat) {
        if !colorCircle.isRotating {
            colorCircle.startRotation()
        }

        currentColorCircleRPM = rpm

        guard isRotationLocked == false else { return }

        colorCircle.changeRPMAnimated(to: rpm, in: 3)
    }

    internal func handleColisions(colisonEvent: CollisionEvent) {
        guard gameActive else { return }

        let ball = colisonEvent.ball
        let segment = colisonEvent.segment

        guard !isMagnetEnabled else {
            ball.removeBall()
            currentScore += 1
            checkIsHighScoreReached()
            return
        }

        if ball.color.getID() == segment.color.getID() {
            EventBus.shared.publish(event: HapticEvent(hapticLevel: .soft))
            EventBus.shared.publish(event: RightCollisionEvent())

            checkCombo(currentBall: ball)

            currentScore += combo
            ball.removeBall()
            previousBallColorID = ball.color.getID()

            difficultyLevler += 1

            checkIsHighScoreReached()

        } else {
            gameActive = false

            ballField.touchesEnded(node: ball)
            ballField.isInteractionActive = false

            ballEmitterTimer?.stop()
            EventBus.shared.publish(event: PowerUpEvent(buttonType: .stopCircle, isActive: false))
            EventBus.shared.publish(event: PowerUpEvent(buttonType: .freeze, isActive: false))
            creditAmount += difficultyLevler
            playedGames += 1

            ball.ballBlink {
                self.ballField.removeAllBalls()
                EventBus.shared.publish(event: EndEvent())
            }
        }
    }

    private func checkCombo(currentBall: Ball) {
        guard !isMagnetEnabled else {
            combo = 1
            EventBus.shared.publish(event: ScoreBoardStrokeEvent(currentComboColor: nil, animationDuration: 0.5))
            return
        }

        if previousBallColorID != nil, previousBallColorID! == currentBall.color.getID() {
            combo += 1
            EventBus.shared
                .publish(event: ScoreBoardStrokeEvent(currentComboColor: currentBall.color, animationDuration: 0.5))
        } else {
            combo = 1
            EventBus.shared.publish(event: ScoreBoardStrokeEvent(currentComboColor: nil, animationDuration: 0.5))
        }

        EventBus.shared.publish(event: ComboLevelEvent(text: "+\(combo)"))
    }

    internal func resetGameStats() {
        gameDifficultyHandler = [Int: () -> Void]()
        addDifficultyLevels()
        ballField.isInteractionActive = true
        remainingPossibleColors = GameColor.getFirstColorSet()
        currentColors = [getRandomColor() ?? .clear, getRandomColor() ?? .clear]

        colorCircle.zRotation = CGFloat.random(in: 0 ..< (.pi * 2))
        colorCircle.setupCircle(color1: currentColors[0], color2: currentColors[1])

        isConfettiFired = false
        isRotationLocked = false
        isMagnetEnabled = false

        currentColorCircleRPM = 0
        difficultyLevler = 0
        currentScore = 0
        combo = 1
        previousBallColorID = nil
    }
}
