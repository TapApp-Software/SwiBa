//
//  GameControlSystem+ComputetProperties.swift
//  SwiBa
//
//  Created by Franz Murner on 06.12.21.
//

import Foundation
import UIKit

extension GameControlSystem {
    var highScore: Int {
        get {
            return UserDefaultDatabase.shared.loadData().highscore
        }
        set {
            EventBus.shared
                .publish(event: AutolabelChangeEvent(newLabelText: String(newValue), labelType: .highScoreLabel))
            UserDefaultDatabase.shared.updateHighscore(newValue: newValue)
        }
    }

    var creditAmount: Int {
        get {
            return UserDefaultDatabase.shared.loadData().creditAmount
        }
        set {
            EventBus.shared
                .publish(event: AutolabelChangeEvent(newLabelText: "\(GameConstants.priceSymbole) \(newValue)",
                                                     labelType: .moneyLabel))
            UserDefaultDatabase.shared.updateCredit(newValue: newValue)
        }
    }

    var powerUpStorage: [PowerUps: Int] {
        get {
            return UserDefaultDatabase.shared.loadData().powerUpStorage
        }
        set {
            UserDefaultDatabase.shared.updateStorage(newValue: newValue)
            refreshPowerUpLabels()
        }
    }

    var darkModeStatusModel: DarkModeStatus {
        get {
            return UserDefaultDatabase.shared.loadData().darkModeStatus
        }
        set {
            UserDefaultDatabase.shared.updateDarkModeStatus(newValue: newValue)
            refreshApperance(newValue: newValue)
        }
    }

    var isSoundOn: Bool {
        get {
            return UserDefaultDatabase.shared.loadData().isSoundOn
        }
        set {
            UserDefaultDatabase.shared.updateIsSoundOn(newValue: newValue)
            EventBus.shared
                .publish(event: ButtonUpdateEvent(buttonType: .speakerSwitch, buttonState: newValue ? .on : .off))
        }
    }

    var playedGames: Int {
        get {
            return UserDefaultDatabase.shared.loadData().playedGames
        }
        set {
            UserDefaultDatabase.shared.updatePlayedGames(newValue: newValue)
        }
    }

    var tourtorialShowed: Bool {
        get {
            return UserDefaultDatabase.shared.loadData().tourtorialShowed
        }
        set {
            UserDefaultDatabase.shared.updateTourtorialShowed(newValue: newValue)
        }
    }
}
