//
//  GCS+CommonEventHandler.swift
//  SwiBa
//
//  Created by Franz Murner on 01.01.22.
//

import Foundation
import SpriteKit

extension GameControlSystem {
    func subscribeEventBusEvents() {
        subscribeAndHandleGoEvent()
        subscribeAndHandleWrongCollisionEvent()
        subscribeAndHandleCollisionEvent()
        subscribeAndHandleButtonPressedEvent()
        subscribeAndHandleUIInterfaceStyleUpdateNotification()
        subscribeAndHandlePowerUpAddButtonPressEvent()
        subscribeAndHandleDeletionConfirmButtonPressEvent()
        subscribeAndHandlePauseMenuCountinueButtonPressed()
        subscribeAndHandleSystemNotificationEvent()
    }

    func subscribeAndHandleGoEvent() {
        EventBus.shared.subscribe(event: .goEvent) { [self] _ in
            resetGameStats()
            ballEmitterTimer = TATimer(timeInterval: 2, timerType: .ballEmitterTimer, repeatBlock: true, block: { _ in
                let randomColorNr = Int.random(in: 0 ..< self.currentColors.count)
                self.ballField.addBall(color: self.currentColors[randomColorNr])
            })
            ballEmitterTimer?.start()
            gameActive = true
        }
    }

    func subscribeAndHandleWrongCollisionEvent() {
        EventBus.shared.subscribe(event: .wrongCollisionEvent) { _ in
            self.ballField.removeAllBalls()
            EventBus.shared.publish(event: EndEvent())
        }
    }

    func subscribeAndHandleCollisionEvent() {
        EventBus.shared.subscribe(event: .collisionEvent) { event in
            guard let colisonEvent = event as? CollisionEvent else { return }
            self.handleColisions(colisonEvent: colisonEvent)
        }
    }

    func subscribeAndHandleButtonPressedEvent() {
        EventBus.shared.subscribe(event: .buttonPressedEvent) { [self] event in
            guard let event = event as? ButtonPressedEvent,
                  let methode = self.predefinedHandlers[event.buttonType]
            else { return }

            if event.buttonType == .magnet || event.buttonType == .overlap || event.buttonType == .stopCircle || event
                .buttonType == .freeze {
                guard let powerup = powerUpMappingTableDockBtnToPu[event.buttonType],
                      let chosenPowerUpCount = powerUpStorage[powerup],
                      chosenPowerUpCount > 0
                else { return }

                methode()

                powerUpStorage[powerup]! -= 1

            } else {
                methode()
            }
        }
    }

    func subscribeAndHandleUIInterfaceStyleUpdateNotification() {
        EventBus.shared.subscribe(event: .UIInterfaceStyleUpdateNotification) { [self] event in

            guard self.darkModeStatusModel == .system else { return }
            guard let event = event as? UIInterfaceStyleUpdateNotification else { return }

            EventBus.shared.publish(event: UIInterfaceStyleUpdate(isDark: event.currentMode == .dark))

            guard gamePauseNode != nil else { return }
            changeApperanceFromPauseNode()
        }
    }

    func subscribeAndHandlePowerUpAddButtonPressEvent() {
        EventBus.shared.subscribe(event: .powerUpAddButtonPressEvent) { [self] event in
            guard let event = event as? PowerUpAddButtonPressEvent,
                  let powerUp = powerUpMappingTableDockBtnToPu[event.buttonType]
            else { return }

            powerUpStorage[powerUp]! += 1

            creditAmount -= GameConstants.prices[event.buttonType] ?? 0
            checkIfBuyable()
        }
    }

    func subscribeAndHandleDeletionConfirmButtonPressEvent() {
        EventBus.shared.subscribe(event: .deletionConfirmButtonPressEvent) { _ in
            self.deletionEventHandler()
        }
    }

    func subscribeAndHandlePauseMenuCountinueButtonPressed() {
        EventBus.shared.subscribe(event: .countinueButtonPressEvent) { [self] _ in
            handleUnpauseThroghPauseMenu()
        }
    }

    func subscribeAndHandleSystemNotificationEvent() {
        EventBus.shared.subscribe(event: .systemNotificationEvent) { [self] event in
            guard let event = event as? SystemNotificationEvent else { return }

            if event.eventType == .sceneDidBecomeActive {
                handleSystemNotificationUnpause()
            } else if event.eventType == .sceneWillResignActive || event.eventType == .sceneDidEnterBackground {
                handleSystemNotificationPause()
            }
        }
    }
}
