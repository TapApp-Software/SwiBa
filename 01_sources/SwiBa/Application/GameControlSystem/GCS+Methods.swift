//
//  GameControl+Methodes.swift
//  SwiBa
//
//  Created by Franz Murner on 04.12.21.
//

import SpriteKit

extension GameControlSystem {
    func deletionEventHandler() {
        EventBus.shared.publish(event: TAAlertCardCloseEvent())
        UserDefaultDatabase.shared.resetAppData()
        initializeScene()
    }

    func showDeletionAlert() {
        gameScene.addChild(TATouchableOverlay(size: self.gameScene.size, connectedEvent: TAAlertCardCloseEvent()))

        let alertCard = TAAlertCard(color: GameColor.secondaryBackgound,
                                    sceneSize: gameScene.size,
                                    text: "DeleteDialog".localized(),
                                    confirmationEvent: DeletionConfirmButtonPressEvent())
        alertCard.zPosition = 100

        gameScene.addChild(alertCard)
    }

    func showPowerUpStore() {
        let stockLocalizedText = "Stock".localized()

        func generateLabelText(for dockButtonType: DockButtonType) -> String {
            return "\(GameConstants.priceSymbole) \(GameConstants.prices[dockButtonType]!)"
        }

        gameScene.addChild(TATouchableOverlay(size: self.gameScene.size, connectedEvent: TABottomCardCloseEvent()))

        let tacard = PowerUpStore(sceneSize: gameScene.size)
        tacard.zPosition = 100
        gameScene.addChild(tacard)

        EventBus.shared
            .publish(event: AutolabelChangeEvent(newLabelText: generateLabelText(for: .magnet),
                                                 labelType: .priceLabelMagnet))
        EventBus.shared
            .publish(event: AutolabelChangeEvent(newLabelText: generateLabelText(for: .overlap),
                                                 labelType: .priceLabelOverlap))
        EventBus.shared
            .publish(event: AutolabelChangeEvent(newLabelText: generateLabelText(for: .stopCircle),
                                                 labelType: .priceLabelStopRotation))
        EventBus.shared
            .publish(event: AutolabelChangeEvent(newLabelText: generateLabelText(for: .freeze),
                                                 labelType: .priceLabelFreze))
        EventBus.shared
            .publish(event: AutolabelChangeEvent(newLabelText: "\(stockLocalizedText): \(powerUpStorage[.magnet]!)",
                                                 labelType: .stockLabelMagnet))
        EventBus.shared
            .publish(event: AutolabelChangeEvent(newLabelText: "\(stockLocalizedText): \(powerUpStorage[.overlap]!)",
                                                 labelType: .stockLabelOverlap))
        EventBus.shared
            .publish(event:
                AutolabelChangeEvent(newLabelText: "\(stockLocalizedText): \(powerUpStorage[.stopCircleRotation]!)",
                                     labelType: .stockLabelStopRotation))
        EventBus.shared
            .publish(event: AutolabelChangeEvent(newLabelText: "\(stockLocalizedText): \(powerUpStorage[.freeze]!)",
                                                 labelType: .stockLabelFreze))
        // swiftlint:disable shorthand_operator
        creditAmount = creditAmount + 1 - 1

        checkIfBuyable()
    }

    func checkIfBuyable() {
        EventBus.shared
            .publish(event: TAButtonChangeEvent(isEnabled: creditAmount >= GameConstants.prices[.magnet] ?? 0,
                                                buttonType: .powerUpStoreMagentAddButton))
        EventBus.shared
            .publish(event: TAButtonChangeEvent(isEnabled: creditAmount >= GameConstants.prices[.overlap] ?? 0,
                                                buttonType: .powerUpStoreOverlapAddButton))
        EventBus.shared
            .publish(event: TAButtonChangeEvent(isEnabled: creditAmount >= GameConstants.prices[.stopCircle] ?? 0,
                                                buttonType: .powerUpStoreStopCircleAddButton))
        EventBus.shared
            .publish(event: TAButtonChangeEvent(isEnabled: creditAmount >= GameConstants.prices[.freeze] ?? 0,
                                                buttonType: .powerUpStoreFreezeAddButton))
    }

    func refreshApperance(newValue: DarkModeStatus) {
        var isDark = false

        if newValue == .system {
            EventBus.shared.publish(event: UIInterfaceStyleOverride(appearance: .unspecified))
            EventBus.shared.publish(event: ButtonUpdateEvent(buttonType: .darkModeToggle,
                                                             buttonState: UITraitCollection.current
                                                                 .userInterfaceStyle == .dark ? .on : .off))
            isDark = UITraitCollection.current.userInterfaceStyle == .dark
        } else if newValue == .lockedLight {
            EventBus.shared.publish(event: UIInterfaceStyleOverride(appearance: .light))
            EventBus.shared.publish(event: ButtonUpdateEvent(buttonType: .darkModeToggle, buttonState: .lockedOff))
            isDark = false
        } else if newValue == .lockedDark {
            EventBus.shared.publish(event: UIInterfaceStyleOverride(appearance: .dark))
            EventBus.shared.publish(event: ButtonUpdateEvent(buttonType: .darkModeToggle, buttonState: .lockedOn))
            isDark = true
        }

        EventBus.shared.publish(event: UIInterfaceStyleUpdate(isDark: isDark))
    }

    func changeApperanceFromPauseNode() {
        gamePauseNode?.removeFromParent()

        gameScene.run(SKAction.wait(forDuration: 0.1)) { [self] in
            gamePauseNode = GamePausedNode(sceneSize: gameScene.size, gameMainNode: gameMainNode)

            guard let gamePauseNode = gamePauseNode else { return }

            gameScene.addChild(gamePauseNode)
        }
    }
}
