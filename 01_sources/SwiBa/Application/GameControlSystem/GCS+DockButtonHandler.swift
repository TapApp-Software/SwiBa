//
//  GameControlSystem+Extension.swift
//  SwiBa
//
//  Created by Franz Murner on 26.09.21.
//

import Foundation
import SpriteKit
import UIKit

// This extension contains the complete logic for any button interaction
extension GameControlSystem {
    func setupPredefinedHandlers() {
        predefinedHandlers[.speakerSwitch] = speakerSwitchHandler
        predefinedHandlers[.powerupStore] = powerUpStoreHandler
        predefinedHandlers[.darkModeToggle] = darkModeToggleHandler
        predefinedHandlers[.reset] = resetButtonHandler
        predefinedHandlers[.magnet] = magnetButtonHandler
        predefinedHandlers[.overlap] = noOverlapButtonHandler
        predefinedHandlers[.stopCircle] = stopCircleRotationHandler
        predefinedHandlers[.freeze] = stopBallDropHandler
    }

    func magnetButtonHandler() {
        isMagnetEnabled = true

        EventBus.shared.publish(event: ScoreBoardStrokeEvent(currentComboColor: nil, animationDuration: 0.5))

        for ball in ballField.getAllBalls() {
            guard let midPoint = colorCircle.getMidPointFrom(color: ball.color) else { return }
            ball.setBallCollision(to: false)
            ball.run(SKAction.move(to: midPoint, duration: 0.5))
        }

        Timer.scheduledTimer(withTimeInterval: 0.7, repeats: false) { _ in
            self.isMagnetEnabled = false
        }
    }

    func noOverlapButtonHandler() {
        for ball in ballField.getAllBalls() {
            ball.setBallCollision(to: false)
        }
    }

    func stopBallDropHandler() {
        ballEmitterTimer?.stop()
        isBallEmitterLocked = true

        EventBus.shared
            .publish(event: PowerUpEvent(buttonType: .freeze, isActive: true, duration: GameConstants.durationFreeze,
                                         completion: { [self] in
                                             ballEmitterTimer?.start()
                                             isBallEmitterLocked = false
                                         }))
    }

    func stopCircleRotationHandler() {
        colorCircle.stopRotation()
        isRotationLocked = true
        EventBus.shared
            .publish(event: PowerUpEvent(buttonType: .stopCircle, isActive: true,
                                         duration: GameConstants.durationStopRotation, completion: { [self] in
                                             isRotationLocked = false
                                             setColorCircleRotation(toRPM: currentColorCircleRPM)
                                         }))
    }

    func speakerSwitchHandler() {
        isSoundOn = !isSoundOn
    }

    func powerUpStoreHandler() {
        showPowerUpStore()
    }

    func darkModeToggleHandler() {
        let statusIndexTable = [DarkModeStatus.system: 0, DarkModeStatus.lockedDark: 1, DarkModeStatus.lockedLight: 2]

        let stateTable = [UIUserInterfaceStyle.dark: [DarkModeStatus.lockedLight, .system, .lockedDark],
                          UIUserInterfaceStyle.light: [.lockedDark, .lockedLight, .system]]

        let statusNr = statusIndexTable[darkModeStatusModel] ?? 0

        darkModeStatusModel = stateTable[UIScreen.main.traitCollection.userInterfaceStyle]![statusNr]
    }

    func resetButtonHandler() {
        showDeletionAlert()
    }
}
