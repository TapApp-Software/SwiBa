//
//  BitMasks.swift
//  SwiBa
//
//  Created by Franz Murner on 07.08.21.
//

import Foundation

enum BitMasks: UInt32 {
    case ball = 0x0000_0001
    case segment = 0x0000_0002
    case outerRing = 0x0000_0008
    case ballNoCollision = 0x0000_0032
}
