//
//  PowerUps.swift
//  SwiBa
//
//  Created by Franz Murner on 06.12.21.
//

import Foundation

enum PowerUps: Codable {
    case magnet
    case overlap
    case freeze
    case stopCircleRotation
}
