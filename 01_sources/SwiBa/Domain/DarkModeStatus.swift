//
//  DarkModeStatus.swift
//  SwiBa
//
//  Created by Franz Murner on 06.12.21.
//

import Foundation

enum DarkModeStatus: Codable {
    case system
    case lockedDark
    case lockedLight
}
