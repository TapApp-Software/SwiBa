//
//  GameDataModel.swift
//  SwiBa
//
//  Created by Franz Murner on 05.12.21.
//

import Foundation

struct GameDataModel: Codable {
    var highscore: Int = 0
    var creditAmount: Int = 0

    var powerUpStorage = [PowerUps.magnet: 2,
                          PowerUps.overlap: 2,
                          PowerUps.freeze: 2,
                          PowerUps.stopCircleRotation: 2]

    var darkModeStatus: DarkModeStatus = .system
    var isSoundOn: Bool = true
    var playedGames = 0
    var tourtorialShowed = false
}
