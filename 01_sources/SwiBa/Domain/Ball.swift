//
//  Ball.swift
//  SwiBa
//
//  Created by Franz Murner on 30.07.21.
//

import SpriteKit

class Ball {
    private var colorID: Int
    private var size: CGSize

    init(colorID: Int, size: CGSize) {
        self.colorID = colorID
        self.size = size
    }
}
