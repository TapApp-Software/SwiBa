//
//  TALabelNode.swift
//  SwiBa
//
//  Created by Franz Murner on 03.10.21.
//

import SpriteKit

class TALabelNode: SKNode {
    // MARK: - Nodes

    private let subscriberID = UUID()
    private var labelNode: SKLabelNode
    public var constranit: TAConstranin

    // MARK: - Parameters

    /// This parameter change the behavior of the setLabelText autoresizing.
    /// False means the font size doesn't changed if the text is to big for the label background
    /// Default: true
    public var autoresizeFont = true {
        didSet {
            autoresize()
        }
    }

    override var position: CGPoint {
        didSet {
            constranit.relativePosition = position
            super.position = position
        }
    }

    public var currentLabelSize: CGSize?
    private var maxLabelSize: CGSize

    private var labelType: LabelTypes?

    // MARK: - Initializer

    /// Default Initializer
    /// - Parameters:
    ///   - maxLabelSize: The maximal label size, if the text bigger as this size the font size will be autoresized
    ///   - labelType: The type of the label is needed for the EventBus. If it is nil no EventBus Event will be created.
    init(maxLabelSize: CGSize, labelType: LabelTypes? = nil) {
        self.maxLabelSize = maxLabelSize
        self.labelType = labelType

        labelNode = SKLabelNode(fontNamed: GameConstants.font)
        labelNode.horizontalAlignmentMode = .center
        labelNode.verticalAlignmentMode = .center
        labelNode.fontSize = maxLabelSize.height
        labelNode.text = "N/A"
        labelNode.numberOfLines = 0

        constranit = TAConstranin(size: maxLabelSize)

        super.init()

        self.currentLabelSize = labelNode.frame.size
        self.addChild(labelNode)

        subscribeEvents()
        self.labelNode.fontColor = GameColor.mainLabel.getColor()
    }

    init(maxLabelSize: CGSize, text: String, labelType: LabelTypes? = nil,
         horizontalAlignmentMode: SKLabelHorizontalAlignmentMode = .center) {
        self.maxLabelSize = maxLabelSize
        self.labelType = labelType

        labelNode = SKLabelNode(fontNamed: GameConstants.font)
        labelNode.horizontalAlignmentMode = horizontalAlignmentMode
        labelNode.verticalAlignmentMode = .center
        labelNode.fontSize = maxLabelSize.height
        labelNode.text = text
        labelNode.numberOfLines = 0

        constranit = TAConstranin(size: maxLabelSize)

        super.init()

        self.currentLabelSize = labelNode.frame.size
        self.addChild(labelNode)

        subscribeEvents()
        autoresize()
        setColor()
    }

    init(startFontSize: CGFloat, text: String, labelType: LabelTypes? = nil,
         horizontalAlignmentMode: SKLabelHorizontalAlignmentMode = .center) {
        labelNode = SKLabelNode(fontNamed: GameConstants.font)
        labelNode.horizontalAlignmentMode = horizontalAlignmentMode
        labelNode.verticalAlignmentMode = .center
        labelNode.fontSize = startFontSize
        labelNode.text = text
        labelNode.numberOfLines = 0

        self.maxLabelSize = labelNode.frame.size
        self.currentLabelSize = labelNode.frame.size
        self.labelType = labelType

        constranit = TAConstranin(size: currentLabelSize ?? CGSize.zero)

        super.init()

        self.addChild(labelNode)

        setColor()

        subscribeEvents()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    deinit {
        unsubscribeEvents()
    }

    // MARK: - Methods

    private func setColor() {
        self.labelNode.fontColor = GameColor.mainLabel.getColor()
    }

    private func subscribeEvents() {
        EventBus.shared.subscribe(subscriberId: subscriberID, event: .UIInterfaceStyleUpdate) { _ in
            self.setColor()
        }

        guard let labelType = labelType else { return }

        EventBus.shared.subscribe(subscriberId: subscriberID, event: .autolabelChangeEvent) { event in
            guard let event = event as? AutolabelChangeEvent,
                  labelType == event.labelType
            else { return }

            self.setLabelText(with: event.newLabelText)
        }
    }

    private func unsubscribeEvents() {
        EventBus.shared.unsubscribeAll(subscriberId: subscriberID)
    }

    /// Set the text of the label. If the option autoresize true (default value) the font size will be adjusted depend on the text length
    /// - Parameter text: "Your label text"
    public func setLabelText(with text: String) {
        labelNode.text = text
        autoresize()
    }

    private func autoresize() {
        var newFontSize = maxLabelSize.height

        let labelNodeDifference = maxLabelSize.width - labelNode.frame.size.width - 50

        if labelNodeDifference < 0, autoresizeFont == true {
            let labelRatio = labelNode.frame.height / maxLabelSize.width
            newFontSize = maxLabelSize.height + labelNodeDifference / 2 * labelRatio
        }

        labelNode.fontSize = newFontSize > 0 ? newFontSize : 1

        currentLabelSize = labelNode.frame.size
    }
}
