//
//  TAActions.swift
//  SwiBa
//
//  Created by Franz Murner on 25.11.21.
//

import SpriteKit

class TAActions {
    static func scaleFlex(from oldValue: CGFloat, to newValue: CGFloat, withDuration duration: TimeInterval,
                          withTimingFunction timingFunction: @escaping (CGFloat) -> CGFloat) -> SKAction {
        let delta = newValue - oldValue

        return SKAction.customAction(withDuration: duration) { node, timer in
            node.setScale(oldValue + delta * timingFunction(timer / duration))
        }
    }
}
