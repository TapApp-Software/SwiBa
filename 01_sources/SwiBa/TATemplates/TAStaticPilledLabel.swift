//
//  TAStaticPilledLabel.swift
//  SwiBa
//
//  Created by Franz Murner on 04.12.21.
//

import SpriteKit

class TAStaticPilledLabel: TANode {
    // MARK: - Nodes

    private var autoLabel: TALabelNode?
    private var pill: SKShapeNode?

    // MARK: - Parameters

    private var text: String
    private var nodeSize: CGSize

    public var constraint: TAConstranin
    private var fillColor: GameColor

    override var position: CGPoint {
        didSet {
            constraint.relativePosition = position
            super.position = position
        }
    }

    // MARK: - Initializer

    init(size: CGSize, text: String, fillColor: GameColor) {
        self.nodeSize = size
        self.fillColor = fillColor
        constraint = TAConstranin(size: size)

        self.text = text

        super.init()

        drawPilledLabel()
        subscribeEvents()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    deinit {
        unsubscribeEvents()
    }

    // MARK: - Methods

    private func subscribeEvents() {
        EventBus.shared.subscribe(subscriberId: subscriberID, event: .UIInterfaceStyleUpdate) { [weak self] _ in
            self?.setColor()
        }
    }

    private func unsubscribeEvents() {
        EventBus.shared.unsubscribeAll(subscriberId: subscriberID)
    }

    public func updateConstraint() {
        drawPilledLabel()
    }

    private func drawPilledLabel() {
        autoLabel?.removeFromParent()
        autoLabel = TALabelNode(maxLabelSize: constraint.absoluteSizeWithPadding, text: text)
        autoLabel?.zPosition = 3

        guard let autoLabel = autoLabel else {
            return
        }

        self.addChild(autoLabel)

        drawPill(size: nodeSize)
    }

    private func drawPill(size: CGSize) {
        let origin = CGPoint(x: -size.width / 2, y: -size.height / 2)

        pill?.removeFromParent()
        pill = SKShapeNode(rect: CGRect(origin: origin, size: size), cornerRadius: size.height / 2)
        pill?.zPosition = 2

        guard let pill = pill else {
            return
        }

        self.addChild(pill)

        setColor()
    }

    private func setColor() {
        pill?.fillColor = fillColor.getColor()
        pill?.strokeColor = .clear
    }

    override func getSize() -> CGSize {
        return nodeSize
    }

    override func colorOverride(fillColor: GameColor, overrideAction: TANode.ColorOverrideAction) {
        switch overrideAction {
        case .onDisable:
            self.fillColor = fillColor
            setColor()
        case .onEnable:
            self.fillColor = fillColor
            setColor()
        case .onTouch:
            self.fillColor = fillColor
            setColor()
        case .onSet:
            self.fillColor = fillColor
            setColor()
        }
    }
}
