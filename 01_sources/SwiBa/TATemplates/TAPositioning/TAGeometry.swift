//
//  TAPadding.swift
//  SwiBa
//
//  Created by Franz Murner on 23.11.21.
//

import CoreGraphics
struct TAGeometry {
    private var iLeft: CGFloat = 0
    private var iRight: CGFloat = 0
    private var iBottom: CGFloat = 0
    private var iTop: CGFloat = 0

    public var left: CGFloat {
        get {
            return getValue(value: iLeft, valueForDimension: globalX)
        }
        set {
            iLeft = newValue
        }
    }

    public var right: CGFloat {
        get {
            return getValue(value: iRight, valueForDimension: globalX)
        }
        set {
            iRight = newValue
        }
    }

    public var bottom: CGFloat {
        get {
            return getValue(value: iBottom, valueForDimension: globalY)
        }
        set {
            iBottom = newValue
        }
    }

    public var top: CGFloat {
        get {
            return getValue(value: iTop, valueForDimension: globalY)
        }
        set {
            iTop = newValue
        }
    }

    public var global: CGFloat = 0
    public var globalY: CGFloat = 0
    public var globalX: CGFloat = 0

    private func getValue(value: CGFloat, valueForDimension: CGFloat) -> CGFloat {
        if value == 0 {
            if valueForDimension == 0 {
                return global
            } else {
                return valueForDimension
            }
        } else {
            return value
        }
    }
}
