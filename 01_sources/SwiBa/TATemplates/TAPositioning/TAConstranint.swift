//
//  TAConstranint.swift
//  SwiBa
//
//  Created by Franz Murner on 23.11.21.
//

import CoreGraphics

class TAConstranin {
    public var padding = TAGeometry()
    public var margin = TAGeometry()

    public var relativePosition = CGPoint.zero

    private var borders = TAGeometry()

    var relativePositionOfLeftBorder: CGFloat {
        return relativePosition.x + absoluteLeftMargin
    }

    var relativePositionOfRightBorder: CGFloat {
        return relativePosition.x + absoluteRightMargin
    }

    var relativePositionOfTopBorder: CGFloat {
        return relativePosition.y + absoluteTopMargin
    }

    var relativePositionOfBottomBorder: CGFloat {
        return relativePosition.y + absoluteBottomMargin
    }

    var absoluteSizeWithPadding: CGSize {
        return CGSize(width: -self.absoluteLeftPadding + self.absoluteRightPadding,
                      height: -self.absoluteBottomPadding + self.absoluteTopPadding)
    }

    var absoluteLeftMargin: CGFloat {
        return borders.left - margin.left
    }

    var absoluteRightMargin: CGFloat {
        return borders.right + margin.right
    }

    var absoluteTopMargin: CGFloat {
        return borders.top + margin.top
    }

    var absoluteBottomMargin: CGFloat {
        return borders.bottom - margin.bottom
    }

    var absoluteLeftPadding: CGFloat {
        return borders.left + padding.left
    }

    var absoluteRightPadding: CGFloat {
        return borders.right - padding.right
    }

    var absoluteTopPadding: CGFloat {
        return borders.top - padding.top
    }

    var absoluteBottomPadding: CGFloat {
        return borders.bottom + padding.bottom
    }

    internal init(size: CGSize) {
        setSize(size: size)
    }

    public func setNewSize(size: CGSize) {
        setSize(size: size)
    }

    private func setSize(size: CGSize) {
        borders.left = -size.width / 2
        borders.right = size.width / 2
        borders.top = size.height / 2
        borders.bottom = -size.height / 2
    }
}
