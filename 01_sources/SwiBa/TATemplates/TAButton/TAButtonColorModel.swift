//
//  TAButtonColorModel.swift
//  SwiBa
//
//  Created by Franz Murner on 06.01.22.
//

import Foundation

struct TAButtonColorModel {
    var onNormal: GameColor
    var onPressed: GameColor
    var onDisabled: GameColor
}
