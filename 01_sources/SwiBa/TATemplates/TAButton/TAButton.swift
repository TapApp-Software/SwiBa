//
//  TAButton.swift
//  SwiBa
//
//  Created by Franz Murner on 22.11.21.
//
import SpriteKit

/// This node is a wrapper. The wrapper creates a button from every node
class TAButton: TANode {
    // MARK: Properties

    let subscriberId = UUID()
    var buttonType: ButtonType?

    var mainColor: GameColor?
    var onTouchColor: GameColor?
    var onDisabledColor: GameColor?

    private(set) var isButtonPressed = false {
        didSet {
            if isButtonPressed {
                onPress()
            } else {
                onLeft()
            }
        }
    }

    private(set) var isButtonEnabled = true {
        didSet {
            if isButtonEnabled {
                enableButton()
            } else {
                disableButton()
            }
        }
    }

    /// Set if you need a Eventbus Event for touchesBegan
    public var touchBeganHandler: IEvent?
    /// Set if you need a Eventbus Event for touchesCanceld
    public var touchCanceledHandler: IEvent?
    /// Set if you need a Eventbus Event for touchesEndet
    public var touchEndetHandler: IEvent?

    public var touchBeganClosure: () -> Void = {}
    public var touchCanceledClosure: () -> Void = {}
    public var touchEndetClosure: () -> Void = {}
    /// Blend Button on press. Default: true
    public var blendOnTouch = true
    /// Play haptic event on press. Default: false
    public var hapticOnTouch = false
    /// Haptic level on press. Default: soft
    public var hapticLevel: UIImpactFeedbackGenerator.FeedbackStyle = .soft

    public var constraint: TAConstranin

    override var position: CGPoint {
        didSet {
            constraint.relativePosition = position
            super.position = position
        }
    }

    // MARK: Nodes

    private var backgroundNode: TANode {
        didSet {
            oldValue.removeFromParent()
            addChild(backgroundNode)
        }
    }

    // MARK: Initalizer

    init(backgroundNode: TANode, buttonType: ButtonType? = nil, mainColor: GameColor? = nil,
         onTouchColor: GameColor? = nil, onDisabledColor: GameColor? = nil) {
        self.mainColor = mainColor
        self.onTouchColor = onTouchColor
        self.onDisabledColor = onDisabledColor
        self.backgroundNode = backgroundNode
        constraint = TAConstranin(size: backgroundNode.getSize())
        self.buttonType = buttonType
        super.init()
        self.isUserInteractionEnabled = true
        self.addChild(backgroundNode)

        if let mainColor = mainColor {
            backgroundNode.colorOverride(fillColor: mainColor, overrideAction: .onSet)
        }

        guard buttonType != nil else { return }
        subscribeEvents()
    }

    convenience init(backgroundNode: TANode, buttonType: ButtonType? = nil, colorModel: TAButtonColorModel) {
        self.init(backgroundNode: backgroundNode,
                  buttonType: buttonType,
                  mainColor: colorModel.onNormal,
                  onTouchColor: colorModel.onPressed,
                  onDisabledColor: colorModel.onDisabled)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    deinit {
        unsubscribeEvents()
    }

    // MARK: Methods

    private func subscribeEvents() {
        EventBus.shared.subscribe(subscriberId: subscriberId, event: .taButtonChangeEvent) { [weak self] event in
            guard let taButtonChangeEvent = event as? TAButtonChangeEvent,
                  let buttonType = self?.buttonType,
                  buttonType == taButtonChangeEvent.buttonType
            else { return }

            self?.isButtonEnabled = taButtonChangeEvent.isEnabled
        }
    }

    private func unsubscribeEvents() {
        EventBus.shared.unsubscribeAll(subscriberId: subscriberId)
    }

    private func disableButton() {
        isUserInteractionEnabled = false

        guard let onDisabledColor = onDisabledColor else {
            return
        }
        backgroundNode.colorOverride(fillColor: onDisabledColor, overrideAction: .onDisable)
    }

    private func enableButton() {
        isUserInteractionEnabled = true

        guard let mainColor = mainColor else {
            return
        }

        backgroundNode.colorOverride(fillColor: mainColor, overrideAction: .onEnable)
    }

    private func onPress() {
        guard let onTouchColor = onTouchColor else {
            return
        }

        backgroundNode.colorOverride(fillColor: onTouchColor, overrideAction: .onTouch)
    }

    private func onLeft() {
        if isButtonEnabled {
            enableButton()
        } else {
            disableButton()
        }
    }

    private func triggerTouchHandler(event: IEvent?) {
        guard let touchHandlerEvent = event else { return }
        EventBus.shared.publish(event: touchHandlerEvent)
    }

    private func playHapticEvent() {
        guard hapticOnTouch else { return }
        EventBus.shared.publish(event: HapticEvent(hapticLevel: hapticLevel))
    }

    override func getSize() -> CGSize {
        return self.backgroundNode.getSize()
    }
}

// MARK: Touchhandler

extension TAButton {
    override func touchesBegan(_: Set<UITouch>, with _: UIEvent?) {
        // isButtonPressed = true
        isButtonPressed = true
        triggerTouchHandler(event: touchBeganHandler)
        playHapticEvent()
        touchBeganClosure()
    }

    override func touchesCancelled(_: Set<UITouch>, with _: UIEvent?) {
        isButtonPressed = false
        triggerTouchHandler(event: touchCanceledHandler)
        playHapticEvent()
        touchCanceledClosure()
    }

    override func touchesEnded(_ touches: Set<UITouch>, with _: UIEvent?) {
        for touch in touches {
            let location = touch.location(in: self)

            if backgroundNode.contains(location) {
                triggerTouchHandler(event: touchEndetHandler)
                touchEndetClosure()
            } else {
                triggerTouchHandler(event: touchCanceledHandler)
                touchCanceledClosure()
            }

            playHapticEvent()

            isButtonPressed = false
        }
    }
}
