//
//  TAConstraints.swift
//  SwiBa
//
//  Created by Franz Murner on 05.01.22.
//

import SpriteKit

public class TAConstraint {
    // MARK: - Nodes

    internal var sourceNode: TANode?

    // MARK: - Parameters

    internal var offset: CGFloat = 0
    internal var inset: CGFloat = 0
    internal var direction: Direction
    internal var constraintType: ConstraintType
    internal var sourceNodeSize: CGSize {
        guard let sourceNode = sourceNode else {
            return CGSize.zero
        }
        return sourceNode.getSize()
    }

    // MARK: - Initializer

    private init(sourceNode: TANode, direction: Direction, offset: CGFloat, constraintType: ConstraintType) {
        self.constraintType = constraintType
        self.offset = offset
        self.sourceNode = sourceNode
        self.direction = direction
    }

    private init(inset: CGFloat, direction: Direction, constraintType: ConstraintType) {
        self.constraintType = constraintType
        self.inset = inset
        self.direction = direction
    }

    private init(offset: CGFloat, direction: Direction, constraintType: ConstraintType) {
        self.constraintType = constraintType
        self.offset = offset
        self.direction = direction
    }

    private init(sourceNode: TANode, direction: Direction, inset: CGFloat, constraintType: ConstraintType) {
        self.constraintType = constraintType
        self.inset = inset
        self.sourceNode = sourceNode
        self.direction = direction
    }

    private init(sourceNode: TANode, constraintType: ConstraintType) {
        self.sourceNode = sourceNode
        self.direction = .right
        self.constraintType = constraintType
    }

    // MARK: - Methods

    /// Erstellt eine Verbinung zwischen zwei Nodes. Die beiden Nodes werden aneineander ausgerichtet. Das heißt sie sind ohne Abstand benachbart (überschreibend)
    /// - Parameters:
    ///   - sourceNode: Die Node zu der die Abstandsbeziehug aufgebaut werden soll...
    ///   - direction: Die Richtung in der die Abstandsbeziehung und der Offset angewendet werden soll...
    ///   - offset: Offset also der Abstand zwischen den beiden Nodes (auch Negative werte möglich)
    /// - Returns: Gibt eine Instanz auf ein Constraint zurück
    public class func setPartner(sourceNode: TANode, direction: Direction,
                                 withOffset offset: CGFloat = 0) -> TAConstraint {
        return TAConstraint(sourceNode: sourceNode, direction: direction, offset: offset, constraintType: .setPartner)
    }

    /// Erstellt eine Verbinung zwischen zwei Nodes. Die Nodes sollen dann innernander Positioniert werden. Die eine Node liegt innerhalb direkt an dem Rand der anderen Node (überschreibend)
    /// - Parameters:
    ///   - sourceNode: Die Node zu der die Abstandsbeziehug aufgebaut werden soll...
    ///   - direction: Die Richtung in der die Abstandsbeziehung und der Inset angewendet werden soll...
    ///   - inset: Inset auf der Seite auf der die Node zum Rand der anderen Node eingerückt werden soll (auch Negative werte möglich)
    /// - Returns: Gibt eine Instanz auf ein Constraint zurück
    public class func setContainingPartner(sourceNode: TANode, direction: Direction,
                                           withInset inset: CGFloat = 0) -> TAConstraint {
        return TAConstraint(sourceNode: sourceNode, direction: direction, inset: inset,
                            constraintType: .setContainingPartner)
    }

    public class func setWhenPartnerIsParent(sourceNode: TANode, direction: Direction,
                                             withInset inset: CGFloat = 0) -> TAConstraint {
        return TAConstraint(sourceNode: sourceNode, direction: direction, inset: inset,
                            constraintType: .setPartnerAsParent)
    }

    /// Fügt der Node einen Offset in die angegebene Richtung hinzu (Nicht überschreibend)
    /// - Parameters:
    ///   - offset: Der grad der verschiebung die hinzugefügt wird (auch Negative werte möglich)
    ///   - direction: Die Richtung in der die Node bewegt wird
    /// - Returns: Gibt eine Instanz auf ein Constraint zurück
    public class func setAdditonalOffset(_ offset: CGFloat, direction: Direction) -> TAConstraint {
        return TAConstraint(offset: offset, direction: direction, constraintType: .setOffset)
    }

    /// Fügt der Node einen Offset in die angegebene Richtung hinzu (Nicht überschreibend)
    /// - Parameters:
    ///   - inset: Der Grad der verschiebung die hinzugefügt wird (auch Negative werte möglich)
    ///   - direction: Die entgegengesetzte Richtung in der die Node bewegt wird
    /// - Returns: Gibt eine Instanz auf ein Constraint zurück
    public class func setAdditonalInset(_ inset: CGFloat, direction: Direction) -> TAConstraint {
        return TAConstraint(inset: inset, direction: direction, constraintType: .setInset)
    }

    public class func setSameYPosition(like sourceNode: TANode) -> TAConstraint {
        return TAConstraint(sourceNode: sourceNode, constraintType: .useSameYPosition)
    }

    public class func setSameXPosition(like sourceNode: TANode) -> TAConstraint {
        return TAConstraint(sourceNode: sourceNode, constraintType: .useSameXPosition)
    }

    /// Wendet die übergebenen Constraints auf die angegebene Node an
    /// - Parameters:
    ///   - constraints: Constraints aus der TANode
    ///   - target: Die Node auf der die Constraints ausgeführt werden...
    public class func processingConstraintConnection(constraints: [TAConstraint], target: TANode) {
        for constraint in constraints {
            switch constraint.constraintType {
            case .setPartner:
                processingPartner(constraint: constraint, target: target)
            case .setOffset:
                processingOffset(constraint: constraint, target: target)
            case .setContainingPartner:
                processingContainingPartner(constraint: constraint, target: target)
            case .setInset:
                processingInset(constraint: constraint, target: target)
            case .useSameYPosition:
                processingSameY(constraint: constraint, target: target)
            case .useSameXPosition:
                processingSameX(constraint: constraint, target: target)
            case .setPartnerAsParent:
                processingPartnerAsParent(constraint: constraint, target: target)
            }
        }
    }
}
