//
//  Direction.swift
//  TASKKit
//
//  Created by Franz Murner on 06.01.22.
//
public enum Direction {
    case left
    case right
    case top
    case bottom
}
