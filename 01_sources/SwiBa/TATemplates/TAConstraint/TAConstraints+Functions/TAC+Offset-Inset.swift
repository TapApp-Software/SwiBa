//
//  TAC+Offset-Inset.swift
//  TASKKit
//
//  Created by Franz Murner on 06.01.22.
//
import SpriteKit

extension TAConstraint {
    class func processingOffset(constraint: TAConstraint, target: TANode) {
        switch constraint.direction {
        case .left:
            target.position.x -= constraint.offset
        case .right:
            target.position.x += constraint.offset
        case .top:
            target.position.y += constraint.offset
        case .bottom:
            target.position.y -= constraint.offset
        }
    }

    class func processingInset(constraint: TAConstraint, target: TANode) {
        switch constraint.direction {
        case .left:
            target.position.x += constraint.inset
        case .right:
            target.position.x -= constraint.inset
        case .top:
            target.position.y -= constraint.inset
        case .bottom:
            target.position.y += constraint.inset
        }
    }
}
