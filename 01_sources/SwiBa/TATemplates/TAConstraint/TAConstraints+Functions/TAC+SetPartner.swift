//
//  TAC+setPartner.swift
//  TASKKit
//
//  Created by Franz Murner on 06.01.22
//

import SpriteKit
extension TAConstraint {
    internal class func processingPartner(constraint: TAConstraint, target: TANode) {
        let targetSize = target.getSize()

        switch constraint.direction {
        case .left:
            target.position.x = constraint.sourceNode!.position.x -
                constraint.sourceNodeSize.width / 2 -
                targetSize.width / 2 -
                constraint.offset
        case .right:
            target.position.x = constraint.sourceNode!.position.x +
                constraint.sourceNodeSize.width / 2 +
                targetSize.width / 2 +
                constraint.offset
        case .top:
            target.position.y = constraint.sourceNode!.position.y +
                constraint.sourceNodeSize.height / 2 +
                targetSize.height / 2 +
                constraint.offset
        case .bottom:
            target.position.y = constraint.sourceNode!.position.y -
                constraint.sourceNodeSize.height / 2 -
                targetSize.height / 2 -
                constraint.offset
        }
    }

    internal class func processingContainingPartner(constraint: TAConstraint, target: TANode) {
        switch constraint.direction {
        case .left:
            target.position.x = -getAbsoluteInsetXPosition(of: constraint, target: target)
        case .right:
            target.position.x = getAbsoluteInsetXPosition(of: constraint, target: target)
        case .top:
            target.position.y = getAbsoluteInsetYPosition(of: constraint, target: target)
        case .bottom:
            target.position.y = -getAbsoluteInsetYPosition(of: constraint, target: target)
        }
    }

    internal class func processingPartnerAsParent(constraint: TAConstraint, target: TANode) {
        switch constraint.direction {
        case .left:
            target.position.x = -calculateInternChildXPosition(constraint: constraint, target: target)
        case .right:
            target.position.x = calculateInternChildXPosition(constraint: constraint, target: target)
        case .top:
            target.position.y = calculateInternChildYPosition(constraint: constraint, target: target)
        case .bottom:
            target.position.y = -calculateInternChildYPosition(constraint: constraint, target: target)
        }
    }

    private class func calculateInternChildXPosition(constraint: TAConstraint, target: TANode) -> CGFloat {
        let targetSizeWidth = target.getSize().width

        return constraint.sourceNodeSize.width / 2 - targetSizeWidth / 2 - constraint.inset
    }

    private class func calculateInternChildYPosition(constraint: TAConstraint, target: TANode) -> CGFloat {
        let targetSizeWidth = target.getSize().height

        return constraint.sourceNodeSize.height / 2 - targetSizeWidth / 2 - constraint.inset
    }

    private class func getAbsoluteXPosition(of constaint: TAConstraint, target: TANode) -> CGFloat {
        let targetSize = target.getSize()

        let borderPosition = constaint.sourceNodeSize.width / 2 + constaint.sourceNode!.position.x + targetSize
            .width / 2

        return borderPosition + constaint.offset
    }

    private class func getAbsoluteYPosition(of constraint: TAConstraint, target: TANode) -> CGFloat {
        let targetSize = target.getSize()

        let borderPosition = constraint.sourceNode!.position.y + constraint.sourceNodeSize.height / 2 + targetSize
            .height / 2
        return borderPosition + constraint.offset
    }

    private class func getAbsoluteInsetXPosition(of constaint: TAConstraint, target: TANode) -> CGFloat {
        let targetSize = target.getSize()

        let borderPosition = constaint.sourceNodeSize.width / 2 + constaint.sourceNode!.position.x - targetSize
            .width / 2

        return borderPosition - constaint.inset
    }

    private class func getAbsoluteInsetYPosition(of constaint: TAConstraint, target: TANode) -> CGFloat {
        let targetSize = target.getSize()

        let borderPosition = constaint.sourceNodeSize.height / 2 + constaint.sourceNode!.position.y - targetSize
            .height / 2
        return borderPosition - constaint.offset
    }
}
