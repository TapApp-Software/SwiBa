//
//  TAC+UserSameYPosition.swift
//  TASKKit
//
//  Created by Franz Murner on 06.01.22.
//

import SpriteKit

extension TAConstraint {
    class func processingSameY(constraint: TAConstraint, target: TANode) {
        guard let sourceNode = constraint.sourceNode else { return }
        target.position.y = sourceNode.position.y
    }

    class func processingSameX(constraint: TAConstraint, target: TANode) {
        guard let sourceNode = constraint.sourceNode else { return }
        target.position.x = sourceNode.position.x
    }
}
