//
//  ConstraintType.swift
//  TASKKit
//
//  Created by Franz Murner on 06.01.22.
//
internal enum ConstraintType {
    case setPartner
    case setOffset
    case setContainingPartner
    case setPartnerAsParent
    case setInset
    case useSameYPosition
    case useSameXPosition
}
