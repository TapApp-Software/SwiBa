//
//  TAImageNode.swift
//  SwiBa
//
//  Created by Franz Murner on 07.01.22.
//

import SpriteKit

class TAImageNode: TANode {
    // MARK: - Nodes

    var spriteNode = SKSpriteNode()

    // MARK: - Parameters

    private var width: CGFloat?
    private var height: CGFloat?

    var imageName: String {
        didSet {
            setImage()
        }
    }

    // MARK: - Initializer

    init(imageName: String, size: CGSize) {
        self.width = size.width
        self.height = size.height
        self.imageName = imageName
        super.init()
        self.addChild(spriteNode)
        subscribeEvents()
        setImage()
    }

    init(imageName: String, width: CGFloat) {
        self.width = width
        self.imageName = imageName
        super.init()
        self.addChild(spriteNode)
        subscribeEvents()
        setImage()
    }

    init(imageName: String, height: CGFloat) {
        self.height = height
        self.imageName = imageName
        super.init()
        self.addChild(spriteNode)
        subscribeEvents()
        setImage()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    deinit {
        unsubscribeEvents()
    }

    // MARK: - Methods

    private func setImage(isDark: Bool = false) {
        let apperanceString = isDark ? "Dark" : "Light"

        let texture = SKTexture(imageNamed: "\(imageName)-\(apperanceString)")

        spriteNode.texture = texture

        if let width = width, let height = height {
            spriteNode.size = CGSize(width: width, height: height)
        } else if let width = width {
            let textureRatio = texture.size().height / texture.size().width
            spriteNode.size = CGSize(width: width, height: width * textureRatio)
        } else if let height = height {
            let textureRatio = texture.size().width / texture.size().height
            spriteNode.size = CGSize(width: height * textureRatio, height: height)
        }
    }

    private func subscribeEvents() {
        EventBus.shared.subscribe(subscriberId: subscriberID, event: .UIInterfaceStyleUpdate) { [weak self] event in
            guard let event = event as? UIInterfaceStyleUpdate else { return }
            self?.setImage(isDark: event.isDark)
        }
    }

    private func unsubscribeEvents() {
        EventBus.shared.unsubscribeAll(subscriberId: subscriberID)
    }
}
