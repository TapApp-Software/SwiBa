//
//  TAPilledLabel.swift
//  SwiBa
//
//  Created by Franz Murner on 30.11.21.
//

import SpriteKit

class TAPilledLabel: TANode {
    // MARK: - Nodes

    private var autoLabel: TALabelNode?
    private var pill: SKShapeNode?

    // MARK: - Parameters

    private var labelType: LabelTypes?
    private var text: String {
        didSet {
            drawPilledLabel()
        }
    }

    public var constraint: TAConstranin
    private var maxSize: CGSize
    private var currentSize: CGSize = .zero
    private var fillColor: GameColor

    override var position: CGPoint {
        didSet {
            constraint.relativePosition = position
            super.position = position
        }
    }

    // MARK: - Initializer

    init(maxSize: CGSize, text: String, fillColor: GameColor, labelType: LabelTypes? = nil) {
        self.maxSize = maxSize
        self.fillColor = fillColor
        constraint = TAConstranin(size: maxSize)

        self.text = text
        self.labelType = labelType

        super.init()

        drawPilledLabel()
        subscribeEvents()
    }

    convenience init(maxSize: CGSize, labelType: LabelTypes, fillColor: GameColor) {
        self.init(maxSize: maxSize, text: "N/A", fillColor: fillColor, labelType: labelType)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    deinit {
        unsubscribeEvents()
    }

    // MARK: - Methods

    private func subscribeEvents() {
        EventBus.shared.subscribe(subscriberId: subscriberID, event: .UIInterfaceStyleUpdate) { [weak self] _ in
            self?.setColor()
        }

        EventBus.shared.subscribe(subscriberId: subscriberID, event: .autolabelChangeEvent) { [weak self] event in
            guard let event = event as? AutolabelChangeEvent,
                  self?.labelType == event.labelType
            else { return }

            self?.text = event.newLabelText
        }
    }

    private func unsubscribeEvents() {
        EventBus.shared.unsubscribeAll(subscriberId: subscriberID)
    }

    public func updateConstraint() {
        drawPilledLabel()
    }

    private func drawPilledLabel() {
        autoLabel?.removeFromParent()
        autoLabel = TALabelNode(maxLabelSize: constraint.absoluteSizeWithPadding, text: text)
        autoLabel?.zPosition = 3

        guard let autoLabel = autoLabel else {
            return
        }

        self.addChild(autoLabel)

        currentSize = CGSize(width: (autoLabel.currentLabelSize?.width ?? maxSize.width) + constraint.padding
            .left + constraint.padding
            .right,
            height: maxSize.height)

        drawPill(size: currentSize)
    }

    private func drawPill(size: CGSize) {
        let origin = CGPoint(x: -size.width / 2, y: -size.height / 2)

        pill?.removeFromParent()
        pill = SKShapeNode(rect: CGRect(origin: origin, size: size), cornerRadius: size.height / 2)
        pill?.zPosition = 2

        guard let pill = pill else {
            return
        }

        self.addChild(pill)

        setColor()
    }

    private func setColor() {
        pill?.fillColor = fillColor.getColor()
        pill?.strokeColor = .clear
    }

    override func getSize() -> CGSize {
        return currentSize
    }

    override func colorOverride(fillColor: GameColor, overrideAction: TANode.ColorOverrideAction) {
        switch overrideAction {
        case .onDisable:
            self.fillColor = fillColor
            setColor()
        case .onEnable:
            self.fillColor = fillColor
            setColor()
        case .onTouch:
            self.fillColor = fillColor
            setColor()
        case .onSet:
            self.fillColor = fillColor
            setColor()
        }
    }
}
