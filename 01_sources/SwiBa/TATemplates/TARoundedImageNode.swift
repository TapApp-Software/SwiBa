//
//  TARoundedImageNode.swift
//  SwiBa
//
//  Created by Franz Murner on 30.11.21.
//

import SpriteKit

class TARoundetImageNode: TANode {
    // MARK: - Nodes

    private var backgroundNode: SKShapeNode!
    private var imageNode: SKSpriteNode?

    override var position: CGPoint {
        didSet {
            super.position = position
        }
    }

    // MARK: - Parameters

    let subscriberId = UUID()
    var radius: CGFloat
    var size: CGSize
    var imageScaling: CGFloat
    var fillColor: GameColor
    var imageTextureName: String

    // MARK: - Initializer

    init(imageName: String, radius: CGFloat, fillColor: GameColor, imageScaling: CGFloat = 0.7) {
        self.radius = radius
        self.imageScaling = imageScaling
        self.imageTextureName = imageName
        self.fillColor = fillColor
        self.size = CGSize(width: radius * 2, height: radius * 2)
        super.init()
        setupBackgroundNode()
        setImage()
        subscribeEvents()
    }

    deinit {
        unsubscribeEvents()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Methods

    private func subscribeEvents() {
        EventBus.shared.subscribe(subscriberId: subscriberId, event: .UIInterfaceStyleUpdate) { [weak self] _ in
            self?.setImage()
            self?.setColor()
        }
    }

    private func unsubscribeEvents() {
        EventBus.shared.unsubscribeAll(subscriberId: subscriberId)
    }

    private func setupBackgroundNode() {
        backgroundNode = SKShapeNode(circleOfRadius: radius)
        addChild(backgroundNode)
        setColor()
    }

    private func setColor() {
        backgroundNode.fillColor = fillColor.getColor()
        backgroundNode.strokeColor = .clear
    }

    private func setImage() {
        let textureAppearanceString = UITraitCollection.current.userInterfaceStyle == .dark ? "Dark" : "Light"
        let imageLoadPathString = imageTextureName + "-" + textureAppearanceString
        let texture = SKTexture(imageNamed: imageLoadPathString)
        imageNode?.removeFromParent()

        let newSize = size.applying(CGAffineTransform(scaleX: imageScaling,
                                                      y: imageScaling))

        imageNode = SKSpriteNode(texture: texture,
                                 size: HelperFunctions.textureRatioResize(textureSize: texture.size(),
                                                                          newSize: newSize))

        guard let imageNode = imageNode else {
            return
        }

        backgroundNode.addChild(imageNode)
    }
}
