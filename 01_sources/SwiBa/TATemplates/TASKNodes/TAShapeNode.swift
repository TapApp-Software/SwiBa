//
//  TAShapeNode.swift
//  SwiBa
//
//  Created by Franz Murner on 05.01.22.
//

import Foundation
import SpriteKit

class TAShapeNode: TANode {
    // MARK: - Nodes

    let node: SKShapeNode

    // MARK: - Parameters

    var fillColor: GameColor = .clear {
        didSet {
            setColor()
        }
    }

    var strokeColor: GameColor = .clear {
        didSet {
            setColor()
        }
    }

    var lineWidth: CGFloat = 0 {
        didSet {
            node.lineWidth = lineWidth
        }
    }

    // MARK: - Initializer

    init(circleOfRadius: CGFloat) {
        node = SKShapeNode(circleOfRadius: circleOfRadius)
        super.init()
        addChild(node)
        subscribeEvents()
    }

    init(size: CGSize, cornerRadius: CGFloat) {
        let origin = CGPoint(x: -size.width / 2, y: -size.height / 2)
        node = SKShapeNode(rect: CGRect(origin: origin, size: size), cornerRadius: cornerRadius)
        super.init()
        addChild(node)
        subscribeEvents()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    deinit {
        unsubscribeEvents()
    }

    // MARK: - Methods

    private func subscribeEvents() {
        EventBus.shared.subscribe(subscriberId: subscriberID, event: .UIInterfaceStyleUpdate) { [weak self] _ in
            self?.setColor()
        }
    }

    private func unsubscribeEvents() {
        EventBus.shared.unsubscribeAll(subscriberId: subscriberID)
    }

    func setColor() {
        node.fillColor = fillColor.getColor()
        node.strokeColor = strokeColor.getColor()
    }

    override func colorOverride(fillColor: GameColor, overrideAction _: TANode.ColorOverrideAction) {
        self.fillColor = fillColor
    }
}
