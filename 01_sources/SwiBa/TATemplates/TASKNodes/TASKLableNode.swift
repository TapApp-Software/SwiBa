//
//  TASKLableNode.swift
//  SwiBa
//
//  Created by Franz Murner on 07.01.22.
//

import Foundation
import SpriteKit

class TASKLabelNode: TANode {
    // MARK: - Nodes

    var labelNode = SKLabelNode()

    // MARK: - Parameters

    var labelType: LabelTypes

    var text: String = "N/A" {
        didSet {
            setLabel()
        }
    }

    var customStringFormatationAttributes: [NSAttributedString.Key: NSObject?]? {
        didSet {
            setLabel()
        }
    }

    var font: UIFont = .systemFont(ofSize: 12, weight: .regular) {
        didSet {
            setLabel()
        }
    }

    var alingnment: NSTextAlignment = .natural {
        didSet {
            setLabel()
        }
    }

    var fontColor: GameColor = .mainLabel {
        didSet {
            setLabel()
        }
    }

    var numberOfLines = 0 {
        didSet {
            setLabel()
        }
    }

    var lableWidth: CGFloat {
        didSet {
            labelNode.preferredMaxLayoutWidth = lableWidth
        }
    }

    // MARK: - Initializer

    init(lableWidth: CGFloat, labelType: LabelTypes = .none) {
        self.lableWidth = lableWidth
        self.labelType = labelType
        super.init()

        createLabel()
        setLabel()
        subscribeEvents()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    deinit {
        unsubscribeEvents()
    }

    // MARK: - Methods

    private func setLabel() {
        if let customStringFormatationAttributes = customStringFormatationAttributes {
            let attributes = customStringFormatationAttributes as [NSAttributedString.Key: Any]
            labelNode.attributedText = NSAttributedString(string: text,
                                                          attributes: attributes)
        } else {
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = alingnment

            let attributes = [NSAttributedString.Key.foregroundColor: fontColor.getColor(),
                              NSAttributedString.Key.font: font,
                              NSAttributedString.Key.paragraphStyle: paragraphStyle]
            labelNode.attributedText = NSAttributedString(string: text,
                                                          attributes: attributes)
        }
    }

    private func createLabel() {
        labelNode.horizontalAlignmentMode = .center
        labelNode.verticalAlignmentMode = .center
        labelNode.lineBreakMode = .byWordWrapping
        labelNode.preferredMaxLayoutWidth = lableWidth
        labelNode.numberOfLines = 0

        self.addChild(labelNode)
    }

    private func subscribeEvents() {
        EventBus.shared.subscribe(subscriberId: subscriberID, event: .UIInterfaceStyleUpdate) { [weak self] _ in
            self?.setLabel()
        }

        EventBus.shared.subscribe(subscriberId: subscriberID, event: .autolabelChangeEvent) { [weak self] event in
            guard let event = event as? AutolabelChangeEvent,
                  self?.labelType == event.labelType
            else { return }
            self?.text = event.newLabelText
        }
    }

    private func unsubscribeEvents() {
        EventBus.shared.unsubscribeAll(subscriberId: subscriberID)
    }
}
