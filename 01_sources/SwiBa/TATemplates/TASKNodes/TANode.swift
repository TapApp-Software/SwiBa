//
//  TANode.swift
//  SwiBa
//
//  Created by Franz Murner on 22.11.21.
//

import SpriteKit

public class TANode: SKNode {
    var borderSize: CGSize?

    enum ColorOverrideAction {
        case onDisable
        case onEnable
        case onTouch
        case onSet
    }

    var subscriberID = UUID()

    var taConstraints: [TAConstraint]? {
        didSet {
            guard let taConstraints = taConstraints else { return }
            TAConstraint.processingConstraintConnection(constraints: taConstraints, target: self)
        }
    }

    func getSize() -> CGSize {
        guard let borderSize = borderSize else {
            return self.calculateAccumulatedFrame().size
        }

        return borderSize
    }

    func colorOverride(fillColor _: GameColor, overrideAction _: ColorOverrideAction) {}
}
