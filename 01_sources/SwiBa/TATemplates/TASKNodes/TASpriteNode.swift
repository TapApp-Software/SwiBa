//
//  TASpriteNode.swift
//  SwiBa
//
//  Created by Franz Murner on 06.01.22.
//

import SpriteKit

public class TASpriteNode: TANode {
    // MARK: - Nodes

    var spriteNode: SKSpriteNode
    var size: CGSize

    var color: GameColor {
        didSet {
            setColor()
        }
    }

    var colorAlpha: CGFloat {
        didSet {
            setColor()
        }
    }

    // MARK: - Parameters

    // MARK: - Initializer

    init(color: GameColor? = nil, size: CGSize) {
        self.spriteNode = SKSpriteNode()
        self.color = .black
        self.size = size

        if let color = color {
            self.color = color
            self.colorAlpha = 1
        } else {
            self.color = .black
            self.colorAlpha = 0
        }

        super.init()
        spriteNode.size = size
        addChild(spriteNode)
        setColor()
        subscribeEvents()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    deinit {
        unsubscribeEvents()
    }

    // MARK: - Methods

    private func setColor() {
        spriteNode.color = color.getColor().withAlphaComponent(colorAlpha)
    }

    private func subscribeEvents() {
        EventBus.shared.subscribe(subscriberId: subscriberID, event: .UIInterfaceStyleUpdate) { [weak self] _ in
            self?.setColor()
        }
    }

    private func unsubscribeEvents() {
        EventBus.shared.unsubscribeAll(subscriberId: subscriberID)
    }
}
