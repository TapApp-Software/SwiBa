//
//  TATimer.swift
//  SwiBa
//
//  Created by Franz Murner on 31.12.21.
//

import Foundation

class TATimer {
    // MARK: - Properties

    private var subscriberID = UUID()
    private var timerType: TimerType
    private var currentTimer: Timer?
    private var repeatBlock: Bool
    private var block: (TATimer) -> Void
    public var timeInterval: TimeInterval {
        didSet {
            guard !isTimerPaused else { return }
            stop()
            start()
        }
    }

    private(set) var isTimerPaused = true

    // MARK: - Initializer

    public init(timeInterval: TimeInterval, timerType: TimerType, repeatBlock: Bool,
                block: @escaping (TATimer) -> Void) {
        self.block = block
        self.timerType = timerType
        self.repeatBlock = repeatBlock
        self.timeInterval = timeInterval

        subscribeEvents()
    }

    deinit {
        unsubscribeEvents()
    }

    // MARK: - EventBus Subscriptions

    public func subscribeEvents() {
        EventBus.shared.subscribe(subscriberId: subscriberID, event: .taTimerStartEvent) { [weak self] event in
            guard let event = event as? TATimerStartEvent,
                  event.timerType == self?.timerType
            else { return }
            self?.start()
        }

        EventBus.shared.subscribe(subscriberId: subscriberID, event: .taTimerStopEvent) { [weak self] event in
            guard let event = event as? TATimerStopEvent,
                  event.timerType == self?.timerType
            else { return }
            self?.stop()
        }
    }

    public func unsubscribeEvents() {
        EventBus.shared.unsubscribeAll(subscriberId: subscriberID)
    }

    // MARK: - Methodes

    public func start() {
        isTimerPaused = false
        currentTimer = Timer.scheduledTimer(withTimeInterval: self.timeInterval, repeats: repeatBlock) { [self] _ in
            block(self)
        }
        currentTimer?.fireDate = Date.now.addingTimeInterval(self.timeInterval)
    }

    public func stop() {
        isTimerPaused = true
        currentTimer?.invalidate()
    }
}
