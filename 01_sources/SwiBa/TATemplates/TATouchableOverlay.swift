//
//  TouchableOverlay.swift
//  SwiBa
//
//  Created by Franz Murner on 21.11.21.
//

import SpriteKit

class TATouchableOverlay: SKNode {
    // MARK: - Nodes

    var overlaySprite: SKSpriteNode

    // MARK: - Parameters

    let subscriberID = UUID()
    var connectedEvent: IEvent

    // MARK: - Initializer

    init(size: CGSize, connectedEvent: IEvent) {
        overlaySprite = SKSpriteNode(color: .black, size: size)
        self.connectedEvent = connectedEvent
        super.init()
        self.alpha = 0
        self.isUserInteractionEnabled = true
        overlaySprite.zPosition = 100
        self.addChild(overlaySprite)

        self.run(SKAction.fadeAlpha(to: 0.4, duration: 0.2))

        subscribeEvents()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    deinit {
        unsubscribeEvents()
    }

    // MARK: - Methods

    private func subscribeEvents() {
        EventBus.shared.subscribe(subscriberId: subscriberID, event: connectedEvent.getEventType()) { [weak self] _ in
            self?.removeOverlay()
        }
    }

    private func unsubscribeEvents() {
        EventBus.shared.unsubscribeAll(subscriberId: subscriberID)
    }

    private func removeOverlay() {
        self.run(SKAction.fadeAlpha(to: 0, duration: 0.2)) {
            self.removeFromParent()
        }
    }
}

extension TATouchableOverlay {
    override func touchesEnded(_: Set<UITouch>, with _: UIEvent?) {
        EventBus.shared.publish(event: connectedEvent)
    }
}
