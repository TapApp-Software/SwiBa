//
//  TAIndicator.swift
//  SwiBa
//
//  Created by Franz Murner on 04.01.22.
//

import SpriteKit

class TAIndicator: TANode {
    // MARK: - Nodes

    private let dotParent = SKNode()
    private var dots: [SKShapeNode] = []

    // MARK: - Parameters

    private let radius: CGFloat = 10
    public var count: Int = 0 {
        didSet {
            let difference = count - oldValue
            if difference < 0 {
                for _ in 0 ..< -difference {
                    removeDot()
                }
            } else if difference > 0 {
                for index in 0 ..< difference {
                    createDot(index: oldValue + index)
                }
            }

            setColor()
            setPosition()
        }
    }

    public var activeDotIndexNr = 0 {
        didSet {
            setColor()
        }
    }

    // MARK: - Initializer

    override init() {
        super.init()
        addChild(dotParent)
        subscribeEvents()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    deinit {
        unsubscribeEvents()
    }

    // MARK: - Methods

    private func createDot(index: Int) {
        let dot = SKShapeNode(circleOfRadius: radius)
        dot.position.x = radius * 5 * CGFloat(index)
        dotParent.addChild(dot)
        dots.append(dot)
    }

    private func removeDot() {
        guard !dots.isEmpty else { return }
        dots.removeLast().removeFromParent()
    }

    private func setPosition() {
        dotParent.position.x = -(radius * 5 * CGFloat(count - 1) / 2)
    }

    private func setColor() {
        for index in 0 ..< count {
            let dot = dots[index]

            dot.strokeColor = .clear

            if index == activeDotIndexNr {
                dot.fillColor = GameColor.activeIndicatorColor.getColor()
            } else {
                dot.fillColor = GameColor.inactiveIndicatorColor.getColor()
            }
        }
    }

    private func subscribeEvents() {
        EventBus.shared.subscribe(subscriberId: subscriberID, event: .UIInterfaceStyleUpdate) { [self] _ in
            setColor()
        }
    }

    private func unsubscribeEvents() {
        EventBus.shared.unsubscribeAll(subscriberId: subscriberID)
    }
}
