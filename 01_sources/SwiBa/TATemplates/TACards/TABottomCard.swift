//
//  BottomCard.swift
//  SwiBa
//
//  Created by Franz Murner on 21.11.21.
//

import SpriteKit

class TABottomCard: TANode {
    // MARK: - Nodes

    private var objectID = UUID()
    private(set) var card: TACard!
    private var closeButton: TAButton!
    public var titleLableNode: TASKLabelNode!

    // MARK: - Properties

    private var sceneSize: CGSize
    private var openPosY: CGFloat = 0
    private var closedPosY: CGFloat = 0
    private var height: CGFloat
    private var title: String

    // MARK: - Initializer

    init(title: String, sceneSize: CGSize, height: CGFloat) {
        self.sceneSize = sceneSize
        self.height = height
        self.title = title
        super.init()
        openPosY = (-sceneSize.height / 2) + (height / 2) - 30
        closedPosY = (-self.sceneSize.height / 2) - (self.height)
        setupCard()
        setupCloseButton()
        setUpTitle()
        setupConstraints()
        subscribeEvents()
        showCard()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    deinit {
        unsubscribeEvents()
    }

    // MARK: - Methods

    private func setupCard() {
        let cardSize = CGSize(width: sceneSize.width, height: height)
        card = TACard(backgroundColor: .secondaryBackgound, size: cardSize,
                      shadowColor: .cardNodeShadowColor)
        card.zPosition = 1
        card.borderSize = cardSize.applyBoth(-70)
        position.y = closedPosY
        self.addChild(card)
    }

    private func showCard() {
        let action = SKAction.moveTo(y: self.openPosY, duration: 0.6)
        action.timingFunction = TATimingFunction.exponentialEaseOut(_:)
        self.run(action)
    }

    private func setUpTitle() {
        titleLableNode = TASKLabelNode(lableWidth: borderSize?.width ?? 1000)
        titleLableNode.text = title
        titleLableNode.font = UIFont(name: GameConstants.font, size: 120) ?? UIFont
            .systemFont(ofSize: 120, weight: .semibold)
        titleLableNode.zPosition = 3
        card.addChild(titleLableNode)
    }

    private func setupCloseButton() {
        let closeNode = TASFSymboleNode(symboleName: "xmark.circle.fill", color: GameColor.clear,
                                        size: 90)
        closeButton = TAButton(backgroundNode: closeNode, mainColor: .grey, onTouchColor: .greyPressed,
                               onDisabledColor: .greyDisabled)
        closeButton.zPosition = 3
        closeButton.touchEndetHandler = TABottomCardCloseEvent()
        self.addChild(closeButton)
    }

    private func setupConstraints() {
        titleLableNode.taConstraints = [.setWhenPartnerIsParent(sourceNode: card, direction: .top),
                                        .setWhenPartnerIsParent(sourceNode: card, direction: .left)]

        closeButton.taConstraints = [.setWhenPartnerIsParent(sourceNode: card, direction: .top, withInset: 10),
                                     .setWhenPartnerIsParent(sourceNode: card, direction: .right)]
    }

    private func subscribeEvents() {
        EventBus.shared.subscribe(subscriberId: objectID, event: .taBottomCardCloseEvent) { [weak self] _ in
            let action = SKAction.moveTo(y: self!.closedPosY, duration: 0.6)
            action.timingFunction = TATimingFunction.quarticEaseOut(_:)

            self?.run(action) {
                self?.removeFromParent()
            }
        }
    }

    private func unsubscribeEvents() {
        EventBus.shared.unsubscribeAll(subscriberId: objectID)
    }
}
