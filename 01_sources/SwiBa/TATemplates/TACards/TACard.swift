//
//  TACard.swift
//  SwiBa
//
//  Created by Franz Murner on 21.11.21.
//

import SpriteKit

class TACard: TANode {
    // MARK: - Properties

    private let id = UUID()
    var color: GameColor
    var size: CGSize
    var path: CGPath?
    var shadowColor: GameColor?

    // MARK: Nodes

    private var shadowNode = SKShapeNode()
    private var shadowNode2 = SKShapeNode()
    private var cardNode: SKShapeNode!

    // MARK: Initializer

    init(backgroundColor: GameColor, size: CGSize, shadowColor: GameColor? = nil) {
        self.shadowColor = shadowColor
        self.color = backgroundColor
        self.size = size
        super.init()
        self.borderSize = size
        createPath()
        draw()
        drawShadow()
        setColor()
        subscribeEvents()
    }

    deinit {
        unsubscribeEvents()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Methods

    // MARK: Draw

    private func draw() {
        guard let path = path else {
            print("TACard: Error while draw the Shape -> No Path!")
            return
        }

        cardNode = SKShapeNode()
        cardNode.path = path

        shadowNode2.addChild(cardNode)
    }

    private func drawShadow() {
        guard let path = path else {
            print("TACard: Error while draw the Shape -> No Path!")
            return
        }

        shadowNode.path = path
        shadowNode.lineWidth = 1
        shadowNode2.path = path
        shadowNode2.lineWidth = 1
        self.addChild(shadowNode)
        shadowNode.addChild(shadowNode2)
    }

    private func createPath() {
        path = UIBezierPath(roundedRect: CGRect(origin: CGPoint(x: -size.width / 2, y: -size.height / 2), size: size),
                            byRoundingCorners: .allCorners,
                            cornerRadii: CGSize(width: 20, height: 20)).cgPath
    }

    private func setColor() {
        cardNode.fillColor = color.getColor()
        cardNode.strokeColor = .clear
        shadowNode.fillColor = .clear
        shadowNode2.fillColor = .clear
        shadowNode.strokeColor = .clear
        shadowNode2.strokeColor = .clear

        guard let shadowColor = shadowColor else {
            return
        }

        shadowNode.strokeColor = shadowColor.getColor()
        shadowNode2.strokeColor = shadowColor.getColor()
        shadowNode.glowWidth = 8
        shadowNode2.glowWidth = 12
    }

    // MARK: Events

    private func subscribeEvents() {
        EventBus.shared.subscribe(subscriberId: id, event: .UIInterfaceStyleUpdate) { [weak self] _ in
            self?.setColor()
        }
    }

    private func unsubscribeEvents() {
        EventBus.shared.unsubscribeAll(subscriberId: id)
    }
}
