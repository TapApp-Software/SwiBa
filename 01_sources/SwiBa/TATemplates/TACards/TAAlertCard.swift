//
//  TAAlertLabel.swift
//  SwiBa
//
//  Created by Franz Murner on 04.12.21.
//

import SpriteKit

class TAAlertCard: TANode {
    // MARK: - Nodes

    var card: TACard!
    var cancelButton: TAButton!
    var confirmButton: TAButton!
    var textLabel: TASKLabelNode!

    // MARK: - Parameters

    var confirmationEvent: IEvent
    var contentSize: CGSize
    var padding: CGFloat = 50

    // MARK: - Initializer

    init(color _: GameColor, sceneSize: CGSize, text: String, confirmationEvent: IEvent) {
        self.confirmationEvent = confirmationEvent
        let cardSize = CGSize(width: sceneSize.width - 50, height: 600)
        card = TACard(backgroundColor: GameColor.secondaryBackgound, size: cardSize,
                      shadowColor: GameColor.cardNodeShadowColor)
        contentSize = cardSize.applyBoth(-padding * 2)
        super.init()
        self.addChild(card)
        setupCancelButton()
        setupConfirmButton()
        setupTextLabel(text: text)

        openAlertCardAnimation()
        setPositionOfObjects()
        subscribeEvents()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    deinit {
        unsubscribeEvents()
    }

    // MARK: - Methods

    private func setupCancelButton() {
        let cancelLabel =
            TAStaticPilledLabel(size: CGSize(width: contentSize.width, height: 100),
                                text: "Cancel".localized(),
                                fillColor: GameColor.clear)

        cancelLabel.updateConstraint()
        cancelButton = TAButton(backgroundNode: cancelLabel, mainColor: .grey, onTouchColor: .greyPressed,
                                onDisabledColor: .greyDisabled)
        cancelButton.zPosition = 3
        cancelButton.constraint.margin.bottom = 30
        cancelButton.touchEndetHandler = TAAlertCardCloseEvent()

        addChild(cancelButton)
    }

    private func setupConfirmButton() {
        let confirmLabel =
            TAStaticPilledLabel(size: CGSize(width: contentSize.width, height: 100),
                                text: "Delete".localized(),
                                fillColor: GameColor.clear)

        confirmLabel.updateConstraint()

        confirmButton = TAButton(backgroundNode: confirmLabel, mainColor: .confirmRed, onTouchColor: .confirmRedPressed,
                                 onDisabledColor: .confirmRedDisabled)
        confirmButton.zPosition = 3
        confirmButton.constraint.margin.bottom = 30
        confirmButton.touchEndetHandler = confirmationEvent

        addChild(confirmButton)
    }

    private func setupTextLabel(text: String) {
        textLabel = TASKLabelNode(lableWidth: contentSize.width)
        textLabel.text = text
        textLabel.font = UIFont(name: GameConstants.font, size: 100) ?? UIFont.systemFont(ofSize: 100)
        textLabel.alingnment = .center
        addChild(textLabel)
    }

    private func setPositionOfObjects() {
        self.borderSize = contentSize
        textLabel.taConstraints = [.setWhenPartnerIsParent(sourceNode: self, direction: .top)]
        cancelButton.taConstraints = [.setWhenPartnerIsParent(sourceNode: self, direction: .bottom)]
        confirmButton.taConstraints = [.setPartner(sourceNode: cancelButton, direction: .top, withOffset: padding / 2)]
    }

    private func openAlertCardAnimation() {
        self.yScale = 0
        self.run(SKAction.scaleY(to: 1, duration: 0.1))
    }

    private func closeAlertCardAnimation() {
        self.yScale = 1
        self.run(SKAction.scaleY(to: 0, duration: 0.1)) {
            self.removeFromParent()
        }
    }

    private func subscribeEvents() {
        EventBus.shared.subscribe(subscriberId: subscriberID, event: .taAlertCardCloseEvent) { [weak self] _ in
            self?.closeAlertCardAnimation()
        }
    }

    private func unsubscribeEvents() {
        EventBus.shared.unsubscribeAll(subscriberId: subscriberID)
    }
}
