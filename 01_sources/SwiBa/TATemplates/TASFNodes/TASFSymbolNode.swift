//
//  TASymbolNode.swift
//  SwiBa
//
//  Created by Franz Murner on 21.11.21.
//

import SpriteKit

class TASFSymboleNode: TANode {
    // MARK: - Parameters

    private var symboleName: String {
        didSet {
            loadSymbole()
        }
    }

    private var size: CGFloat
    private var fontSize: CGFloat = 300

    private var font: UIFont {
        return UIFont.systemFont(ofSize: fontSize, weight: weight)
    }

    var gameColor: GameColor? {
        didSet {
            loadSymbole()
        }
    }

    var weight: UIFont.Weight {
        didSet {
            loadSymbole()
        }
    }

    var hasDarkApperance: Bool = false

    private var isCustomSymbole: Bool
    private var isApperanceInFileName: Bool

    // MARK: - Nodes

    private var symboleNode: SKSpriteNode?

    // MARK: - Initializer

    init(symboleName: String, color: GameColor? = nil, size: CGFloat, weight: UIFont.Weight = .regular) {
        self.symboleName = symboleName
        isCustomSymbole = false
        self.size = size
        self.gameColor = color
        self.weight = weight
        isApperanceInFileName = false
        super.init()
        loadSymbole()
        subscribeEvents()
    }

    init(customSymboleName: String, color: GameColor? = nil, size: CGFloat, weight: UIFont.Weight = .regular) {
        self.symboleName = customSymboleName
        isCustomSymbole = true
        self.size = size
        self.gameColor = color
        self.weight = weight
        isApperanceInFileName = false
        super.init()
        loadSymbole()
        subscribeEvents()
    }

    init(customSymboleName: String, fileNameIncludeAppearnce: Bool, size: CGFloat, weight: UIFont.Weight = .regular) {
        self.symboleName = customSymboleName
        isCustomSymbole = true
        self.size = size
        self.weight = weight
        isApperanceInFileName = fileNameIncludeAppearnce
        super.init()
        loadSymbole()
        subscribeEvents()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    deinit {
        unsubscribeEvents()
    }

    // MARK: - Methods

    // MARK: Setup

    private func loadSymbole() {
        if isCustomSymbole {
            getCustomSFSymbole()
        } else {
            getStandartSFSymbole()
        }
    }

    private func getCustomSFSymbole() {
        if isApperanceInFileName {
            let appearanceString = UITraitCollection.current.userInterfaceStyle == .dark ? "Dark" : "Light"
            createSymbole(withTexture: .getCustomTextureFromSymbole(named: "\(symboleName)-\(appearanceString)",
                                                                    color: nil, font: font))
        } else {
            createSymbole(withTexture: .getCustomTextureFromSymbole(named: symboleName, color: gameColor, font: font))
        }
    }

    private func getStandartSFSymbole() {
        createSymbole(withTexture: .getTextureFromSymbole(symboleName: symboleName, color: gameColor, font: font))
    }

    private func createSymbole(withTexture texture: SKTexture?) {
        symboleNode?.removeFromParent()

        guard let texture = texture
        else { return }

        let textureRatio = texture.size().width / texture.size().height

        let realSize = CGSize(width: size * textureRatio, height: size)
        symboleNode = SKSpriteNode(texture: texture, size: realSize)

        guard let symboleNode = symboleNode else {
            return
        }

        self.addChild(symboleNode)
    }

    // MARK: Events

    private func subscribeEvents() {
        EventBus.shared.subscribe(subscriberId: subscriberID, event: .UIInterfaceStyleUpdate) { [weak self] _ in
            self?.loadSymbole()
        }
    }

    private func unsubscribeEvents() {
        EventBus.shared.unsubscribeAll(subscriberId: subscriberID)
    }

    override func colorOverride(fillColor: GameColor, overrideAction _: TANode.ColorOverrideAction) {
        self.gameColor = fillColor
        self.loadSymbole()
    }
}
