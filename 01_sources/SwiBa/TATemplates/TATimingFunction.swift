/*
 * Timing functions for SKTEffects. Based on Robert Penner's easing equations
 * http://robertpenner.com/easing/ and https://github.com/warrenm/AHEasing
 * Animation https://easings.net
 *
 * Copyright (c) 2013-2014 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

// swiftlint:disable all

import CoreGraphics
import Foundation

public class TATimingFunction {
    public static func linear(_ t: Float) -> Float {
        return t
    }

    public static func quadraticEaseIn(_ t: Float) -> Float {
        return t * t
    }

    public static func quadraticEaseOut(_ t: Float) -> Float {
        return t * (2.0 - t)
    }

    public static func quadraticEaseInOut(_ t: Float) -> Float {
        if t < 0.5 {
            return 2.0 * t * t
        } else {
            let f = t - 1.0
            return 1.0 - 2.0 * f * f
        }
    }

    func cubicEaseIn(_ t: Float) -> Float {
        return t * t * t
    }

    func cubicEaseOut(_ t: Float) -> Float {
        let f = t - 1.0
        return 1.0 + f * f * f
    }

    public static func cubicEaseInOut(_ t: Float) -> Float {
        if t < 0.5 {
            return 4.0 * t * t * t
        } else {
            let f = t - 1.0
            return 1.0 + 4.0 * f * f * f
        }
    }

    public static func quarticEaseIn(_ t: Float) -> Float {
        return t * t * t * t
    }

    public static func quarticEaseOut(_ t: Float) -> Float {
        let f = t - 1.0
        return 1.0 - f * f * f * f
    }

    public static func quarticEaseInOut(_ t: Float) -> Float {
        if t < 0.5 {
            return 8.0 * t * t * t * t
        } else {
            let f = t - 1.0
            return 1.0 - 8.0 * f * f * f * f
        }
    }

    public static func quinticEaseIn(_ t: Float) -> Float {
        return t * t * t * t * t
    }

    public static func quinticEaseOut(_ t: Float) -> Float {
        let f = t - 1.0
        return 1.0 + f * f * f * f * f
    }

    func quinticEaseInOut(_ t: Float) -> Float {
        if t < 0.5 {
            return 16.0 * t * t * t * t * t
        } else {
            let f = t - 1.0
            return 1.0 + 16.0 * f * f * f * f * f
        }
    }

    public static func sineEaseIn(_ t: Float) -> Float {
        return sin((t - 1.0) * .pi / 2) + 1.0
    }

    public static func sineEaseOut(_ t: Float) -> Float {
        return sin(t * .pi / 2)
    }

    public static func sineEaseInOut(_ t: Float) -> Float {
        return 0.5 * (1.0 - cos(t * .pi))
    }

    public static func circularEaseIn(_ t: Float) -> Float {
        return 1.0 - sqrt(1.0 - t * t)
    }

    public static func circularEaseOut(_ t: Float) -> Float {
        return sqrt((2.0 - t) * t)
    }

    public static func circularEaseInOut(_ t: Float) -> Float {
        if t < 0.5 {
            return 0.5 * (1.0 - sqrt(1.0 - 4.0 * t * t))
        } else {
            return 0.5 * sqrt(-4.0 * t * t + 8.0 * t - 3.0) + 0.5
        }
    }

    public static func exponentialEaseIn(_ t: Float) -> Float {
        return (t == 0.0) ? t : pow(2.0, 10.0 * (t - 1.0))
    }

    public static func exponentialEaseOut(_ t: Float) -> Float {
        return (t == 1.0) ? t : 1.0 - pow(2.0, -10.0 * t)
    }

    public static func exponentialEaseInOut(_ t: Float) -> Float {
        if t == 0.0 || t == 1.0 {
            return t
        } else if t < 0.5 {
            return 0.5 * pow(2.0, 20.0 * t - 10.0)
        } else {
            return 1.0 - 0.5 * pow(2.0, -20.0 * t + 10.0)
        }
    }

    public static func elasticEaseIn(_ t: Float) -> Float {
        return sin(13.0 * .pi / 2 * t) * pow(2.0, 10.0 * (t - 1.0))
    }

    public static func elasticEaseOut(_ t: Float) -> Float {
        return sin(-13.0 * .pi / 2 * (t + 1.0)) * pow(2.0, -10.0 * t) + 1.0
    }

    public static func elasticEaseInOut(_ t: Float) -> Float {
        if t < 0.5 {
            return 0.5 * sin(13.0 * .pi * t) * pow(2.0, 20.0 * t - 10.0)
        } else {
            return 0.5 * sin(-13.0 * .pi * t) * pow(2.0, -20.0 * t + 10.0) + 1.0
        }
    }

    public static func backEaseIn(_ t: Float) -> Float {
        let s: Float = 1.70158
        return ((s + 1.0) * t - s) * t * t
    }

    public static func backEaseOut(_ t: Float) -> Float {
        let s: Float = 1.70158
        let f = 1.0 - t
        return 1.0 - ((s + 1.0) * f - s) * f * f
    }

    public static func backEaseInOut(_ t: Float) -> Float {
        let s: Float = 1.70158
        if t < 0.5 {
            let f = 2.0 * t
            return 0.5 * ((s + 1.0) * f - s) * f * f
        } else {
            let f = 2.0 * (1.0 - t)
            return 1.0 - 0.5 * ((s + 1.0) * f - s) * f * f
        }
    }

    public static func extremeBackEaseIn(_ t: Float) -> Float {
        return (t * t - sin(t * .pi)) * t
    }

    public static func extremeBackEaseOut(_ t: Float) -> Float {
        let f = 1.0 - t
        return 1.0 - (f * f - sin(f * .pi)) * f
    }

    public static func extremeBackEaseInOut(_ t: Float) -> Float {
        if t < 0.5 {
            let f = 2.0 * t
            return 0.5 * (f * f - sin(f * .pi)) * f
        } else {
            let f = 2.0 * (1.0 - t)
            return 1.0 - 0.5 * (f * f - sin(f * .pi)) * f
        }
    }

    public static func bounceEaseIn(_ t: Float) -> Float {
        return 1.0 - bounceEaseOut(1.0 - t)
    }

    public static func bounceEaseOut(_ t: Float) -> Float {
        if t < 1.0 / 2.75 {
            return 7.5625 * t * t
        } else if t < 2.0 / 2.75 {
            let f = t - 1.5 / 2.75
            return 7.5625 * f * f + 0.75
        } else if t < 2.5 / 2.75 {
            let f = t - 2.25 / 2.75
            return 7.5625 * f * f + 0.9375
        } else {
            let f = t - 2.625 / 2.75
            return 7.5625 * f * f + 0.984375
        }
    }

    public static func bounceEaseInOut(_ t: Float) -> Float {
        if t < 0.5 {
            return 0.5 * bounceEaseIn(t * 2.0)
        } else {
            return 0.5 * bounceEaseOut(t * 2.0 - 1.0) + 0.5
        }
    }

    public static func smoothstep(_ t: Float) -> Float {
        return t * t * (3 - 2 * t)
    }

    public static func createShakeFunction(_ oscillations: Int) -> (Float) -> Float {
        return { t in -pow(2.0, -10.0 * t) * sin(t * .pi * Float(oscillations) * 2.0) + 1.0 }
    }

    public static func linear(_ t: CGFloat) -> CGFloat {
        return t
    }

    public static func quadraticEaseIn(_ t: CGFloat) -> CGFloat {
        return t * t
    }

    public static func quadraticEaseOut(_ t: CGFloat) -> CGFloat {
        return t * (2.0 - t)
    }

    public static func quadraticEaseInOut(_ t: CGFloat) -> CGFloat {
        if t < 0.5 {
            return 2.0 * t * t
        } else {
            let f = t - 1.0
            return 1.0 - 2.0 * f * f
        }
    }

    func cubicEaseIn(_ t: CGFloat) -> CGFloat {
        return t * t * t
    }

    func cubicEaseOut(_ t: CGFloat) -> CGFloat {
        let f = t - 1.0
        return 1.0 + f * f * f
    }

    public static func cubicEaseInOut(_ t: CGFloat) -> CGFloat {
        if t < 0.5 {
            return 4.0 * t * t * t
        } else {
            let f = t - 1.0
            return 1.0 + 4.0 * f * f * f
        }
    }

    public static func quarticEaseIn(_ t: CGFloat) -> CGFloat {
        return t * t * t * t
    }

    public static func quarticEaseOut(_ t: CGFloat) -> CGFloat {
        let f = t - 1.0
        return 1.0 - f * f * f * f
    }

    public static func quarticEaseInOut(_ t: CGFloat) -> CGFloat {
        if t < 0.5 {
            return 8.0 * t * t * t * t
        } else {
            let f = t - 1.0
            return 1.0 - 8.0 * f * f * f * f
        }
    }

    public static func quinticEaseIn(_ t: CGFloat) -> CGFloat {
        return t * t * t * t * t
    }

    public static func quinticEaseOut(_ t: CGFloat) -> CGFloat {
        let f = t - 1.0
        return 1.0 + f * f * f * f * f
    }

    func quinticEaseInOut(_ t: CGFloat) -> CGFloat {
        if t < 0.5 {
            return 16.0 * t * t * t * t * t
        } else {
            let f = t - 1.0
            return 1.0 + 16.0 * f * f * f * f * f
        }
    }

    public static func sineEaseIn(_ t: CGFloat) -> CGFloat {
        return sin((t - 1.0) * .pi / 2) + 1.0
    }

    public static func sineEaseOut(_ t: CGFloat) -> CGFloat {
        return sin(t * .pi / 2)
    }

    public static func sineEaseInOut(_ t: CGFloat) -> CGFloat {
        return 0.5 * (1.0 - cos(t * .pi))
    }

    public static func circularEaseIn(_ t: CGFloat) -> CGFloat {
        return 1.0 - sqrt(1.0 - t * t)
    }

    public static func circularEaseOut(_ t: CGFloat) -> CGFloat {
        return sqrt((2.0 - t) * t)
    }

    public static func circularEaseInOut(_ t: CGFloat) -> CGFloat {
        if t < 0.5 {
            return 0.5 * (1.0 - sqrt(1.0 - 4.0 * t * t))
        } else {
            return 0.5 * sqrt(-4.0 * t * t + 8.0 * t - 3.0) + 0.5
        }
    }

    public static func exponentialEaseIn(_ t: CGFloat) -> CGFloat {
        return (t == 0.0) ? t : pow(2.0, 10.0 * (t - 1.0))
    }

    public static func exponentialEaseOut(_ t: CGFloat) -> CGFloat {
        return (t == 1.0) ? t : 1.0 - pow(2.0, -10.0 * t)
    }

    public static func exponentialEaseInOut(_ t: CGFloat) -> CGFloat {
        if t == 0.0 || t == 1.0 {
            return t
        } else if t < 0.5 {
            return 0.5 * pow(2.0, 20.0 * t - 10.0)
        } else {
            return 1.0 - 0.5 * pow(2.0, -20.0 * t + 10.0)
        }
    }

    public static func elasticEaseIn(_ t: CGFloat) -> CGFloat {
        return sin(13.0 * .pi / 2 * t) * pow(2.0, 10.0 * (t - 1.0))
    }

    public static func elasticEaseOut(_ t: CGFloat) -> CGFloat {
        return sin(-13.0 * .pi / 2 * (t + 1.0)) * pow(2.0, -10.0 * t) + 1.0
    }

    public static func elasticEaseOutSoft(_ t: CGFloat) -> CGFloat {
        return sin(-9.0 * .pi / 2 * (t + 1.0)) * pow(2.0, -10.0 * t) + 1.0
    }

    public static func elasticEaseOutExtreme(_ t: CGFloat) -> CGFloat {
        return sin(-17.0 * .pi / 2 * (t + 1.0)) * pow(1.6, -8.0 * t) + 1.0
    }

    public static func elasticEaseInOut(_ t: CGFloat) -> CGFloat {
        if t < 0.5 {
            return 0.5 * sin(13.0 * .pi * t) * pow(2.0, 20.0 * t - 10.0)
        } else {
            return 0.5 * sin(-13.0 * .pi * t) * pow(2.0, -20.0 * t + 10.0) + 1.0
        }
    }

    public static func backEaseIn(_ t: CGFloat) -> CGFloat {
        let s: CGFloat = 1.70158
        return ((s + 1.0) * t - s) * t * t
    }

    public static func backEaseOut(_ t: CGFloat) -> CGFloat {
        let s: CGFloat = 1.70158
        let f = 1.0 - t
        return 1.0 - ((s + 1.0) * f - s) * f * f
    }

    public static func backEaseInOut(_ t: CGFloat) -> CGFloat {
        let s: CGFloat = 1.70158
        if t < 0.5 {
            let f = 2.0 * t
            return 0.5 * ((s + 1.0) * f - s) * f * f
        } else {
            let f = 2.0 * (1.0 - t)
            return 1.0 - 0.5 * ((s + 1.0) * f - s) * f * f
        }
    }

    public static func extremeBackEaseIn(_ t: CGFloat) -> CGFloat {
        return (t * t - sin(t * .pi)) * t
    }

    public static func extremeBackEaseOut(_ t: CGFloat) -> CGFloat {
        let f = 1.0 - t
        return 1.0 - (f * f - sin(f * .pi)) * f
    }

    public static func extremeBackEaseInOut(_ t: CGFloat) -> CGFloat {
        if t < 0.5 {
            let f = 2.0 * t
            return 0.5 * (f * f - sin(f * .pi)) * f
        } else {
            let f = 2.0 * (1.0 - t)
            return 1.0 - 0.5 * (f * f - sin(f * .pi)) * f
        }
    }

    public static func bounceEaseIn(_ t: CGFloat) -> CGFloat {
        return 1.0 - bounceEaseOut(1.0 - t)
    }

    public static func bounceEaseOut(_ t: CGFloat) -> CGFloat {
        if t < 1.0 / 2.75 {
            return 7.5625 * t * t
        } else if t < 2.0 / 2.75 {
            let f = t - 1.5 / 2.75
            return 7.5625 * f * f + 0.75
        } else if t < 2.5 / 2.75 {
            let f = t - 2.25 / 2.75
            return 7.5625 * f * f + 0.9375
        } else {
            let f = t - 2.625 / 2.75
            return 7.5625 * f * f + 0.984375
        }
    }

    public static func bounceEaseInOut(_ t: CGFloat) -> CGFloat {
        if t < 0.5 {
            return 0.5 * bounceEaseIn(t * 2.0)
        } else {
            return 0.5 * bounceEaseOut(t * 2.0 - 1.0) + 0.5
        }
    }

    public static func smoothstep(_ t: CGFloat) -> CGFloat {
        return t * t * (3 - 2 * t)
    }

    public static func createShakeFunction(_ oscillations: Int) -> (CGFloat) -> CGFloat {
        return { t in -pow(2.0, -10.0 * t) * sin(t * .pi * CGFloat(oscillations) * 2.0) + 1.0 }
    }
}

// swiftlint:enable all
