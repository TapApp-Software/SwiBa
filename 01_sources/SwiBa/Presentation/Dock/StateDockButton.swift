//
//  StateDockButton.swift
//  SwiBa
//
//  Created by Franz Murner on 25.09.21.
//

import SpriteKit

enum ButtonState {
    case on // Set this if you need one button for default
    case off
    case lockedOn
    case lockedOff
}

class StateDockButton: SimpleDockButton {
    // MARK: - Properties

    private var buttonImages: [ButtonState: String?]

    // MARK: - Initializer

    init(ellipseSize: CGSize, fillColor: GameColor, strokeColor: GameColor, buttonImages: [ButtonState: String?],
         dockButtonType: DockButtonType) {
        self.buttonImages = buttonImages
        super.init(ellipseSize: ellipseSize, fillColor: fillColor, strokeColor: strokeColor,
                   buttonImage: buttonImages[.on]!, dockButtonType: dockButtonType)
        registerForButtonUpdateEvent()
    }

    convenience init(ellipseSize: CGSize, buttonImages: [ButtonState: String?], dockButtonType: DockButtonType) {
        self.init(ellipseSize: ellipseSize, fillColor: GameColor.buttonColor, strokeColor: GameColor.separatorColor,
                  buttonImages: buttonImages, dockButtonType: dockButtonType)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Logic

    func registerForButtonUpdateEvent() {
        EventBus.shared.subscribe(event: .buttonUpdateEvent) { event in
            guard let event = event as? ButtonUpdateEvent,
                  event.buttonType == self.type,
                  let buttonImage = self.buttonImages[event.buttonState],
                  let buttonImage = buttonImage
            else { return }

            self.circledImageButton.setImage(withName: buttonImage)
        }
    }
}
