//
//  PowerUpDockButtons.swift
//  SwiBa
//
//  Created by Franz Murner on 28.09.21.
//

import SpriteKit

class PowerUpDockButtons: LabeledDockButton {
    // MARK: - Nodes

    private var lockNode: SKShapeNode
    private var circleProgressBar: CircleProgressBar

    // MARK: - Properties

    private let radius: CGFloat = GameConstants.dockButtonRadius
    private var buttonLocked = false

    // MARK: - Initialisation

    override init(ellipseSize: CGSize, fillColor: GameColor, strokeColor: GameColor, buttonImage: String?,
                  dockButtonType: DockButtonType) {
        lockNode = SKShapeNode(circleOfRadius: radius)
        lockNode.zPosition = 3
        lockNode.lineWidth = 2

        circleProgressBar = CircleProgressBar(fillColor: .progressBarColor, strokeColor: .separatorColor, lineWidth: 2,
                                              radius: radius)
        circleProgressBar.zPosition = 4

        super.init(ellipseSize: ellipseSize, fillColor: fillColor, strokeColor: strokeColor, buttonImage: buttonImage,
                   dockButtonType: dockButtonType)

        circledImageButton.addChild(lockNode)
        circledImageButton.addChild(circleProgressBar)

        setColor()
        setLockNode()
        registerButtonEvents()
    }

    convenience init(ellipseSize: CGSize, buttonImage: String?, dockButtonType: DockButtonType) {
        self.init(ellipseSize: ellipseSize, fillColor: GameColor.buttonColor, strokeColor: GameColor.separatorColor,
                  buttonImage: buttonImage, dockButtonType: dockButtonType)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Methods

    private func setColor() {
        lockNode.fillColor = GameColor.progressBarColor.getColor()
        lockNode.strokeColor = GameColor.separatorColor.getColor()
    }

    private func setLockNode() {
        lockNode.isHidden = !buttonLocked
    }

    private func registerButtonEvents() {
        EventBus.shared.subscribe(event: .UIInterfaceStyleUpdate) { _ in
            self.setColor()
        }

        EventBus.shared.subscribe(event: .powerUpEvent) { event in
            guard let event = event as? PowerUpEvent,
                  event.buttonType == self.type
            else { return }

            if event.isActive {
                self.circleProgressBar.startProgressCircle(duration: event.duration, completion: event.completion)
            } else {
                self.circleProgressBar.stopProgressCircle()
            }
        }

        EventBus.shared.subscribe(event: .buttonLockEvent) { event in
            guard let event = event as? ButtonLockEvent,
                  event.buttonType == self.type
            else { return }

            self.buttonLocked = event.buttonLocked
            self.setLockNode()
        }
    }
}
