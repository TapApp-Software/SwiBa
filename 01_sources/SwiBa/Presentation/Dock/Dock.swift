//
//  ScoreBoard.swift
//  SwiBa
//
//  Created by Franz Murner on 16.08.21.
//

import SpriteKit

class Dock: SKNode {
    // MARK: - Nodes

    private var dockButtonsMenu: [DockButton]!
    private var dockButtonsGame: [DockButton]!
    private var moneyLabel: TAPilledLabel!
    private var backgroundCircle: SKShapeNode!

    // MARK: - Parameters

    private let showPositionOfMoneyLabelY: CGFloat = 350
    private var size: CGSize
    private var backgroundEllipseSize: CGSize

    // MARK: - Initializer

    init(size: CGSize) {
        self.size = size
        self.backgroundEllipseSize = CGSize(width: size.width * 2, height: size.width)
        super.init()
        self.position.y = HelperFunctions.hasDeviceNotch() ? -1310 : -1150
        drawBackgroundCircle()
        drawPilledLabel()
        setColor()
        registerEvents()
        createMenuDockButtons()
        createPlayDockButtons()

        self.zPosition = 19
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Methodes

    // MARK: Drawing

    private func drawBackgroundCircle() {
        backgroundCircle =
            SKShapeNode(ellipseIn: CGRect(origin: CGPoint(x: -backgroundEllipseSize.width / 2,
                                                          y: -backgroundEllipseSize.height / 2),
                                          size: backgroundEllipseSize))
        backgroundCircle.glowWidth = 2
        backgroundCircle.isAntialiased = true
        self.addChild(backgroundCircle)
    }

    private func drawPilledLabel() {
        moneyLabel = TAPilledLabel(maxSize: CGSize(width: 500, height: 80), labelType: .moneyLabel,
                                   fillColor: .buttonColor)
        moneyLabel.constraint.padding.globalX = 50
        moneyLabel.constraint.padding.globalY = 10
        moneyLabel.updateConstraint()
        self.addChild(moneyLabel)
    }

    private func setColor() {
        guard backgroundCircle != nil else {
            return
        }

        backgroundCircle.fillColor = GameColor.secondaryBackgound.getColor()
        backgroundCircle.strokeColor = GameColor.separatorColor.getColor()
    }

    // MARK: EventBus

    private func registerEvents() {
        EventBus.shared.subscribe(event: .UIInterfaceStyleUpdate) { _ in
            self.setColor()
        }

        EventBus.shared.subscribe(event: .goEvent) { [self] _ in
            showGameDockHandler()
        }

        EventBus.shared.subscribe(event: .endEvent) { [self] _ in
            showMenuDockHandler()
        }

        EventBus.shared.subscribe(event: .manualDockEvents) { [self] event in
            guard let manualDockEvent = event as? ManualDockEvents else { return }
            if manualDockEvent.isMainMenuDock {
                showMenuDockHandler(withAnimation: false)
            } else {
                showGameDockHandler(withAnimation: false)
            }
        }
    }

    // MARK: EventBus Handler

    private func showMenuDockHandler(withAnimation animated: Bool = true) {
        if animated {
            let moneyLabelShowAction = SKAction.sequence([SKAction.wait(forDuration: 1.5),
                                                          SKAction.moveTo(y: showPositionOfMoneyLabelY, duration: 0.5)])
            moneyLabelShowAction.timingMode = .easeOut
            moneyLabel.run(moneyLabelShowAction)
            switchEvent(oldDock: dockButtonsGame, newDock: dockButtonsMenu)
        } else {
            moneyLabel.position.y = showPositionOfMoneyLabelY
            showDockButtonsUnanimated(dockButtons: dockButtonsMenu)
        }
    }

    private func showGameDockHandler(withAnimation animated: Bool = true) {
        if animated {
            let moneyLabelHideAction = SKAction.moveTo(y: 0, duration: 0.5)
            moneyLabelHideAction.timingMode = .easeIn
            moneyLabel.run(moneyLabelHideAction)
            switchEvent(oldDock: dockButtonsMenu, newDock: dockButtonsGame)
        } else {
            moneyLabel.position.y = 0
            showDockButtonsUnanimated(dockButtons: dockButtonsGame)
        }
    }

    // MARK: Logical methods

    ///  Create buttons witch are needed for interaction with a set of quick settings
    private func createMenuDockButtons() {
        let speakerSwitchButton = StateDockButton(ellipseSize: backgroundEllipseSize,
                                                  buttonImages: ButtonImages.speakerSwitch,
                                                  dockButtonType: .speakerSwitch)
        let gameCenterButton = SimpleDockButton(ellipseSize: backgroundEllipseSize,
                                                buttonImage: GameImages.cart.rawValue, dockButtonType: .powerupStore)
        let darkModeToggleButton = StateDockButton(ellipseSize: backgroundEllipseSize,
                                                   buttonImages: ButtonImages.darkModeToggle,
                                                   dockButtonType: .darkModeToggle)
        let resetButton = SimpleDockButton(ellipseSize: backgroundEllipseSize, buttonImage: "ResetIcon",
                                           dockButtonType: .reset)

        dockButtonsMenu = [speakerSwitchButton,
                           gameCenterButton,
                           darkModeToggleButton,
                           resetButton]

        self.addChild(speakerSwitchButton)
        self.addChild(gameCenterButton)
        self.addChild(darkModeToggleButton)
        self.addChild(resetButton)
    }

    private func createPlayDockButtons() {
        let magnet = PowerUpDockButtons(ellipseSize: backgroundEllipseSize, buttonImage: GameImages.magnet.rawValue,
                                        dockButtonType: .magnet)
        let overlap = PowerUpDockButtons(ellipseSize: backgroundEllipseSize, buttonImage: GameImages.overlap.rawValue,
                                         dockButtonType: .overlap)
        let stopCircle = PowerUpDockButtons(ellipseSize: backgroundEllipseSize,
                                            buttonImage: GameImages.stopRotation.rawValue, dockButtonType: .stopCircle)
        let freeze = PowerUpDockButtons(ellipseSize: backgroundEllipseSize, buttonImage: GameImages.freze.rawValue,
                                        dockButtonType: .freeze)

        dockButtonsGame = [magnet,
                           overlap,
                           stopCircle,
                           freeze]

        self.addChild(magnet)
        self.addChild(overlap)
        self.addChild(stopCircle)
        self.addChild(freeze)
    }

    public func showDockButtonsUnanimated(dockButtons: [DockButton]) {
        let startPoint = -size.width / 2

        let space = size.width / CGFloat(dockButtons.count)

        for index in 1 ... dockButtons.count {
            let x = startPoint + (space * CGFloat(index)) - space / 2
            dockButtons[index - 1].changeXWithAutomaticY(newX: x)
        }
    }

    public func showDockButtons(dockButtons: [DockButton]) {
        for button in dockButtons {
            button.changeXWithAutomaticY(newX: -600)
        }

        let startPoint = -size.width / 2

        let space = size.width / CGFloat(dockButtons.count)

        for index in 1 ... dockButtons.count {
            let x = startPoint + (space * CGFloat(index)) - space / 2
            dockButtons[index - 1].moveButtonAnimatedTo(x, duration: 0.15 * CGFloat(index),
                                                        waitTime: 0.25 * CGFloat(dockButtons.count - index),
                                                        completion: {})
        }
    }

    public func hideDockButtons(dockButtons: [DockButton]) {
        let endPoint = size.width / 2 + 500

        for index in 1 ... dockButtons.count {
            let currentButton = dockButtons[index - 1]
            currentButton.moveButtonAnimatedTo(endPoint, duration: 0.15 * CGFloat(index),
                                               waitTime: 0.20 * CGFloat(dockButtons.count - index), completion: {})
        }
    }

    func switchEvent(oldDock: [DockButton], newDock: [DockButton]) {
        var calledHideAction = false
        let hideMenuAction = SKAction.customAction(withDuration: 0.1) { _, _ in
            guard calledHideAction == false else {
                return
            }
            calledHideAction = true

            self.hideDockButtons(dockButtons: oldDock)
        }

        var calledShowAction = false
        let showMenuAction = SKAction.customAction(withDuration: 0.1) { _, _ in
            guard calledShowAction == false else {
                return
            }

            calledShowAction = true

            self.showDockButtons(dockButtons: newDock)
        }

        self.run(SKAction.group([hideMenuAction,
                                 SKAction.sequence([SKAction.wait(forDuration: 1.5),
                                                    showMenuAction])]))
    }
}
