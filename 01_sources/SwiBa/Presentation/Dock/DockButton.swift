//
//  DB.swift
//  SwiBa
//
//  Created by Franz Murner on 25.09.21.
//

import SpriteKit

protocol DockButton {
    func moveButtonAnimatedTo(_ newX: CGFloat, duration: CGFloat, waitTime: CGFloat, completion: @escaping () -> Void)

    func changeXWithAutomaticY(newX: CGFloat)
}
