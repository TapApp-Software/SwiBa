//
//  TouchableLabelNode.swift
//  SwiBa
//
//  Created by Franz Murner on 28.09.21.
//

import SpriteKit

class TouchableLabelNode: SKNode {
    // MARK: - Nodes

    private var labelNode: TALabelNode!
    private var labelBackgroundNode: SKShapeNode!

    // MARK: - Parameters

    private var fillColor: GameColor
    private var strokeColor: GameColor

    private let size = CGSize(width: 130, height: 60)

    // MARK: - Initialiser

    init(fillColor: GameColor, strokeColor: GameColor) {
        self.fillColor = fillColor
        self.strokeColor = strokeColor

        super.init()

        setupLabelNode()
        setColor()
        registerForEvents()
    }

    override convenience init() {
        self.init(fillColor: GameColor.buttonColor, strokeColor: GameColor.separatorColor)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Methods

    // MARK: Setup

    private func setupLabelNode() {
        labelBackgroundNode =
            SKShapeNode(rect: CGRect(x: -size.width / 2, y: -size.height / 2, width: size.width, height: size.height),
                        cornerRadius: size.height / 2)
        labelBackgroundNode.zPosition = 1

        labelNode = TALabelNode(maxLabelSize: size)
        labelNode.setLabelText(with: "N/A")

        addChild(labelBackgroundNode)
        labelBackgroundNode.addChild(labelNode)
    }

    private func setColor() {
        labelBackgroundNode.fillColor = fillColor.getColor()
        labelBackgroundNode.strokeColor = strokeColor.getColor()
    }

    private func registerForEvents() {
        EventBus.shared.subscribe(event: .UIInterfaceStyleUpdate) { _ in
            self.setColor()
        }
    }

    public func setLabelText(with text: String) {
        labelNode.setLabelText(with: text)
    }
}
