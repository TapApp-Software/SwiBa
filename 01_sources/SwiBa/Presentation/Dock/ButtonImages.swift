//
//  ButtonImages.swift
//  SwiBa
//
//  Created by Franz Murner on 25.09.21.
//

import SpriteKit

class ButtonImages {
    static var speakerSwitch = [ButtonState.on: GameImages.soundOnIcon.rawValue,
                                ButtonState.off: GameImages.soundOffIcon.rawValue,
                                ButtonState.lockedOn: nil,
                                ButtonState.lockedOff: nil]

    static var darkModeToggle = [ButtonState.on: "DarkModeToggle",
                                 ButtonState.off: "DarkModeToggle",
                                 ButtonState.lockedOn: "DarkModeToggleLock",
                                 ButtonState.lockedOff: "DarkModeToggleLock"]
}
