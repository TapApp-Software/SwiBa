//
//  CircledImageButton.swift
//  SwiBa
//
//  Created by Franz Murner on 27.09.21.
//

import SpriteKit

class CircledImageButton: SKNode {
    private var backgroundCircle: SKShapeNode!
    private var imageNode: SKSpriteNode!
    private var blendNode: SKShapeNode!

    // MARK: - Parameters

    private let radius: CGFloat = GameConstants.dockButtonRadius
    private var fillColor: GameColor
    private var strokeColor: GameColor
    private var lineWidth: CGFloat
    private var buttonImage: String?

    init(fillColor: GameColor, strokeColor: GameColor, lineWidth: CGFloat = 0, buttonImage: String?) {
        self.fillColor = fillColor
        self.strokeColor = strokeColor
        self.lineWidth = lineWidth
        self.buttonImage = buttonImage
        super.init()
        drawButton()

        registerForEvents()
        setColor()
        setImage()
    }

    convenience init(buttonImage: String?) {
        self.init(fillColor: GameColor.buttonColor, strokeColor: GameColor.separatorColor, buttonImage: buttonImage)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func drawButton() {
        self.name = "DockButton"
        backgroundCircle = SKShapeNode(circleOfRadius: radius)
        backgroundCircle.name = "DockButton"
        backgroundCircle.lineWidth = lineWidth
        imageNode = SKSpriteNode()
        imageNode.name = "DockButton"
        self.addChild(backgroundCircle)
        self.addChild(imageNode)
    }

    private func registerForEvents() {
        EventBus.shared.subscribe(event: .UIInterfaceStyleUpdate) { _ in
            self.setColor()
            self.setImage()
        }
    }

    private func setColor() {
        backgroundCircle.fillColor = fillColor.getColor()
        backgroundCircle.strokeColor = strokeColor.getColor()
    }

    private func setImage() {
        let size = radius * 1.4
        guard let imageName = buttonImage
        else { return }

        let textureAppearanceString = UITraitCollection.current.userInterfaceStyle == .dark ? "Dark" : "Light"
        let textureString = "\(imageName)-\(textureAppearanceString)"

        let texture = SKTexture(imageNamed: textureString)
        imageNode.texture = texture
        imageNode.size = HelperFunctions.textureRatioResize(textureSize: texture.size(),
                                                            newSize: CGSize(width: size, height: size))
    }

    internal func setImage(withName buttonImage: String) {
        self.buttonImage = buttonImage
        setImage()
    }

    /// A animation for a smooth spring effect
    private func onTouchesEndedAnimation() {
        self.run(TAActions.scaleFlex(from: 0.95, to: 1, withDuration: 1,
                                     withTimingFunction: TATimingFunction.elasticEaseOutExtreme(_:)))
    }
}

extension CircledImageButton {
    override func touchesBegan(_: Set<UITouch>, with _: UIEvent?) {
        self.removeAllActions()
        self.setScale(0.95)
        blendNode = SKShapeNode(circleOfRadius: radius)
        blendNode.fillColor = GameColor.black.getColor().withAlphaComponent(0.3)
        blendNode.strokeColor = .clear
        blendNode.name = "DockButton"
        addChild(blendNode)
    }

    override func touchesCancelled(_: Set<UITouch>, with _: UIEvent?) {
        blendNode.removeFromParent()
        onTouchesEndedAnimation()
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        parent?.touchesEnded(touches, with: event)
        blendNode.removeFromParent()
        onTouchesEndedAnimation()
    }
}
