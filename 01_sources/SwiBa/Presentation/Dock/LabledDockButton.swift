//
//  LabledDockButton.swift
//  SwiBa
//
//  Created by Franz Murner on 28.09.21.
//

import SpriteKit

class LabeledDockButton: SimpleDockButton {
    // MARK: - Nodes

    private var labelNode: TouchableLabelNode

    // MARK: - Parameters

    private let activePosition: CGFloat = -120
    private let labelMovementDuration: TimeInterval = 0.5

    // MARK: - Initialiser

    override init(ellipseSize: CGSize, fillColor: GameColor, strokeColor: GameColor, buttonImage: String?,
                  dockButtonType: DockButtonType) {
        labelNode = TouchableLabelNode(fillColor: fillColor, strokeColor: strokeColor)
        super.init(ellipseSize: ellipseSize, fillColor: fillColor, strokeColor: strokeColor, buttonImage: buttonImage,
                   dockButtonType: dockButtonType)
        self.addChild(labelNode)
        registerForLabelUpdates()
    }

    convenience init(ellipseSize: CGSize, buttonImage: String?, dockButtonType: DockButtonType) {
        self.init(ellipseSize: ellipseSize, fillColor: GameColor.buttonColor, strokeColor: GameColor.separatorColor,
                  buttonImage: buttonImage, dockButtonType: dockButtonType)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Methods

    // MARK: Action

    private func labelHideMovementAction() -> SKAction {
        let action = SKAction.group([SKAction.fadeOut(withDuration: labelMovementDuration),
                                     SKAction.move(to: CGPoint.zero, duration: labelMovementDuration)])

        action.timingMode = .easeOut

        return action
    }

    private func labelShowMovementAction() -> SKAction {
        let action = SKAction.group([SKAction.fadeIn(withDuration: labelMovementDuration),
                                     SKAction.move(to: CGPoint(x: 0, y: activePosition),
                                                   duration: labelMovementDuration)])

        action.timingMode = .easeOut

        return action
    }

    override func moveButtonAnimatedTo(_ newX: CGFloat, duration: CGFloat, waitTime: CGFloat,
                                       completion: @escaping () -> Void = {}) {
        if newX == 1000 {
            // Hide buttons/label from user

            labelNode.run(labelHideMovementAction()) {
                super.moveButtonAnimatedTo(newX, duration: duration, waitTime: waitTime, completion: completion)
            }
        } else {
            // Show buttons/label to user
            super.moveButtonAnimatedTo(newX, duration: duration, waitTime: waitTime) {
                EventBus.shared.publish(event: HapticEvent(hapticLevel: .rigid))
                self.labelNode.run(self.labelShowMovementAction(), completion: completion)
            }
        }
    }

    private func registerForLabelUpdates() {
        EventBus.shared.subscribe(event: .labelButtonUpdateEvent) { event in
            guard let event = event as? LabelButtonUpdateEvent,
                  self.type == event.buttonType
            else { return }
            self.labelNode.setLabelText(with: event.labelValue)
        }
    }
}
