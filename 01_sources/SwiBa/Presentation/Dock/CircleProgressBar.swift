//
//  CircleTimer.swift
//  SwiBa
//
//  Created by Franz Murner on 29.09.21.
//

import SpriteKit

class CircleProgressBar: SKNode {
    // MARK: - Nodes

    private var progressCircle: SKShapeNode

    // MARK: - Properties

    private var radius: CGFloat
    private var fillColor: GameColor
    private var lineWidth: CGFloat
    private var strokeColor: GameColor

    // MARK: - Initialisation

    init(fillColor: GameColor, strokeColor: GameColor, lineWidth: CGFloat = 0, radius: CGFloat) {
        self.radius = radius
        self.fillColor = fillColor
        self.lineWidth = lineWidth
        self.strokeColor = strokeColor

        progressCircle = SKShapeNode()
        progressCircle.zRotation = CGFloat.pi / 2
        progressCircle.zPosition = 2
        progressCircle.lineWidth = lineWidth

        let touchProtectionNode = SKShapeNode(circleOfRadius: radius)
        touchProtectionNode.strokeColor = .clear
        touchProtectionNode.fillColor = .clear
        touchProtectionNode.zPosition = 1

        super.init()

        self.addChild(touchProtectionNode)
        self.addChild(progressCircle)

        self.isHidden = true

        setColor()
        registerForEvents()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Methods

    /// The progress circle will  be reduced clockwise
    /// - Parameters:
    ///   - duration: the duration for this animation
    ///   - completion: a completion that called if the animation is finishing - optional parameter!
    public func startProgressCircle(duration: CGFloat, completion: @escaping () -> Void) {
        self.isHidden = false

//        progressCircle.removeAllActions()

        let action = SKAction.customAction(withDuration: TimeInterval(duration)) { _, progress in
            self.circleProgressBarPath(percent: (100 / duration) * (duration - progress))
        }

        progressCircle.run(action, completion: {
            self.stopProgressCircle()
            completion()
        })
    }

    public func stopProgressCircle() {
        progressCircle.removeAllActions()
        self.isHidden = true
    }

    private func circleProgressBarPath(percent: CGFloat) {
        let path = CGMutablePath()
        let endAngle = CGFloat.pi * 0.02 * percent
        path.addArc(center: CGPoint.zero, radius: radius, startAngle: 0, endAngle: endAngle, clockwise: false)
        path.addLine(to: CGPoint.zero)
        path.closeSubpath()
        progressCircle.path = path
    }

    private func setColor() {
        progressCircle.fillColor = fillColor.getColor()
        progressCircle.strokeColor = strokeColor.getColor()
    }

    private func registerForEvents() {
        EventBus.shared.subscribe(event: .UIInterfaceStyleUpdate) { _ in
            self.setColor()
        }
    }
}
