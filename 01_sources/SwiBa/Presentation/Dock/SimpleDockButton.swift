//
//  SimpleDockButton.swift
//  SwiBa
//
//  Created by Franz Murner on 03.09.21.
//

import SpriteKit

class SimpleDockButton: SKNode, DockButton {
    // MARK: - Nodes

    internal var circledImageButton: CircledImageButton

    // MARK: - Parameters

    private var ellipseSize: CGSize
    internal var type: DockButtonType

    // MARK: - Init

    init(ellipseSize: CGSize, fillColor: GameColor, strokeColor: GameColor, buttonImage: String?,
         dockButtonType: DockButtonType) {
        circledImageButton = CircledImageButton(fillColor: fillColor, strokeColor: strokeColor, lineWidth: 2,
                                                buttonImage: buttonImage)
        circledImageButton.zPosition = 2
        self.type = dockButtonType
        self.ellipseSize = ellipseSize
        super.init()

        addChild(circledImageButton)

        self.zPosition = 20
        self.changeXWithAutomaticY(newX: -600)
    }

    convenience init(ellipseSize: CGSize, buttonImage: String?, dockButtonType: DockButtonType) {
        self.init(ellipseSize: ellipseSize, fillColor: GameColor.buttonColor, strokeColor: GameColor.separatorColor,
                  buttonImage: buttonImage, dockButtonType: dockButtonType)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Logic

    /// Change the y position dependently of the x position over trigonome
    /// - Parameter newX
    internal func changeXWithAutomaticY(newX: CGFloat) {
        guard let newY = calculateCircleYPosition(xPosition: newX) else { return }
        self.position = CGPoint(x: newX, y: newY)
    }

    /// The method thats  calculate the y position of a x value in a circle
    /// - Parameter xPosition: The new x-position, for the new y
    /// - Returns: The new y-position dependable of the x-position
    private func calculateCircleYPosition(xPosition: CGFloat) -> CGFloat? {
        let widthToHightRatio = ellipseSize.height / ellipseSize.width

        guard xPosition < ellipseSize.width else {
            return nil
        }

        guard xPosition != 0 else {
            return ellipseSize.height / 2
        }

        let xUnit = xPosition / (ellipseSize.width * widthToHightRatio)
        let alpha = acos(xUnit)
        let yUnit = sin(alpha)
        let yBorder = yUnit * (ellipseSize.height * widthToHightRatio)

        return yBorder
    }

    /// Animate the movement to the new button position with automaticY calculation
    /// - Parameters:
    ///   - newX
    ///   - duration
    ///   - waitTime: The time for the delay of the animation
    ///   - completion: A completion handler thats called if the animation finishing -- This is a optional parameter
    internal func moveButtonAnimatedTo(_ newX: CGFloat, duration: CGFloat, waitTime: CGFloat,
                                       completion: @escaping () -> Void = {}) {
        let oldX: CGFloat = self.position.x
        let difference = newX - oldX

        let action = SKAction.customAction(withDuration: TimeInterval(duration)) { node, progress in
            guard let simpleDockButton = node as? SimpleDockButton else { return }
            simpleDockButton.changeXWithAutomaticY(newX: oldX + difference / duration * progress)
        }

        action.timingMode = .easeOut

        if newX == 1000 {
            self.run(SKAction.sequence([SKAction.wait(forDuration: TimeInterval(waitTime)),
                                        action]), completion: completion)
        } else {
            self.run(SKAction.sequence([SKAction.wait(forDuration: TimeInterval(waitTime)),
                                        action]), completion: {
                EventBus.shared.publish(event: HapticEvent(hapticLevel: .rigid))
                completion()
            })
        }
    }
}

// MARK: - Touch Handlers

extension SimpleDockButton {
    override func touchesEnded(_: Set<UITouch>, with _: UIEvent?) {
        EventBus.shared.publish(event: ButtonPressedEvent(buttonType: type))
    }
}
