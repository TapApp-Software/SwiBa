//
//  DockButtonType.swift
//  SwiBa
//
//  Created by Franz Murner on 29.09.21.
//

import Foundation

enum DockButtonType {
    case none
    case speakerSwitch
    case powerupStore
    case reset
    case darkModeToggle // Menu buttons
    case magnet
    case overlap
    case stopCircle
    case freeze // PowerUp buttons
}
