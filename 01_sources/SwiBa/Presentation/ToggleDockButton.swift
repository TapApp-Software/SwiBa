//
//  ToggleDockButton.swift
//  SwiBa
//
//  Created by Franz Murner on 14.09.21.
//

import SpriteKit

class ToggleDockButton: DockButton {
    var isOn: Bool = false
    var onImageName: String
    var offImageName: String

    init(ellipseSize: CGSize, fillColor: GameColor, strokeColor: GameColor, dockButtonType: DockButtonType,
         onImageName: String, offImageName: String) {
        self.offImageName = offImageName
        self.onImageName = onImageName
        super.init(ellipseSize: ellipseSize, fillColor: fillColor, strokeColor: strokeColor, imageName: nil,
                   dockButtonType: dockButtonType)

        setIconState()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setIconState() {
        imageName = isOn ? onImageName : offImageName
        setImage()
    }
}

extension ToggleDockButton {
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        EventBus.singleton.publish(event: ButtonPressedEvent(buttonType: type, backcall: { val in
            self.isOn = val
            self.setIconState()
        }))
    }
}
