//
//  BallTouchHandler.swift
//  SwiBa
//
//  Created by Franz Murner on 07.08.21.
//

import SpriteKit

class BallField: SKNode {
    var isInteractionActive = false
    var currentTouchedNode: Ball?
    var size: CGSize

    init(size: CGSize) {
        self.size = size
        super.init()
        self.isUserInteractionEnabled = false
        self.zPosition = 2
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func getAllBalls() -> [Ball] {
        var balls = [Ball]()
        for node in self.children {
            guard let ball = node as? Ball else {
                continue
            }

            balls.append(ball)
        }

        return balls
    }

    func addBall(color: GameColor) {
        let ball = Ball(color: color)
        ball.position.y = CGFloat.random(in: 0 ... 400) - 200
        ball.position.x = CGFloat.random(in: 0 ... 400) - 200
        self.addChild(ball)
    }

    func removeAllBalls() {
        for node in self.children {
            guard let ball = node as? Ball else {
                continue
            }

            ball.removeBall()
        }
    }

    func touchesBegan(node: SKNode) {
        guard isInteractionActive else { return }
        guard let ballNode = node.parent?.parent as? Ball, currentTouchedNode == nil else { return }
        currentTouchedNode = ballNode
        ballNode.onTouchAnimation()
    }

    func touchesMoved(node _: SKNode, position: CGPoint) {
        guard isInteractionActive else { return }

        guard let currentTouchedNode = currentTouchedNode else { return }
        guard currentTouchedNode.isTouchEnabled else { return }

        let radius: CGFloat = size.width / 2

        // Calculate the y-Threshold to prevent moving-out the collision area
        let xUnit = position.x / radius
        let alpha = acos(xUnit)
        let yUnit = sin(alpha)
        let yThreshold = yUnit * radius

        if position.y > yThreshold {
            currentTouchedNode.position = CGPoint(x: position.x, y: yThreshold)
            currentTouchedNode.isTouchEnabled = false
        } else if position.y < -yThreshold {
            currentTouchedNode.position = CGPoint(x: position.x, y: -yThreshold)
            currentTouchedNode.isTouchEnabled = false
        } else {
            currentTouchedNode.position = position
        }
    }

    func touchesEnded(node _: SKNode) {
        guard isInteractionActive else {
            currentTouchedNode = nil
            return
        }

        guard let currentTouchedNodeSave = currentTouchedNode else {
            return
        }

        currentTouchedNodeSave.removeFromParent()
        self.addChild(currentTouchedNodeSave)

        currentTouchedNodeSave.onDropAnimation()
        currentTouchedNode = nil
    }
}
