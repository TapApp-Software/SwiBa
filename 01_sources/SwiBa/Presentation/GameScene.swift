//
//  GameScene.swift
//  SwiBa
//
//  Created by Franz Murner on 30.07.21.
//

import SpriteKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    var colorCircle: ColorCircle!
    var ballField: BallField!
    var scoreBoard: ScoreBoard!
    var goMenu: GoMenu!
    var gameControlSystem: GameControlSystem!
    var dockMenu: Dock!
    var lockedTouchObject: SKNode?
    var audioNode: SKNode!

    var gameMainNode: SKNode!

    override init(size: CGSize) {
        super.init(size: size)

        EventBus.shared.subscribe(event: .touchLockOnEvent) { _ in
            self.isUserInteractionEnabled = false
        }

        EventBus.shared.subscribe(event: .touchLockOffEvent) { _ in
            self.isUserInteractionEnabled = true
        }
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func sceneDidLoad() {
        setupGameConstants()

        physicsWorld.contactDelegate = self

        setupScene()

        initializeElements()

        gameControlSystem = GameControlSystem(gameScene: self)

        subscribeEvents()

        let introScene = IntroScreen(sceneSize: size)
        introScene.zPosition = 1000
        addChild(introScene)
    }

    func setupScene() {
        self.backgroundColor = GameColor.primaryBackground.getColor()
        EventBus.shared.subscribe(event: .UIInterfaceStyleUpdate) { _ in
            self.backgroundColor = GameColor.primaryBackground.getColor()
        }
    }

    func initializeElements() {
        gameMainNode = SKNode()
        ballField = BallField(size: size)
        colorCircle = ColorCircle()
        scoreBoard = ScoreBoard(size: size)
        goMenu = GoMenu(size: size)
        dockMenu = Dock(size: size)
        audioNode = AudioNode()

        self.addChild(gameMainNode)

        gameMainNode.addChild(ballField)
        gameMainNode.addChild(colorCircle)
        gameMainNode.addChild(scoreBoard)
        gameMainNode.addChild(goMenu)
        gameMainNode.addChild(dockMenu)
        gameMainNode.addChild(audioNode)
    }

    func setupGameConstants() {
        GameConstants.circleRadius = size.width / 2 - 20.0
    }

    func subscribeEvents() {}
}

extension GameScene {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let node = self.atPoint(touch.location(in: self))
            if node.name == "Ball", gameControlSystem.gameActive {
                ballField.touchesBegan(node: node)
                lockedTouchObject = node
                EventBus.shared.publish(event: HapticEvent(hapticLevel: .soft))
            }

            if node.name == "GO-Button" {
                goMenu.touchesBegan()
                lockedTouchObject = goMenu
                EventBus.shared.publish(event: HapticEvent(hapticLevel: .heavy))
            }

            if node.name == "DockButton" {
                node.parent?.touchesBegan(touches, with: event)
                lockedTouchObject = node.parent
                EventBus.shared.publish(event: HapticEvent(hapticLevel: .medium))
            }
        }
    }

    override func touchesMoved(_ touches: Set<UITouch>, with _: UIEvent?) {
        for touch in touches {
            let node = self.atPoint(touch.location(in: self))
            ballField.touchesMoved(node: node, position: touch.location(in: self))
        }
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            guard let lockedTouchObject = lockedTouchObject else { return }

            let node = self.atPoint(touch.location(in: self))

            if lockedTouchObject.name == "Ball" {
                ballField.touchesEnded(node: node)
            }

            if lockedTouchObject.name == node.name, lockedTouchObject.name == "GO-Button" {
                goMenu.touchesEnded()
                EventBus.shared.publish(event: HapticEvent(hapticLevel: .soft))
            } else if node.name == nil, lockedTouchObject.name == "GO-Button" {
                goMenu.touchesCancelled()
                EventBus.shared.publish(event: HapticEvent(hapticLevel: .soft))
            }

            if lockedTouchObject.name == node.name, lockedTouchObject.name == "DockButton" {
                lockedTouchObject.touchesEnded(touches, with: event)
                EventBus.shared.publish(event: HapticEvent(hapticLevel: .soft))
            } else if node.name == nil, lockedTouchObject.name == "DockButton" {
                lockedTouchObject.touchesCancelled(touches, with: event)
                EventBus.shared.publish(event: HapticEvent(hapticLevel: .soft))
            }

            self.lockedTouchObject = nil
        }
    }
}

extension GameScene {
    func didBegin(_ contact: SKPhysicsContact) {
        let nodeA = contact.bodyA.node
        let nodeB = contact.bodyB.node

        if nodeA?.name == "Segment" {
            guard let ball = nodeB as? Ball, let segment = nodeA as? Segment else { return }
            guard !ball.isBallCollided else { return }
            ball.isBallCollided = true
            EventBus.shared.publish(event: CollisionEvent(ball: ball, segment: segment))
        } else if nodeB?.name == "Segment" {
            guard let ball = nodeA as? Ball, let segment = nodeB as? Segment else { return }
            guard !ball.isBallCollided else { return }
            ball.isBallCollided = true
            EventBus.shared.publish(event: CollisionEvent(ball: ball, segment: segment))
        }
    }
}
