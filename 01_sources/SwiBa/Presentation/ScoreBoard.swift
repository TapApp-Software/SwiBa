//
//  ScoreBoard.swift
//  SwiBa
//
//  Created by Franz Murner on 16.08.21.
//

import SpriteKit

class ScoreBoard: SKNode {
    private var backgroundCircle: SKShapeNode!
    private var scoreLabel: TALabelNode!
    private var highScoreTextLabel: TALabelNode!
    private var highScoreLabel: TALabelNode!
    private var size: CGSize

    private var backgroundCircleStrokeColor = GameColor.separatorColor

    init(size: CGSize) {
        self.size = size
        super.init()
        self.position.y = size.height - (size.height * 0.45)
        drawBackgroundCircle()
        createScoreLabel()
        setColor()
        registerForEvents()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func drawBackgroundCircle() {
        backgroundCircle =
            SKShapeNode(ellipseIn: CGRect(x: -size.width, y: -size.height / 4, width: size.width * 2,
                                          height: size.height / 2))
        backgroundCircle.glowWidth = 2
        backgroundCircle.isAntialiased = true
        self.addChild(backgroundCircle)
    }

    private func createScoreLabel() {
        let backgroundCircleBottomPosition = -backgroundCircle.frame.height / 2

        let scoreLabelFontSize: CGFloat = 200
        let highScoreTextLabelFontSize: CGFloat = 70
        let highScoreLabelFontSize: CGFloat = 70

        scoreLabel = TALabelNode(maxLabelSize: CGSize(width: size.width / 2, height: scoreLabelFontSize),
                                 labelType: .scoreLabel)
        scoreLabel.position.y = (backgroundCircleBottomPosition + (scoreLabelFontSize / 4)) + 50

        highScoreTextLabel = TALabelNode(maxLabelSize: CGSize(width: size.width, height: highScoreTextLabelFontSize),
                                         labelType: .highScoreTextLabel)
        highScoreTextLabel.position.y = (backgroundCircleBottomPosition + (highScoreTextLabelFontSize / 4)) + 270

        highScoreLabel = TALabelNode(maxLabelSize: CGSize(width: size.width, height: highScoreLabelFontSize),
                                     labelType: .highScoreLabel)
        highScoreLabel.position
            .y = (backgroundCircleBottomPosition + (highScoreLabelFontSize / 4)) + 270 - highScoreTextLabelFontSize

        self.addChild(highScoreTextLabel)
        self.addChild(highScoreLabel)
        self.addChild(scoreLabel)
    }

    private func setColor() {
        guard backgroundCircle != nil, scoreLabel != nil else {
            return
        }

        backgroundCircle.fillColor = GameColor.secondaryBackgound.getColor()
        backgroundCircle.strokeColor = backgroundCircleStrokeColor.getColor()
    }

    private func blendStrokeColor(newColor: GameColor?, animationDuration: CGFloat) {
        let colorBlendActionKey = "colorBlendAction"
        backgroundCircle.removeAction(forKey: colorBlendActionKey)

        let oldColorValue = backgroundCircleStrokeColor.getColor()

        backgroundCircleStrokeColor = GameColor.separatorColor

        if let comboColor = newColor {
            backgroundCircleStrokeColor = comboColor
        }

        let newColorValue = backgroundCircleStrokeColor.getColor()

        let colorTransitionAction = SKAction.colorTransitionAction(fromColor: oldColorValue, toColor: newColorValue,
                                                                   duration: animationDuration)
        backgroundCircle.run(colorTransitionAction, withKey: colorBlendActionKey)
    }

    private func generateComboLabel(text: String) {
        let backgroundCircleBottomPosition = -backgroundCircle.frame.height / 2

        let label = TALabelNode(startFontSize: 50, text: text)
        label.position.y = backgroundCircleBottomPosition

        let action = SKAction.sequence([SKAction.wait(forDuration: 0.3),
                                        SKAction.group([SKAction.moveBy(x: 0, y: 50, duration: 1.0),
                                                        SKAction.fadeAlpha(to: 0, duration: 1.0)]),
                                        SKAction.removeFromParent()])

        self.addChild(label)
        label.run(action)
    }

    private func registerForEvents() {
        EventBus.shared.subscribe(event: .UIInterfaceStyleUpdate) { _ in
            self.setColor()
        }

        EventBus.shared.subscribe(event: .scoreBoardStrokeEvent) { event in
            guard let colorComboEvent = event as? ScoreBoardStrokeEvent else { return }
            self.blendStrokeColor(newColor: colorComboEvent.currentComboColor,
                                  animationDuration: colorComboEvent.animationDuration)
        }

        EventBus.shared.subscribe(event: .comboLevelEvent) { event in
            guard let comboLabelEvent = event as? ComboLevelEvent else { return }
            self.generateComboLabel(text: comboLabelEvent.text)
        }
    }
}
