//
//  CircleController.swift
//  SwiBa
//
//  Created by Franz Murner on 30.07.21.
//

import SpriteKit

class ColorCircle: SKNode {
    // MARK: - Subclasses

    private var segments = [Segment]()

    // MARK: - Properties

    private var radius = GameConstants.circleRadius ?? 450
    private var currentRPM: CGFloat = 0
    private var isAddingSegment = false
    private var circleRotationTimer: TATimer?

    private(set) var isRotating: Bool = false

    override init() {
        super.init()

        circleRotationTimer = TATimer(timeInterval: 0.01, timerType: .circleRotationTimer, repeatBlock: true,
                                      block: { timer in
                                          if self.zRotation >= CGFloat.pi * 2 || self.zRotation <= -CGFloat.pi * 2 {
                                              self.zRotation = 0
                                          }

                                          if self.currentRPM == 0 {
                                              timer.stop()
                                              self.isRotating = false
                                          }

                                          self.zRotation += 0.001 * self.currentRPM // 2*PI / 100 * 60 = 1 RPM
                                      })
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    /// Changes the rpm of the circle immediately
    /// - Parameter rpm: a positive value is counterclockwise and a negative value is clockwise
    public func changeRPM(to rpm: CGFloat) {
        currentRPM = rpm
    }

    /// Changes the rpm of the circle continuously to the rpm
    /// - Parameters:
    ///   - rpm: a positive value is counterclockwise and a negative value is clockwise
    ///   - completion: called if the duration is over (not needed)
    public func changeRPMAnimated(to rpm: CGFloat, in duration: TimeInterval, completion _: @escaping () -> Void = {}) {
        let actionKey = "RPM-Change"

        self.removeAction(forKey: actionKey)

        let oldRPM = currentRPM
        let difference = rpm - oldRPM

        let action = SKAction.customAction(withDuration: duration) { _, progress in
            self.currentRPM = oldRPM + difference / CGFloat(duration) * progress
        }

        action.timingMode = .easeIn

        self.run(action, withKey: actionKey)
    }

    public func stopRotation() {
        currentRPM = 0
    }

    /// Starting a timer that has a standart rpm of 1
    public func startRotation() {
        self.isRotating = true
        currentRPM = 0.0001
        circleRotationTimer?.start()
    }

    /// Adding a segment to the scene and in the segment list at the last position. And animate the circle dependently.
    /// - Parameters:
    ///   - animationSpeed: How long it takes to show the resize the segment
    ///   - completion: Called if the animation is completed
    public func addSegment(animationSpeed: CGFloat, color: GameColor, completion: @escaping () -> Void) {
        let addingEventKey = "addingSegment"
        if isAddingSegment {
            self.removeAction(forKey: addingEventKey)
            self.changeSizeOfLastSegment(to: 100 / CGFloat(segments.count))
        }

        let segment = Segment(start: 100, end: 100, color: color, radius: 450)
        segments.append(segment)
        self.addChild(segment)

        let size = 100 / CGFloat(segments.count)

        let animation = SKAction.customAction(withDuration: TimeInterval(animationSpeed)) { _, progress in
            self.changeSizeOfLastSegment(to: size / animationSpeed * CGFloat(progress))
        }

        self.run(animation) {
            self.isAddingSegment = false
            completion()
        }
    }

    /// Removing segment from node animated
    /// - Parameters:
    ///   - completion: Called if the animation is completed
    public func removeSegment(animationSpeed: CGFloat, completion: @escaping () -> Void) {
        guard segments.count > 1 else { return }

        let size = 100 / CGFloat(segments.count)

        let animation = SKAction.customAction(withDuration: TimeInterval(animationSpeed)) { _, progress in
            self.changeSizeOfLastSegment(to: size / animationSpeed * CGFloat(animationSpeed - progress))
        }

        self.run(animation) {
            self.segments.last?.removeFromParent()
            self.segments.removeLast()
            completion()
        }
    }

    /**
     Diese Funktion berechnet die x und y koordinaten der mitte eines  Segments

     - Parameter segmentID: Eine spezifische ID für das gewünschte Element (Beginnend bei 0)
     - Returns: Wenn die SegmentID ungültig ist dann kommt nil, ansonsten wird die Position zurückgegeben
     */
    public func getMidPointFrom(color: GameColor) -> CGPoint? {
        for segment in segments {
            if segment.color.getID() == color.getID() {
                let rotationInPercent = zRotation / (CGFloat.pi * 2) * 100
                let rot = segment.midPoint + rotationInPercent
                let x = cos(CGFloat.pi * 2 / 100 * rot) * radius
                let y = sin(CGFloat.pi * 2 / 100 * rot) * radius
                return CGPoint(x: x, y: y)
            }
        }
        return nil
    }

    /// This method reset the color-circle to its default state
    public func setupCircle(color1: GameColor, color2: GameColor) {
        self.removeAllChildren()
        self.removeAllActions()
        currentRPM = 0
        isRotating = false
        isAddingSegment = false

        segments = [Segment(start: 0, end: 50, color: color1, radius: 450),
                    Segment(start: 50, end: 100, color: color2, radius: 450)]

        for segment in segments {
            self.addChild(segment)
        }

        for segment in segments {
            segment.redraw()
        }
    }

    // MARK: - private Methods

    private func changeSizeOfLastSegment(to size: CGFloat) {
        guard segments.count > 1 else { return }
        guard let lastSegment = segments.last else { return }

        let diff = lastSegment.end - lastSegment.start
        let reduceValue = (size - diff) / CGFloat(segments.count - 1)

        for index in 0 ..< segments.count {
            let segment = segments[index]
            if index != 0 {
                segment.start -= reduceValue * CGFloat(index)
            }
            segment.redraw()
        }

        for index in 0 ..< segments.count {
            let segment = segments[index]

            if index != segments.count - 1 {
                segment.end = segments[index + 1].start
            }

            segment.redraw()
        }
    }
}
