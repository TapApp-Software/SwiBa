//
//  PowerUpStoreContentCard.swift
//  SwiBa
//
//  Created by Franz Murner on 28.11.21.
//

import SpriteKit

class PowerUpStoreContentCard: TANode {
    // MARK: - Nodes

    private var backgroundCard: TACard!
    private var imageNode: TARoundetImageNode!
    private var labelNode: TASKLabelNode!
    private var priceLabel: TAShapeNode!
    private var stockLabel: TASKLabelNode!
    private var addButton: TAButton!
    private var dockButtonType: DockButtonType

    // MARK: - Parameters

    private var subcardSize: CGSize
    private var space: CGFloat = 30

    // MARK: - Initializer

    init(size: CGSize, image: GameImages, labelText: String, priceLabel: LabelTypes, stockLabel: LabelTypes,
         dockButtonType: DockButtonType, addButtonType: ButtonType) {
        self.subcardSize = size
        self.dockButtonType = dockButtonType
        super.init()
        createBackgroundCard()
        setupImageNode(image: image)
        setupLabelNode(labelText: labelText)
        setupPriceLabel(labelType: priceLabel)
        setupAddButton(addButtonType: addButtonType)
        setupStockLabel(labelType: stockLabel)
        setupConstraints()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Methods

    private func createBackgroundCard() {
        backgroundCard = TACard(backgroundColor: .tertiaryBackground, size: subcardSize)
        backgroundCard.borderSize = subcardSize.applyBoth(-space * 2)
        self.addChild(backgroundCard)
    }

    private func setupImageNode(image: GameImages) {
        imageNode = TARoundetImageNode(imageName: image.rawValue, radius: 80, fillColor: GameColor.quaternaryBackground)
        backgroundCard.addChild(imageNode)
    }

    private func setupLabelNode(labelText: String) {
        labelNode = TASKLabelNode(lableWidth: backgroundCard.borderSize?.width ?? 300)
        labelNode.text = labelText
        labelNode.font = UIFont.tapFontRusticalFont(ofSize: 90)
        backgroundCard.addChild(labelNode)
    }

    private func setupPriceLabel(labelType: LabelTypes) {
        let pillSize = CGSize(width: 280, height: 80)

        priceLabel = TAShapeNode(size: pillSize, cornerRadius: pillSize.height / 2)
        priceLabel.fillColor = .quaternaryBackground

        let label = TASKLabelNode(lableWidth: 300, labelType: labelType)
        label.font = UIFont.tapFontRusticalFont(ofSize: 70)
        priceLabel.addChild(label)

        backgroundCard.addChild(priceLabel)
    }

    private func setupAddButton(addButtonType: ButtonType) {
        let pillSize = CGSize(width: backgroundCard.borderSize?.width ?? 300, height: 100)

        let addLabel = TAShapeNode(size: pillSize, cornerRadius: pillSize.height / 2)

        let label = TASKLabelNode(lableWidth: 300)
        label.font = UIFont.tapFontRusticalFont(ofSize: 90)
        label.text = "Add".localized()
        addLabel.addChild(label)

        addButton = TAButton(backgroundNode: addLabel, buttonType: addButtonType,
                             mainColor: GameColor.confirmGreen, onTouchColor: .confirmGreenPressed,
                             onDisabledColor: .confirmGreenDisabled)
        addButton.touchEndetHandler = PowerUpAddButtonPressEvent(buttonType: dockButtonType)

        backgroundCard.addChild(addButton)
    }

    private func setupStockLabel(labelType: LabelTypes) {
        stockLabel = TASKLabelNode(lableWidth: backgroundCard.borderSize?.width ?? 300, labelType: labelType)
        stockLabel.font = UIFont.tapFontRusticalFont(ofSize: 40)
        backgroundCard.addChild(stockLabel)
    }

    private func setupConstraints() {
        imageNode.taConstraints = [.setWhenPartnerIsParent(sourceNode: backgroundCard, direction: .top)]
        labelNode.taConstraints = [.setPartner(sourceNode: imageNode, direction: .bottom, withOffset: space - 10)]
        stockLabel.taConstraints = [.setPartner(sourceNode: labelNode, direction: .bottom, withOffset: -5)]
        priceLabel.taConstraints = [.setPartner(sourceNode: stockLabel, direction: .bottom, withOffset: space)]
        addButton.taConstraints = [.setWhenPartnerIsParent(sourceNode: backgroundCard, direction: .bottom)]
    }
}
