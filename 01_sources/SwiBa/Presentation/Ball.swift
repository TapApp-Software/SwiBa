//
//  Ball.swift
//  SwiBa
//
//  Created by Franz Murner on 05.08.21.
//

import SpriteKit

class Ball: SKNode {
    private let ballID = UUID()

    private var categoryBitMask = BitMasks.ball.rawValue
    private var collisionBitMask = BitMasks.ball.rawValue | BitMasks.outerRing.rawValue

    private var radius: CGFloat = GameConstants.ballRadius {
        didSet {
            changePath()
            setupPhysics()
        }
    }

    private let lineWidth: CGFloat = GameConstants.ballLineWidth

    var color: GameColor
    var node: SKShapeNode!
    var nodeShadow: SKShapeNode!
    var isBallCollided = false {
        didSet {
            isTouchEnabled = false
        }
    }

    var isTouchEnabled = true

    init(color: GameColor) {
        self.color = color
        super.init()
        setupBall()
        changePath()
        setupPhysics()
        onShowAnimation()
        registerForEvents()
    }

    deinit {
        EventBus.shared.unsubscribeAll(subscriberId: ballID)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setupBall() {
        nodeShadow = SKShapeNode()
        nodeShadow.lineWidth = lineWidth

        node = SKShapeNode()
        node.lineWidth = lineWidth
        setupColor()
        node.name = "Ball"
        self.addChild(nodeShadow)
        nodeShadow.addChild(node)
        self.zPosition = 2
        self.name = "Ball-Phy"
    }

    private func setupColor() {
        node.strokeColor = color.getColor()
        nodeShadow.strokeColor = UIColor.separator.withAlphaComponent(0.5)
        nodeShadow.glowWidth = 5
        node.fillColor = color.getColor()
    }

    private func setupPhysics() {
        self.physicsBody = SKPhysicsBody(circleOfRadius: radius + lineWidth / 2)
        self.physicsBody?.affectedByGravity = false
        self.physicsBody?.categoryBitMask = categoryBitMask
        self.physicsBody?.collisionBitMask = collisionBitMask
        self.physicsBody?.contactTestBitMask = BitMasks.segment.rawValue
        self.physicsBody?.usesPreciseCollisionDetection = true
    }

    public func setBallCollision(to status: Bool) {
        categoryBitMask = status ? BitMasks.ball.rawValue : BitMasks.ballNoCollision.rawValue
        collisionBitMask = status ? BitMasks.ball.rawValue | BitMasks.outerRing.rawValue : BitMasks.outerRing.rawValue
        node.strokeColor = color.getColor().withAlphaComponent(0.7)
        setupPhysics()
    }

    private func changePath() {
        let path = CGPath(ellipseIn: CGRect(x: -radius, y: -radius, width: radius * 2, height: radius * 2),
                          transform: nil)
        node.path = path
        nodeShadow.path = path
    }

    public func ballBlink(completionHandler: @escaping () -> Void = {}) {
        let blinkDuration: TimeInterval = 0.4
        let blinks = 3

        let blinkOnAction = SKAction.customAction(withDuration: 0) { _, _ in
            self.node.fillColor = self.color.getColor()
            EventBus.shared.publish(event: HapticEvent(hapticLevel: .heavy))
            EventBus.shared.publish(event: ScoreBoardStrokeEvent(currentComboColor: self.color, animationDuration: 0.1))
        }

        let blinkOffAction = SKAction.customAction(withDuration: 0) { _, _ in
            self.node.fillColor = GameColor.clear.getColor()
            EventBus.shared.publish(event: HapticEvent(hapticLevel: .medium))
            EventBus.shared.publish(event: ScoreBoardStrokeEvent(currentComboColor: nil, animationDuration: 0.1))
        }

        let blinkAction = SKAction.sequence([SKAction.wait(forDuration: blinkDuration / 2),
                                             blinkOnAction,
                                             SKAction.wait(forDuration: blinkDuration / 2),
                                             blinkOffAction])

        let action = SKAction.sequence([SKAction.repeat(blinkAction, count: blinks)])

        self.run(action, completion: completionHandler)
    }

    public func removeBall() {
        let actionPack = SKAction.group([SKAction.scaleAction(oldRadius: 25, newRadius: 300, animationDuration: 1,
                                                              timingFunction: TATimingFunction.quinticEaseOut(_:)),
                                         SKAction.reduceLineWidth(oldLineWidth: 50, newLineWidth: 0,
                                                                  animationDuration: 0.5,
                                                                  timingFunction: TATimingFunction
                                                                      .exponentialEaseOut(_:)),
                                         SKAction.fadeFillAlpha(start: 1, end: 0, animationDuration: 0.5),
                                         SKAction.sequence([SKAction.fadeAlpha(to: 0, duration: 0.2)])])

        node.run(actionPack)
        nodeShadow.run(actionPack) {
            self.removeFromParent()
        }
    }

    public func onTouchAnimation() {
        self.removeAllActions()
        self.radius = GameConstants.ballRadius

        self.run(scaleAction(oldRadius: GameConstants.ballRadius,
                             newRadius: GameConstants.getSizeWithPercetage(size: GameConstants.ballRadius),
                             animationDuration: 0.2))
        self.zPosition = 3
    }

    public func onShowAnimation() {
        guard !isBallCollided else {
            return
        }

        self.run(scaleAction(oldRadius: 0, newRadius: GameConstants.ballRadius, animationDuration: 1.0))
    }

    public func onDropAnimation() {
        guard !isBallCollided else {
            return
        }

        self.run(scaleAction(oldRadius: GameConstants.getSizeWithPercetage(size: GameConstants.ballRadius),
                             newRadius: GameConstants.ballRadius,
                             animationDuration: 1.0,
                             timingFunction: TATimingFunction.elasticEaseOut(_:)))
    }

    private func scaleAction(oldRadius: CGFloat,
                             newRadius: CGFloat,
                             animationDuration: TimeInterval,
                             timingFunction: @escaping (CGFloat) -> CGFloat = TATimingFunction
                                 .elasticEaseOutSoft) -> SKAction {
        let delta: CGFloat = newRadius - oldRadius

        let scaleAction = SKAction.customAction(withDuration: animationDuration) { _, timer in
            self.radius = oldRadius + delta * timingFunction(timer)
        }

        return scaleAction
    }

    private func registerForEvents() {
        EventBus.shared.subscribe(subscriberId: ballID, event: .UIInterfaceStyleUpdate) { [weak self] _ in
            self?.setupColor()
        }
    }
}
