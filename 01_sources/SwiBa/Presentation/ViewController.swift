//
//  ViewController.swift
//  SwiBa
//
//  Created by Franz Murner on 30.07.21.
//

import SpriteKit
import UIKit

class ViewController: UIViewController {
    static var currentAppAppearance: UIUserInterfaceStyle = .unspecified
    static var currentSystemAppearance: UIUserInterfaceStyle = .unspecified
    static var currentView: UIView?

    override func viewDidLoad() {
        super.viewDidLoad()

        ViewController.currentView = view

        ViewController.currentAppAppearance = ViewController.currentSystemAppearance

        if let view = self.view as? SKView {
            let sceneSize = CGSize(width: 1000, height: 1000)
            let newSceneHeight = view.frame.height / view.frame.size.width * sceneSize.width

            let scene = GameScene(size: CGSize(width: sceneSize.width, height: newSceneHeight))

            scene.anchorPoint = CGPoint(x: 0.5, y: 0.5)

            scene.scaleMode = .aspectFit

            view.ignoresSiblingOrder = false
            view.isMultipleTouchEnabled = false

            view.presentScene(scene)
//            view.showsPhysics = true
        }

        // Do any additional setup after loading the view.
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        if previousTraitCollection?.userInterfaceStyle != traitCollection.userInterfaceStyle {
            EventBus.shared
                .publish(event: UIInterfaceStyleUpdateNotification(currentMode: traitCollection.userInterfaceStyle))
        }
    }

    override var prefersStatusBarHidden: Bool {
        return !HelperFunctions.hasDeviceNotch()
    }
}
