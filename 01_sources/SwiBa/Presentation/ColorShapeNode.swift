//
//  ColorShapeNode.swift
//  SwiBa
//
//  Created by Franz Murner on 29.09.21.
//

import SpriteKit

/// A node with a colored texture for the go-circle
class ColorShapeNode: SKNode {
    // MARK: - Nodes

    private let colorPartsNode: SKSpriteNode
    private let overlayNode: SKShapeNode
    private let ringOverlay: SKShapeNode

    // MARK: - Properties

    private var radius: CGFloat
    private var lineWidth: CGFloat = GameConstants.colorCircleLineWidth

    // MARK: - Initialiser

    init(radius: CGFloat) {
        self.radius = radius

        colorPartsNode = SKSpriteNode()
        colorPartsNode.zPosition = 1

        overlayNode = SKShapeNode(circleOfRadius: radius)
        overlayNode.lineWidth = 20
        overlayNode.zPosition = 2

        ringOverlay = SKShapeNode(circleOfRadius: radius - lineWidth + 5)
        ringOverlay.lineWidth = lineWidth
        ringOverlay.zPosition = 3

        super.init()

        addChild(colorPartsNode)
        addChild(overlayNode)
        addChild(ringOverlay)

        setupTexture()
        setupOverlayColor()
        registerEvents()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func registerEvents() {
        EventBus.shared.subscribe(event: .UIInterfaceStyleUpdate) { _ in
            self.setupTexture()
            self.setupOverlayColor()
        }
    }

    private func setupOverlayColor() {
        overlayNode.strokeColor = GameColor.primaryBackground.getColor()
        overlayNode.fillColor = GameColor.goMenuOverlayFillColor.getColor()
        ringOverlay.fillColor = .clear
        ringOverlay.strokeColor = GameColor.goMenuOverlayStrokeColor.getColor()
    }

    private func setupTexture() {
        let backgroundTexture = drawBackgroundTexture()
        colorPartsNode.size = backgroundTexture?.size() ?? CGSize.zero
        colorPartsNode.texture = backgroundTexture
    }

    private func drawBackgroundTexture() -> SKTexture? {
        let maskNode = SKShapeNode(circleOfRadius: radius)
        maskNode.strokeColor = .clear
        maskNode.lineWidth = 0
        maskNode.fillColor = .white

        let cropNode = SKCropNode()
        cropNode.maskNode = maskNode

        let background = SKSpriteNode(color: .black, size: CGSize(width: radius * 3, height: radius * 3))
        cropNode.addChild(background)

        let effectNode = SKEffectNode()
        let blurFilter = CIFilter(name: "CIGaussianBlur", parameters: ["inputRadius": 350])
        effectNode.filter = blurFilter
        cropNode.addChild(effectNode)

        let partI = createBackgroundPart(positionManipulation: CGPoint(x: -1, y: 1), color: .systemBlue)
        effectNode.addChild(partI)

        let partII = createBackgroundPart(positionManipulation: CGPoint(x: 1, y: 1), color: .systemRed)
        effectNode.addChild(partII)

        let partIII = createBackgroundPart(positionManipulation: CGPoint(x: 1, y: -1), color: .systemGreen)
        effectNode.addChild(partIII)

        let partVI = createBackgroundPart(positionManipulation: CGPoint(x: -1, y: -1), color: .systemYellow)
        effectNode.addChild(partVI)

        return SKView().texture(from: cropNode)
    }

    private func createBackgroundPart(positionManipulation: CGPoint, color: GameColor) -> SKSpriteNode {
        let size = radius * 1.5
        let position = size / 2
        let sprite = SKSpriteNode()
        sprite.size = CGSize(width: size, height: size)
        sprite.position = CGPoint(x: positionManipulation.x * position, y: positionManipulation.y * position)
        sprite.color = color.getColor()

        return sprite
    }
}
