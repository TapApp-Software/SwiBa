//
//  HighScoreConfettiComponent.swift
//  SwiBa
//
//  Created by Franz Murner on 31.10.21.
//

import SpriteKit

class HighScoreConfettiComponent {
    private static let confettiPrototypeFile = "ConfettiBlueprint.sks"
    private static var possibleColors = GameColor.getAllColorsForGame()
    private static var texturesToEmit = ["sparkles", "triangle.fill", "suit.heart.fill", "circle.fill",
                                         "sparkles", "triangle.fill", "suit.heart.fill", "circle.fill",
                                         "sparkles", "triangle.fill", "suit.heart.fill", "circle.fill"]

    public static func fire(target: SKNode) {
        EventBus.shared.publish(event: HapticEvent(hapticLevel: .heavy))
        EventBus.shared.publish(event: HapticEvent(hapticLevel: .heavy))
        EventBus.shared.publish(event: HapticEvent(hapticLevel: .heavy))
        EventBus.shared.publish(event: HapticEvent(hapticLevel: .heavy))

        for texture in texturesToEmit {
            if possibleColors.isEmpty {
                possibleColors = GameColor.getAllColorsForGame()
            }

            let randomColor = possibleColors.remove(at: Int.random(in: 0 ..< possibleColors.count))
            let randomScale = CGFloat.random(in: 0.3 ... 1)

            emit(textureName: texture, color: randomColor.getColor(), target: target, scale: randomScale)
        }

        possibleColors = GameColor.getAllColorsForGame()
    }

    private static func emit(textureName: String, color: UIColor, target: SKNode, scale: CGFloat) {
        guard let emitterNode = SKEmitterNode(fileNamed: confettiPrototypeFile),
              let textureImage = UIImage(systemName: textureName)?.withTintColor(.white),
              let pngData = textureImage.pngData(),
              let image = UIImage(data: pngData)
        else { return }

        emitterNode.particleScale = scale
        emitterNode.position = CGPoint(x: 0, y: 950)
        emitterNode.particleTexture = SKTexture(image: image)
        emitterNode.particleColorSequence = SKKeyframeSequence(keyframeValues: [color], times: [1])
        emitterNode.run(SKAction.sequence([SKAction.wait(forDuration: 10),
                                           SKAction.removeFromParent()]))
        target.addChild(emitterNode)
    }
}
