//
//  GoMenu.swift
//  SwiBa
//
//  Created by Franz Murner on 17.08.21.
//

import SpriteKit

class GoMenu: SKNode {
    private var size: CGSize
    private var radius: CGFloat

    private var blendNode: SKShapeNode?
    private var backgroundNode: ColorShapeNode
    private var labelButton: LabelButton

    init(size: CGSize) {
        self.size = size
        radius = (GameConstants.circleRadius ?? 0) + GameConstants.colorCircleLineWidth / 2

        backgroundNode = ColorShapeNode(radius: radius)
        backgroundNode.zPosition = 1

        labelButton = LabelButton(text: "GO".localized(), name: "GO-Button", fontSize: 300, labelType: .goMenuLabel)
        labelButton.zPosition = 3

        super.init()

        self.zPosition = 10
        self.name = "GO-Button"

        addChild(backgroundNode)
        addChild(labelButton)

        startRotationAnimation()
        registerEvents()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func registerEvents() {
        EventBus.shared.subscribe(event: .UIInterfaceStyleUpdate) { _ in
            self.changeColor()
        }

        EventBus.shared.subscribe(event: .endEvent) { _ in
            EventBus.shared.publish(event: TouchLockOnEvent())
            self.showAction()
        }
    }

    private func startRotationAnimation() {
        let action = SKAction.sequence([SKAction.rotate(toAngle: CGFloat.pi * 2, duration: 60),
                                        SKAction.rotate(toAngle: 0, duration: 0)])

        backgroundNode.run(SKAction.repeatForever(action))
    }

    private func changeColor() {
        blendNode?.fillColor = GameColor.touchBlendNodeColor.getColor()
        blendNode?.strokeColor = GameColor.primaryBackground.getColor()
    }

    private func showAction() {
        self.isHidden = false
        let showAction = SKAction.sequence([SKAction.group([SKAction.rotate(byAngle: -CGFloat.pi * 2, duration: 0.7),
                                                            SKAction.scale(to: 1.02, duration: 0.7),
                                                            SKAction.fadeAlpha(to: 1, duration: 0.7)]),
            SKAction.scale(to: 0.98, duration: 0.2),
            SKAction.scale(to: 1.01, duration: 0.2),
            SKAction.scale(to: 0.99, duration: 0.2),
            SKAction.scale(to: 1.005, duration: 0.2),
            SKAction.scale(to: 1, duration: 0.2)])

        self.run(showAction) {
            EventBus.shared.publish(event: TouchLockOffEvent())
            self.startRotationAnimation()
        }
    }
}

extension GoMenu {
    func touchesBegan() {
        blendNode = SKShapeNode(circleOfRadius: radius - GameConstants.colorCircleLineWidth - 10)
        blendNode?.zPosition = 2
        blendNode?.lineWidth = 10

        changeColor()

        addChild(blendNode!)

        labelButton.setScale(0.9)
    }

    func touchesCancelled() {
        blendNode?.removeFromParent()
        blendNode = nil
        labelButton.setScale(1)
    }

    func touchesEnded() {
        touchesCancelled()
        backgroundNode.removeAllActions()

        let blendOutAction = SKAction.group([SKAction.rotate(byAngle: CGFloat.pi * 2, duration: 0.7),
                                             SKAction.scale(to: 0, duration: 0.7),
                                             SKAction.fadeAlpha(to: 0, duration: 0.7)])

        blendOutAction.timingMode = .easeIn

        EventBus.shared.publish(event: GoEvent())

        self.run(blendOutAction) {
            self.isHidden = true
            EventBus.shared.publish(event: TouchLockOffEvent())
        }
    }
}
