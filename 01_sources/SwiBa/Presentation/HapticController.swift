//
//  HapticController.swift
//  SwiBa
//
//  Created by Franz Murner on 01.11.21.
//

import Foundation
import UIKit

class HapticController {
    init() {
        registerForEvents()
    }

    private func registerForEvents() {
        EventBus.shared.subscribe(event: .hapticEvent) { event in
            guard let hapticEvent = event as? HapticEvent else { return }
            let generator = UIImpactFeedbackGenerator(style: hapticEvent.hapticLevel)
            generator.impactOccurred()
        }
    }
}
