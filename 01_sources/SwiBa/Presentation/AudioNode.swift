//
//  AudioNode.swift
//  SwiBa
//
//  Created by Franz Murner on 13.12.21.
//

import SpriteKit

class AudioNode: TANode {
    var isSoundOn = true

    override init() {
        super.init()
        subscribeEvents()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func playPlop() {
        let random = Int.random(in: 1 ... 5)
        let plopAction = SKAction.playSoundFileNamed("Plop-\(random).m4a", waitForCompletion: false)
        self.run(plopAction)
    }

    private func playConfettiExplosion() {
        let confettiPlop = SKAction.playSoundFileNamed("ConfettiCannon.m4a", waitForCompletion: false)
        self.run(confettiPlop)
    }

    private func subscribeEvents() {
        EventBus.shared.subscribe(event: .buttonUpdateEvent) { [self] event in
            guard let event = event as? ButtonUpdateEvent,
                  event.buttonType == .speakerSwitch
            else { return }
            isSoundOn = event.buttonState == .on
        }

        EventBus.shared.subscribe(event: .rightCollisionEvent) { [self] _ in
            guard isSoundOn else { return }
            playPlop()
        }

        EventBus.shared.subscribe(event: .confettiEvent) { [self] _ in
            guard self.isSoundOn else { return }
            playConfettiExplosion()
        }
    }
}
