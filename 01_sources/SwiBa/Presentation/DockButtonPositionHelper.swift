//
//  DockBtnPositionMgr.swift
//  SwiBa
//
//  Created by Franz Murner on 04.09.21.
//

import SpriteKit

class DockButtonPositionHelper {
    private var widthToHightRatio: CGFloat
    private var ellipseSize: CGSize

    init() {
        self.ellipseSize = ellipseSize
        self.widthToHightRatio = ellipseSize.height / ellipseSize.width
    }
}
