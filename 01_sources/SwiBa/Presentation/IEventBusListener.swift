//
//  IEventBusListener.swift
//  SwiBa
//
//  Created by Franz Murner on 02.08.21.
//

import Foundation

protocol IEventBusListener {
    func published(event: IEvent)
}
