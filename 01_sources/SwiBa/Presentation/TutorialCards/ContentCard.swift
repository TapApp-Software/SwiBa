//
//  ContentCard.swift
//  SwiBa
//
//  Created by Franz Murner on 03.01.22.
//

import SpriteKit

class ContentCard: TANode {
    // MARK: - Nodes

    private var card: TACard!
    private var titleLabel: TASKLabelNode!
    private var bodyLabel: TASKLabelNode!
    private var contentNode: TANode!

    // MARK: - Parameters

    private var sceneSize: CGSize
    private var cardSize: CGSize
    private var contentCardModel: ContentCardModel
    private var padding: CGFloat = 50

    // MARK: - Initializer

    init(sceneSize: CGSize, contentCardModel: ContentCardModel) {
        self.sceneSize = sceneSize
        self.contentCardModel = contentCardModel
        cardSize = CGSize(width: sceneSize.width * 0.95, height: 1500)
        super.init()
        self.borderSize = cardSize
        createCard()
        createTitleLabel()
        createTextArea()
        createContentNode()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Methods

    private func createCard() {
        card = TACard(backgroundColor: .secondaryBackgound, size: cardSize,
                      shadowColor: .cardNodeShadowColor)
        addChild(card)
    }

    private func createTitleLabel() {
        titleLabel = TASKLabelNode(lableWidth: card.size.width - padding * 2)
        titleLabel.font = UIFont.systemFont(ofSize: 100, weight: .bold)
        titleLabel.text = contentCardModel.title
        titleLabel
            .taConstraints =
            [TAConstraint.setWhenPartnerIsParent(sourceNode: card, direction: .left, withInset: padding),
             TAConstraint.setWhenPartnerIsParent(sourceNode: card, direction: .top,
                                                 withInset: padding - 15)]

        card.addChild(titleLabel)
    }

    private func createTextArea() {
        bodyLabel = TASKLabelNode(lableWidth: card.size.width - padding * 2)
        bodyLabel.font = UIFont.systemFont(ofSize: 60, weight: .semibold)
        bodyLabel.text = contentCardModel.textContent
        bodyLabel
            .taConstraints =
            [TAConstraint.setPartner(sourceNode: titleLabel, direction: .bottom, withOffset: 30),
             TAConstraint.setWhenPartnerIsParent(sourceNode: card, direction: .left, withInset: padding)]

        card.addChild(bodyLabel)
    }

    private func createContentNode() {
        guard let method = contentCardModel.nodeContentCallBack else { return }

        let paddingToBodyText: CGFloat = 50

        let textlabelBottomBorderPosition = bodyLabel.position.y - bodyLabel.getSize()
            .height / 2 - paddingToBodyText
        let cardBottomBorderPosition = card.position.y - card.getSize().height / 2 + padding

        let cardContentHeight = abs(cardBottomBorderPosition) + textlabelBottomBorderPosition
        let cardContentWidth = card.getSize().width - padding * 2

        contentNode = method(CGSize(width: cardContentWidth, height: cardContentHeight))
        contentNode.taConstraints = [.setWhenPartnerIsParent(sourceNode: card, direction: .bottom, withInset: padding)]
        card.addChild(contentNode)
    }
}
