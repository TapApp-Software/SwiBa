//
//  ContentCard1Content.swift
//  SwiBa
//
//  Created by Franz Murner on 15.01.22.
//

import SpriteKit

class ContentCard1Content: TANode {
    // MARK: - Nodes

    var madeWithLabel: TASKLabelNode!
    var heartSymbole: TASFSymboleNode!
    var inBavariaLabel: TASKLabelNode!

    // MARK: - Parameters

    var size: CGSize

    // MARK: - Initializer

    init(size: CGSize) {
        self.size = size
        super.init()
        createMadeWithLabel()
        createHeart()
        createInBavariaLabel()
        setPostitions()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Methods

    private func setPostitions() {
        self.borderSize = size

        madeWithLabel.taConstraints = [.setPartner(sourceNode: heartSymbole, direction: .top, withOffset: 20)]
        inBavariaLabel.taConstraints = [.setPartner(sourceNode: heartSymbole, direction: .bottom, withOffset: 0)]
    }

    private func createMadeWithLabel() {
        madeWithLabel = TASKLabelNode(lableWidth: size.width)
        madeWithLabel.font = UIFont.systemFont(ofSize: 100, weight: .bold)
        madeWithLabel.fontColor = .mainLabel
        madeWithLabel.text = "Made with"
        madeWithLabel.alingnment = .center
        addChild(madeWithLabel)
    }

    private func createHeart() {
        heartSymbole = TASFSymboleNode(symboleName: "heart.fill", color: .systemRed,
                                       size: 300)
        addChild(heartSymbole)
    }

    private func createInBavariaLabel() {
        inBavariaLabel = TASKLabelNode(lableWidth: size.width)
        inBavariaLabel.font = UIFont.systemFont(ofSize: 100, weight: .bold)
        inBavariaLabel.fontColor = .mainLabel
        inBavariaLabel.text = "in Bavaria"
        inBavariaLabel.alingnment = .center
        addChild(inBavariaLabel)
    }
}
