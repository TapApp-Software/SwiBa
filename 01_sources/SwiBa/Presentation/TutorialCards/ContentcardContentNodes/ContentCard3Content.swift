//
//  ContentCard3Content.swift
//  SwiBa
//
//  Created by Franz Murner on 09.01.22.
//

import SpriteKit

class ContentCard3Content: TANode {
    // MARK: - Nodes

    private var storeIcon: TARoundetImageNode!
    private var magnetIcon: TARoundetImageNode!
    private var overlapIcon: TARoundetImageNode!
    private var stopRotation: TARoundetImageNode!
    private var freezeIcon: TARoundetImageNode!

    // MARK: - Parameters

    // MARK: - Initializer

    override init() {
        super.init()
        createIcons()
        managePositions()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Methods

    private func createIcons() {
        let radius: CGFloat = 80
        let fillColor = GameColor.buttonColor

        storeIcon = TARoundetImageNode(imageName: GameImages.cart.rawValue, radius: radius * 5 / 3,
                                       fillColor: fillColor)
        magnetIcon = TARoundetImageNode(imageName: GameImages.magnet.rawValue, radius: radius, fillColor: fillColor)
        overlapIcon = TARoundetImageNode(imageName: GameImages.overlap.rawValue, radius: radius, fillColor: fillColor)
        stopRotation = TARoundetImageNode(imageName: GameImages.stopRotation.rawValue, radius: radius,
                                          fillColor: fillColor)
        freezeIcon = TARoundetImageNode(imageName: GameImages.freze.rawValue, radius: radius, fillColor: fillColor)

        self.addChild(storeIcon)
        self.addChild(magnetIcon)
        self.addChild(overlapIcon)
        self.addChild(stopRotation)
        self.addChild(freezeIcon)
    }

    private func managePositions() {
        let offset: CGFloat = -20

        magnetIcon.taConstraints = [.setPartner(sourceNode: storeIcon, direction: .left, withOffset: offset),
                                    .setPartner(sourceNode: storeIcon, direction: .top, withOffset: offset)]
        overlapIcon.taConstraints = [.setPartner(sourceNode: storeIcon, direction: .top, withOffset: offset),
                                     .setPartner(sourceNode: storeIcon, direction: .right, withOffset: offset)]
        stopRotation.taConstraints = [.setPartner(sourceNode: storeIcon, direction: .right, withOffset: offset),
                                      .setPartner(sourceNode: storeIcon, direction: .bottom, withOffset: offset)]
        freezeIcon.taConstraints = [.setPartner(sourceNode: storeIcon, direction: .bottom, withOffset: offset),
                                    .setPartner(sourceNode: storeIcon, direction: .left, withOffset: offset)]
    }
}
