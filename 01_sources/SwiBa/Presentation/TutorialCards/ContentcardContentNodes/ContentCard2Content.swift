//
//  ContentCard2Content.swift
//  SwiBa
//
//  Created by Franz Murner on 08.01.22.
//

import SpriteKit

class ContentCard2Content: TANode {
    // MARK: - Nodes

    private var circleNode: TANode = .init()
    private var segment1: Segment!
    private var segment2: Segment!
    private var ballNode1: TAShapeNode!
    private var ballNode2: TAShapeNode!
    private var handNode1: TASFSymboleNode!
    private var handNode2: TASFSymboleNode!

    // MARK: - Parameters

    private var size: CGSize
    private var radius: CGFloat = 0
    private let ballNode1Pos = CGPoint(x: 10, y: -40)
    private let ballNode2Pos = CGPoint(x: -20, y: 30)

    // MARK: - Initializer

    init(size: CGSize) {
        self.size = size
        super.init()

        borderSize = size
        createColorCircle()
        createBallNodes()
        createHandNodes()
        createAnimations()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Methods

    // MARK: Setup

    private func createColorCircle() {
        radius = size.width > size.height ? size.width : size.height
        radius /= 2
        radius -= 100

        segment2 = Segment(start: 0, end: 50, color: .systemRed, radius: radius)
        segment2.redraw()

        segment1 = Segment(start: 50, end: 100, color: .systemBlue, radius: radius)
        segment1.redraw()

        circleNode.zRotation = CGFloat.pi / 4

        addChild(circleNode)
        circleNode.addChild(segment1)
        circleNode.addChild(segment2)
    }

    private func createBallNodes() {
        ballNode1 = TAShapeNode(circleOfRadius: 50)
        ballNode1.fillColor = .systemBlue
        ballNode1.alpha = 0
        ballNode1.position = ballNode1Pos

        ballNode2 = TAShapeNode(circleOfRadius: 50)
        ballNode2.fillColor = .systemRed
        ballNode2.alpha = 0
        ballNode2.position = ballNode2Pos

        self.addChild(ballNode1)
        self.addChild(ballNode2)
    }

    private func createHandNodes() {
        let tipPosition = CGPoint(x: 40, y: -60)
        handNode1 = TASFSymboleNode(symboleName: "hand.point.up.left.fill", color: .mainLabel,
                                    size: 150)
        handNode1.alpha = 0
        handNode1.position = tipPosition

        handNode2 = TASFSymboleNode(symboleName: "hand.point.up.left.fill", color: .mainLabel,
                                    size: 150)
        handNode2.alpha = 0
        handNode2.position = tipPosition

        ballNode1.addChild(handNode1)
        ballNode2.addChild(handNode2)
    }

    private func createAnimations() {
        let waitTimeBetweenAnimations: TimeInterval = 3

        let ballNode1GoalPos = getMidPoint(zRot: circleNode.zRotation, segment: segment1)
        let ballNode2GoalPos = getMidPoint(zRot: circleNode.zRotation, segment: segment2)

        let ball1Action = SKAction
            .sequence([ballAction(source: ballNode1Pos, destination: ballNode1GoalPos),
                       .wait(forDuration: waitTimeBetweenAnimations)])
        let hand1Action = SKAction.sequence([handAction(), .wait(forDuration: waitTimeBetweenAnimations)])

        ballNode1.run(SKAction.repeatForever(ball1Action))
        handNode1.run(SKAction.repeatForever(hand1Action))

        let ball2Action = SKAction
            .sequence([.wait(forDuration: waitTimeBetweenAnimations),
                       ballAction(source: ballNode2Pos, destination: ballNode2GoalPos)])
        let hand2Action = SKAction.sequence([.wait(forDuration: waitTimeBetweenAnimations), handAction()])

        ballNode2.run(SKAction.repeatForever(ball2Action))
        handNode2.run(SKAction.repeatForever(hand2Action))
    }

    private func handAction() -> SKAction {
        return SKAction.sequence([SKAction.wait(forDuration: 0.6),
                                  SKAction.fadeAlpha(to: 1, duration: 0.1),
                                  SKAction.wait(forDuration: 1.7),
                                  SKAction.fadeAlpha(to: 0, duration: 0.2)])
    }

    private func ballAction(source: CGPoint, destination: CGPoint) -> SKAction {
        return SKAction.sequence([SKAction.fadeAlpha(to: 1, duration: 0.3),
                                  SKAction.wait(forDuration: 0.4),
                                  SKAction.move(to: destination, duration: 1.5),
                                  SKAction.wait(forDuration: 0.3),
                                  SKAction.fadeAlpha(to: 0, duration: 0.1),
                                  SKAction.move(to: source, duration: 0.0)])
    }

    private func getMidPoint(zRot: CGFloat, segment: Segment) -> CGPoint {
        let rotationInPercent = zRot / (CGFloat.pi * 2) * 100
        let rot = segment.midPoint + rotationInPercent
        let x = cos(CGFloat.pi * 2 / 100 * rot) * radius
        let y = sin(CGFloat.pi * 2 / 100 * rot) * radius
        return CGPoint(x: x, y: y)
    }
}
