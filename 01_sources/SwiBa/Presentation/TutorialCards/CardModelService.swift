//
//  CardModelService.swift
//  SwiBa
//
//  Created by Franz Murner on 04.01.22.
//

import SpriteKit

// swiftlint:disable all

struct CardModelService {
    static func createContent1(size: CGSize) -> TANode {
        return ContentCard1Content(size: size)
    }

    static func createContent2(size: CGSize) -> TANode {
        return ContentCard2Content(size: size)
    }

    static func createContent3(size: CGSize) -> TANode {
        let content = ContentCard3Content()
        content.borderSize = size
        return content
    }

    static func createContent4(size: CGSize) -> TANode {
        let realContentSize = size.applyBoth(-200)

        let node = TARoundetImageNode(imageName: GameImages.magnet.rawValue, radius: realContentSize.width / 2,
                                      fillColor: .buttonColor)
        node.borderSize = size

        return node
    }

    static func createContent5(size: CGSize) -> TANode {
        let realContentSize = size.applyBoth(-200)

        let node = TARoundetImageNode(imageName: GameImages.overlap.rawValue, radius: realContentSize.width / 2,
                                      fillColor: .buttonColor)
        node.borderSize = size

        return node
    }

    static func createContent6(size: CGSize) -> TANode {
        let realContentSize = size.applyBoth(-200)

        let node = TARoundetImageNode(imageName: GameImages.stopRotation.rawValue, radius: realContentSize.width / 2,
                                      fillColor: .buttonColor)
        node.borderSize = size

        return node
    }

    static func createContent7(size: CGSize) -> TANode {
        let realContentSize = size.applyBoth(-200)

        let node = TARoundetImageNode(imageName: GameImages.freze.rawValue, radius: realContentSize.width / 2,
                                      fillColor: .buttonColor)
        node.borderSize = size

        return node
    }

    private static var page1 = ContentCardModel(title: "TCard1Title".localized(),
                                                textContent: "TCard1Dialog".localized(),
                                                nodeContentCallBack: createContent1)

    private static var page2 = ContentCardModel(title: "TCard2Title".localized(),
                                                textContent: "TCard2Dialog".localized(),
                                                nodeContentCallBack: createContent2)

    private static var page3 = ContentCardModel(title: "PowerUps",
                                                textContent: "TCard3Dialog".localized(),
                                                nodeContentCallBack: createContent3)

    private static var page4 = ContentCardModel(title: "Magnet",
                                                textContent: "TCard4Dialog".localized(),
                                                nodeContentCallBack: createContent4)

    private static var page5 = ContentCardModel(title: "Overlap",
                                                textContent: "TCard5Dialog".localized(),
                                                nodeContentCallBack: createContent5)

    private static var page6 = ContentCardModel(title: "Stop Rotation",
                                                textContent: "TCard6DialogP1"
                                                    .localized() + " \(Int(GameConstants.durationStopRotation)) " +
                                                    "TCard6DialogP2".localized(),
                                                nodeContentCallBack: createContent6)

    private static var page7 = ContentCardModel(title: "Freeze",
                                                textContent: "TCard7DialogP1"
                                                    .localized() + " \(Int(GameConstants.durationFreeze)) " +
                                                    "TCard7DialogP2".localized(),
                                                nodeContentCallBack: createContent7)

    public static func getData() -> [ContentCardModel] {
        return [page1, page2, page3, page4, page5, page6, page7]
    }
}

// swiftlint:enable all
