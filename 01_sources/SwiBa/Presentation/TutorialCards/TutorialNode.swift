//
//  CardStack.swift
//  SwiBa
//
//  Created by Franz Murner on 03.01.22.
//

import SpriteKit

class TutorialNode: TANode {
    // MARK: - Nodes

    private var background: TASpriteNode!
    private var cardStackNode: TANode = .init()
    private var cards: [ContentCard] = []
    private var dots = TAIndicator()
    private var leftForwardButton: TAButton?
    private var rightForwardButton: TAButton?
    private var leftCloseButton: TAButton?
    private var rightCloseButton: TAButton?

    // MARK: - Parameters

    private var currentPage = 0 {
        didSet {
            dots.activeDotIndexNr = currentPage
        }
    }

    private var size: CGSize
    private var cardModelList: [ContentCardModel]

    private let colorModel = TAButtonColorModel(onNormal: .buttonColor,
                                                onPressed: .buttonColorPressed,
                                                onDisabled: .buttonColorDisabled)

    public var tutorialClosedHandler = {}

    // MARK: - Initializer

    init(sceneSize: CGSize, cardModelList: [ContentCardModel]) {
        self.size = sceneSize
        self.cardModelList = cardModelList

        super.init()
        self.cardStackNode.borderSize = CGSize(width: sceneSize.width * 0.95, height: 1500)
        cardStackNode.position.y = HelperFunctions.hasDeviceNotch() ? 0 : 80

        self.addChild(cardStackNode)

        createBackground()
        createCards()
        createIndicators()
        createActionButtons()
        checkButtonVisibility()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Methods

    // MARK: Setup

    private func createBackground() {
        background = TASpriteNode(color: GameColor.primaryBackground, size: size)
        background.colorAlpha = 0.9

        addChild(background)
    }

    private func createCards() {
        dots.count = cardModelList.count

        for index in 0 ..< cardModelList.count {
            let card = ContentCard(sceneSize: size, contentCardModel: cardModelList[index])
            card.zPosition = CGFloat(cardModelList.count - index)
            card.zRotation = 0.00
            card.setScale(1)
            card.isHidden = true
            cards.append(card)
            cardStackNode.addChild(card)
        }

        cards.first?.isHidden = false
    }

    private func createIndicators() {
        dots.activeDotIndexNr = 0
        dots.taConstraints = [TAConstraint.setPartner(sourceNode: cardStackNode,
                                                      direction: .bottom,
                                                      withOffset: 100)]
        background.addChild(dots)
    }

    private func createActionButtons() {
        createLeftButton()
        createRightButton()
        createLeftCloseButton()
        createRightCloseButton()
    }

    private func createButtonWith(texture textureName: String, colorModel: TAButtonColorModel? = nil,
                                  iconColor: GameColor = .mainLabel) -> TAButton {
        let backgroundNode = TAShapeNode(circleOfRadius: GameConstants.dockButtonRadius)
        backgroundNode.strokeColor = .separatorColor
        backgroundNode.lineWidth = 2

        let icon = TASFSymboleNode(symboleName: textureName, color: iconColor,
                                   size: 70)
        icon.weight = .semibold
        backgroundNode.addChild(icon)

        let button = TAButton(backgroundNode: backgroundNode, colorModel: colorModel ?? self.colorModel)

        return button
    }

    private func createLeftButton() {
        let button = createButtonWith(texture: "arrow.backward")
        button.taConstraints = [TAConstraint.setSameYPosition(like: dots),
                                TAConstraint.setContainingPartner(sourceNode: cardStackNode, direction: .left,
                                                                  withInset: 40)]
        button.touchEndetClosure = previousCard
        background.addChild(button)

        leftForwardButton = button
    }

    private func createRightButton() {
        let button = createButtonWith(texture: "arrow.forward")
        button.taConstraints = [TAConstraint.setSameYPosition(like: dots),
                                TAConstraint.setContainingPartner(sourceNode: cardStackNode, direction: .right,
                                                                  withInset: 40)]
        button.touchEndetClosure = nextCard
        background.addChild(button)

        rightForwardButton = button
    }

    private func createLeftCloseButton() {
        let button = createButtonWith(texture: "xmark", iconColor: .systemRed)
        button.taConstraints = [TAConstraint.setSameYPosition(like: dots),
                                TAConstraint.setContainingPartner(sourceNode: cardStackNode, direction: .left,
                                                                  withInset: 40)]
        button.touchEndetClosure = closeCards

        background.addChild(button)

        leftCloseButton = button
    }

    private func createRightCloseButton() {
        let button = createButtonWith(texture: "checkmark", iconColor: .systemGreen)
        button.taConstraints = [TAConstraint.setSameYPosition(like: dots),
                                TAConstraint.setContainingPartner(sourceNode: cardStackNode, direction: .right,
                                                                  withInset: 40)]
        button.touchEndetClosure = closeCards
        background.addChild(button)

        rightCloseButton = button
    }

    // MARK: Logic

    private func nextCard() {
        guard currentPage >= 0, currentPage < cards.count - 1 else { return }

        cards[currentPage + 1].isHidden = false

        let swipeOutAction = SKAction.group([SKAction.rotate(toAngle: CGFloat.random(in: 0.2 ... 1), duration: 0.5),
                                             SKAction.move(to: CGPoint(x: -1500, y: -30), duration: 0.5)])
        swipeOutAction.timingMode = .easeIn

        cards[currentPage].removeAllActions()
        cards[currentPage].run(swipeOutAction)
        currentPage += 1

        checkButtonVisibility()
    }

    private func previousCard() {
        guard currentPage >= 1, currentPage < cards.count else { return }

        let oldCardNr = currentPage

        currentPage -= 1

        let swipeInAction = SKAction.group([SKAction.rotate(toAngle: 0.0, duration: 0.5),
                                            SKAction.move(to: CGPoint(x: 0, y: 0), duration: 0.5)])

        swipeInAction.timingMode = .easeOut
        cards[currentPage].removeAllActions()
        cards[currentPage].run(swipeInAction) { [self] in
            cards[oldCardNr].isHidden = true
        }

        checkButtonVisibility()
    }

    private func closeCards() {
        let swipeOutAction = SKAction.group([SKAction.rotate(toAngle: CGFloat.random(in: 0.2 ... 1), duration: 0.7),
                                             SKAction.move(to: CGPoint(x: -1500, y: -30), duration: 0.5)])
        swipeOutAction.timingMode = .easeIn

        cardStackNode.run(swipeOutAction)

        background.run(SKAction.fadeOut(withDuration: 0.7)) {
            self.removeFromParent()
            self.tutorialClosedHandler()
        }
    }

    private func checkButtonVisibility() {
        if currentPage > 0, currentPage < cards.count - 1 {
            leftCloseButton?.isHidden = true
            rightCloseButton?.isHidden = true
            leftForwardButton?.isHidden = false
            rightForwardButton?.isHidden = false
        } else if currentPage < 1 {
            leftCloseButton?.isHidden = false
            rightCloseButton?.isHidden = true
            leftForwardButton?.isHidden = true
            rightForwardButton?.isHidden = false
        } else if currentPage >= cards.count - 1 {
            leftCloseButton?.isHidden = true
            rightCloseButton?.isHidden = false
            leftForwardButton?.isHidden = false
            rightForwardButton?.isHidden = true
        }
    }
}
