//
//  ContentCardMode.swift
//  SwiBa
//
//  Created by Franz Murner on 03.01.22.
//

import SpriteKit

struct ContentCardModel {
    var title: String
    var textContent: String
    var nodeContentCallBack: ((_ size: CGSize) -> TANode)?
}
