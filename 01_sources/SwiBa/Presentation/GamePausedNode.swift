//
//  GamePausedNode.swift
//  SwiBa
//
//  Created by Franz Murner on 02.01.22.
//

import Foundation
import SpriteKit

class GamePausedNode: TANode {
    // MARK: - Parameters

    private var sceneSize: CGSize

    // MARK: - Initializer

    init(sceneSize: CGSize, gameMainNode _: SKNode) {
        self.sceneSize = sceneSize
        super.init()
        self.zPosition = 1000
        createBackground()
        createPausedLabel()
        createCountinueButton()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Methods

    private func createBackground() {
        var backgroundAlpha = 0.6

        if let image = ViewController.currentView?.getScreenshot() {
            let spriteNode = SKSpriteNode(texture: SKTexture(image: image), size: sceneSize)

            let effectNode = SKEffectNode()
            let blurFilter = CIFilter(name: "CIGaussianBlur", parameters: ["inputRadius": 30])
            effectNode.filter = blurFilter

            effectNode.addChild(spriteNode)

            addChild(effectNode)
        } else {
            backgroundAlpha = 0.9
        }

        let backgroundNode = SKSpriteNode(color: GameColor.primaryBackground.getColor()
            .withAlphaComponent(backgroundAlpha),
            size: sceneSize)
        addChild(backgroundNode)
    }

    private func createPausedLabel() {
        let labelNode = TASKLabelNode(lableWidth: sceneSize.width)
        labelNode.text = "Game Paused".localized()
        labelNode.font = UIFont.tapFontRusticalFont(ofSize: 200)
        labelNode.alingnment = .center
        labelNode
            .taConstraints = [TAConstraint.setWhenPartnerIsParent(sourceNode: self, direction: .top, withInset: 200)]
        addChild(labelNode)
    }

    private func createCountinueButton() {
        let buttonContent = TAStaticPilledLabel(size: CGSize(width: sceneSize.width * 0.6, height: 120),
                                                text: "Countinue".localized(), fillColor: .clear)

        buttonContent.constraint.padding.globalY = 10
        buttonContent.updateConstraint()

        let button = TAButton(backgroundNode: buttonContent,
                              buttonType: .gameCountinueButton,
                              mainColor: .buttonBlue,
                              onTouchColor: .buttonBluePressed,
                              onDisabledColor: .buttonBlueDisabled)

        button
            .taConstraints = [TAConstraint.setWhenPartnerIsParent(sourceNode: self, direction: .bottom, withInset: 350)]
        button.touchEndetHandler = CountinueButtonPressEvent()

        addChild(button)
    }
}
