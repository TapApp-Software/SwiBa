//
//  LabelTypes.swift
//  SwiBa
//
//  Created by Franz Murner on 03.10.21.
//

import Foundation

enum LabelTypes {
    case goMenuLabel
    case scoreLabel
    case highScoreLabel
    case highScoreTextLabel
    case buttonLabel
    case moneyLabel
    case priceLabelMagnet
    case priceLabelOverlap
    case priceLabelFreze
    case priceLabelStopRotation
    case stockLabelMagnet
    case stockLabelOverlap
    case stockLabelFreze
    case stockLabelStopRotation
    case none
}
