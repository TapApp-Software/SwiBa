//
//  PowerUpStore.swift
//  SwiBa
//
//  Created by Franz Murner on 28.11.21.
//

import SpriteKit

class PowerUpStore: TABottomCard {
    // MARK: - Nodes

    private var amountCreditLabel: TAShapeNode!
    private var magnetSubcard: PowerUpStoreContentCard!
    private var overlapSubcard: PowerUpStoreContentCard!
    private var stopRotationSubcard: PowerUpStoreContentCard!
    private var freezeSubcard: PowerUpStoreContentCard!

    // MARK: - Parameters

    private var space: CGFloat = 30
    private var subcardSize: CGSize = .zero

    // MARK: - Initializer

    init(sceneSize: CGSize) {
        let height: CGFloat = HelperFunctions.hasDeviceNotch() ? 1750 : 1700
        super.init(title: "Power-Up Store", sceneSize: sceneSize, height: height)
        subcardSize = CGSize(width: card.borderSize!.width / 2 - space / 2, height: 650)
        createAmountCreditLabel()
        createMagnetSubcard()
        createOverlapSubcard()
        createStopCircleRotationSubcard()
        createFreezeBallEmitterSubcard()
        setConstraints()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Methods

    private func createAmountCreditLabel() {
        let pillSize = CGSize(width: 300, height: 80)

        amountCreditLabel = TAShapeNode(size: pillSize, cornerRadius: pillSize.height / 2)
        amountCreditLabel.fillColor = .tertiaryBackground

        let label = TASKLabelNode(lableWidth: 300, labelType: .moneyLabel)
        label.font = UIFont.tapFontRusticalFont(ofSize: 70)
        amountCreditLabel.addChild(label)

        card.addChild(amountCreditLabel)
    }

    private func createMagnetSubcard() {
        magnetSubcard = PowerUpStoreContentCard(size: subcardSize,
                                                image: .magnet, labelText: "Magnet",
                                                priceLabel: .priceLabelMagnet,
                                                stockLabel: .stockLabelMagnet,
                                                dockButtonType: .magnet,
                                                addButtonType: .powerUpStoreMagentAddButton)

        card.addChild(magnetSubcard)
    }

    private func createOverlapSubcard() {
        overlapSubcard = PowerUpStoreContentCard(size: subcardSize,
                                                 image: .overlap,
                                                 labelText: "Overlap",
                                                 priceLabel: .priceLabelOverlap,
                                                 stockLabel: .stockLabelOverlap,
                                                 dockButtonType: .overlap,
                                                 addButtonType: .powerUpStoreOverlapAddButton)
        card.addChild(overlapSubcard)
    }

    private func createStopCircleRotationSubcard() {
        stopRotationSubcard = PowerUpStoreContentCard(size: subcardSize,
                                                      image: .stopRotation,
                                                      labelText: "Stop Circle",
                                                      priceLabel: .priceLabelStopRotation,
                                                      stockLabel: .stockLabelStopRotation,
                                                      dockButtonType: .stopCircle,
                                                      addButtonType: .powerUpStoreStopCircleAddButton)
        card.addChild(stopRotationSubcard)
    }

    private func createFreezeBallEmitterSubcard() {
        freezeSubcard = PowerUpStoreContentCard(size: subcardSize,
                                                image: .freze, labelText: "Freeze",
                                                priceLabel: .priceLabelFreze,
                                                stockLabel: .stockLabelFreze,
                                                dockButtonType: .freeze,
                                                addButtonType: .powerUpStoreFreezeAddButton)
        card.addChild(freezeSubcard)
    }

    private func setConstraints() {
        amountCreditLabel
            .taConstraints = [.setPartner(sourceNode: titleLableNode, direction: .bottom, withOffset: space)]

        magnetSubcard.taConstraints = [.setWhenPartnerIsParent(sourceNode: card, direction: .left),
                                       .setPartner(sourceNode: amountCreditLabel, direction: .bottom,
                                                   withOffset: space)]

        overlapSubcard.taConstraints = [.setWhenPartnerIsParent(sourceNode: card, direction: .right),
                                        .setSameYPosition(like: magnetSubcard)]

        stopRotationSubcard.taConstraints = [.setWhenPartnerIsParent(sourceNode: card, direction: .left),
                                             .setPartner(sourceNode: overlapSubcard, direction: .bottom,
                                                         withOffset: space)]

        freezeSubcard.taConstraints = [.setWhenPartnerIsParent(sourceNode: card, direction: .right),
                                       .setPartner(sourceNode: overlapSubcard, direction: .bottom, withOffset: space)]
    }
}
