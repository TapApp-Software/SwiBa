//
//  Segment.swift
//  SwiBa
//
//  Created by Franz Murner on 27.10.21.
//

import Foundation
import SpriteKit

class Segment: SKNode {
    // MARK: - Subclasses

    private var shapeNode = SKShapeNode()

    // MARK: - Properties

    public var start: CGFloat
    public var end: CGFloat
    public var color: GameColor
    private var radius: CGFloat

    // MARK: - Computed Properties

    public var midPoint: CGFloat {
        return (start + end) / 2
    }

    // MARK: - Initialization

    init(start: CGFloat, end: CGFloat, color: GameColor, radius: CGFloat) {
        self.start = start
        self.end = end
        self.radius = radius
        self.color = color

        super.init()

        registerForEvents()
        setupNode()
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public Methods

    /// This method must be called if the end or the start value is changed to update the path
    public func redraw() {
        let startAngle: CGFloat = calculateAngle(value: start)
        let endAngle: CGFloat = calculateAngle(value: end)

        let segmentPath = CGMutablePath()
        segmentPath.addArc(center: CGPoint.zero, radius: radius, startAngle: startAngle, endAngle: endAngle,
                           clockwise: false)
        shapeNode.path = segmentPath

        setupPhysics(path: segmentPath)
    }

    // MARK: - Private Methodes

    // MARK: Setup

    private func setupNode() {
        shapeNode.lineWidth = GameConstants.colorCircleLineWidth
        shapeNode.fillColor = .clear
        setupColor()

        self.addChild(shapeNode)
    }

    private func registerForEvents() {
        EventBus.shared.subscribe(event: .UIInterfaceStyleUpdate) { _ in
            self.setupColor()
        }
    }

    private func setupPhysics(path: CGPath) {
        self.name = "Segment"
        self.physicsBody = SKPhysicsBody(edgeChainFrom: path)
        self.physicsBody?.affectedByGravity = false
        self.physicsBody?.categoryBitMask = BitMasks.segment.rawValue
        self.physicsBody?.contactTestBitMask = BitMasks.ball.rawValue
        self.physicsBody?.usesPreciseCollisionDetection = true
        self.physicsBody?.isDynamic = false
    }

    // MARK: Logic

    private func calculateAngle(value: CGFloat) -> CGFloat {
        return (CGFloat.pi * 2 / 100) * value
    }

    private func setupColor() {
        shapeNode.strokeColor = color.getColor()
    }
}
