//
//  IntroScreen.swift
//  SwiBa
//
//  Created by Franz Murner on 28.12.21.
//

import SpriteKit

class IntroScreen: SKNode {
    // MARK: - Initializer

    init(sceneSize: CGSize) {
        super.init()

        let textureAppearanceString = UITraitCollection.current.userInterfaceStyle == .dark ? "Dark" : "Light"

        let backgroundNode = SKSpriteNode(color: GameColor.primaryBackground.getColor(), size: sceneSize)
        addChild(backgroundNode)

        let videoNode = SKVideoNode(fileNamed: "TapApp-Animation-\(textureAppearanceString).mp4")
        backgroundNode.addChild(videoNode)
        videoNode.play()

        videoNode.run(SKAction.sequence([SKAction.wait(forDuration: 3.5),
                                         SKAction.fadeOut(withDuration: 0.5)]))

        self.run(SKAction.sequence([SKAction.wait(forDuration: 4),
                                    SKAction.fadeOut(withDuration: 0.5),
                                    SKAction.removeFromParent()]))
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
