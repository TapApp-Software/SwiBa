//
//  LabelButton.swift
//  SwiBa
//
//  Created by Franz Murner on 30.09.21.
//

import SpriteKit

class LabelButton: SKNode {
    init(text: String, name: String, fontSize: CGFloat, labelType: LabelTypes? = nil) {
        let label = TALabelNode(startFontSize: fontSize, text: text, labelType: labelType)
        label.zPosition = 1

        let button = SKSpriteNode(color: .clear, size: label.currentLabelSize ?? CGSize(width: 50, height: 50))
        button.zPosition = 2
        button.name = name

        super.init()

        addChild(label)
        addChild(button)
    }

    @available(*, unavailable)
    required init?(coder _: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
