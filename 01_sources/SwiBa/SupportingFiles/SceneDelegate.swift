//
//  SceneDelegate.swift
//  SwiBa
//
//  Created by Franz Murner on 30.07.21.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?

    func scene(_: UIScene, willConnectTo _: UISceneSession, options _: UIScene.ConnectionOptions) {
        EventBus.shared.subscribe(event: .UIInterfaceStyleOverride) { event in
            guard let event = event as? UIInterfaceStyleOverride else { return }
            self.window?.overrideUserInterfaceStyle = event.appearance
        }
    }

    func sceneDidDisconnect(_: UIScene) {
        EventBus.shared.publish(event: SystemNotificationEvent(eventType: .sceneDidDisconnect))
    }

    func sceneDidBecomeActive(_: UIScene) {
        EventBus.shared.publish(event: SystemNotificationEvent(eventType: .sceneDidBecomeActive))
    }

    func sceneWillResignActive(_: UIScene) {
        EventBus.shared.publish(event: SystemNotificationEvent(eventType: .sceneWillResignActive))
    }

    func sceneWillEnterForeground(_: UIScene) {
        EventBus.shared.publish(event: SystemNotificationEvent(eventType: .sceneWillEnterForeground))
    }

    func sceneDidEnterBackground(_: UIScene) {
        EventBus.shared.publish(event: SystemNotificationEvent(eventType: .sceneDidEnterBackground))
    }
}
